﻿using System;
using System.IO;
using System.Windows;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Services;
using Galaxy_Rotation_4.ViewModel;
using Galaxy_Rotation_4.ViewModel.Windows;

namespace Galaxy_Rotation_4;

/// <summary>Логика взаимодействия для App.xaml</summary>
public partial class App : Application
{
    protected override void OnStartup(StartupEventArgs e)
    {
        base.OnStartup(e);
        ApplicationModel applicationVm;

        if (e.Args.Length == 1)
        {
            string path = e.Args[0];

            try
            {
                using FileStream file = File.Open(path, FileMode.Open);
                using var reader = new StreamReader(file);
                applicationVm = ApplicationModelManager.Open(reader.ReadToEnd(), Path.GetFileNameWithoutExtension(path));
            }
            catch (Exception exception)
            {
                WindowsManager.Instanse.ShowMessage($"Не удалось открыть файл {path}: {exception.Message}");
                applicationVm = new ApplicationModel();
            }
        }
        else
        {
            applicationVm = new ApplicationModel();
        }

        WindowsManager.Instanse.Open(new MainWindowVm(applicationVm));
    }
}
