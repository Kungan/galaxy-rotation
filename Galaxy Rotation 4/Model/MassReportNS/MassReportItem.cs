﻿using System;
using Galaxy_Rotation_4.Model.Components;

namespace Galaxy_Rotation_4.Model.MassReportNS;

public class MassReportItem
{
    private bool _enabled;
    private int _index;
    private double _mass;
    private bool _fullMassModeEnabled;
    private double _highRadius;
    private double _lowRadius;

    public MassReportItem(Component component, double lowRadius, double highRadius, bool fullMassModeEnabled, int index)
    {
        Component = component;
        _index = index;
        _lowRadius = lowRadius;
        _fullMassModeEnabled = fullMassModeEnabled;
        _highRadius = highRadius;
        component.ParametersChanged += Update;
        component.EnabledChanged += () => Enabled = Component.IsEnabled;
        Enabled = component.IsEnabled;
        Update();
    }

    public Component Component { get; }

    public bool Enabled
    {
        get => _enabled;
        private set
        {
            _enabled = value;
            EnabledChanged?.Invoke();
        }
    }

    public int Index
    {
        get => _index;
        set
        {
            _index = value;
            IndexChanged?.Invoke();
        }
    }

    public double LowRadius
    {
        get => _lowRadius;
        set
        {
            _lowRadius = value;
            Update();
        }
    }

    public double HighRadius
    {
        get => _highRadius;
        set
        {
            _highRadius = value;
            Update();
        }
    }

    public bool FullMassModeEnabled
    {
        get => _fullMassModeEnabled;
        set
        {
            _fullMassModeEnabled = value;
            Update();
        }
    }

    public double Mass
    {
        get => _mass;
        private set
        {
            _mass = value;
            MassChanged?.Invoke();
        }
    }

    public event Action EnabledChanged;
    public event Action MassChanged;
    public event Action IndexChanged;

    private void Update()
    {
        Mass = FullMassModeEnabled ? 
            Component.GetFullMass() : 
            Component.GetMass(_highRadius) - Component.GetMass(_lowRadius);
    }
}
