﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Galaxy_Rotation_4.Model.Components;

namespace Galaxy_Rotation_4.Model.MassReportNS;

public class MassReport
{
    public MassReport(Galaxy galaxy)
    {
        _galaxy = galaxy;
        _lowRadius = 0;
        _highRadius = galaxy.PlotParams.RadiusMax;
        _fullMassModeEnabled = true;
        // Добавлять элементы отчета можно только тогда, когда настроены все параметры отчета о массах.
        AddMassReportItems();
        galaxy.ComponentAdded += Galaxy_ComponentAdded;
        galaxy.ComponentRemoved += Galaxy_ComponentRemoved;
    }

    private readonly Galaxy _galaxy;
    private double _lowRadius;
    private bool _fullMassModeEnabled;
    private double _galaxyMass;
    private double _highRadius;
    private readonly List<MassReportItem> _massReportItems = new();

    public event EventHandler<MassReportItemEventArgs> MassReportItemAdded;
    public event EventHandler<MassReportItemEventArgs> MassReportItemRemoved;
    public event Action GalaxyMassChanged;

    public ReadOnlyCollection<MassReportItem> MassReportItems => _massReportItems.AsReadOnly();

    public bool FullMassModeEnabled
    {
        get => _fullMassModeEnabled;
        set
        {
            _fullMassModeEnabled = value;
            UpdateMassMode();
        }
    }

    public double GalaxyMass
    {
        get => _galaxyMass;
        private set
        {
            _galaxyMass = value;
            GalaxyMassChanged?.Invoke();
        }
    }

    public double LowRadius
    {
        get => _lowRadius;
        set
        {
            _lowRadius = value;
            UpdateReport();
        }
    }

    public double HighRadius
    {
        get => _highRadius;
        set
        {
            _highRadius = value;
            UpdateReport();
        }
    }

    private void Galaxy_ComponentAdded(object sender, ComponentEventArgs e)
    {
        MassReportItem massReportItem = AddMassReportItem(e.Component);
        UpdateGalaxyMass();
        MassReportItemAdded?.Invoke(this, new MassReportItemEventArgs(massReportItem));
    }

    private void AddMassReportItems()
    {
        foreach (Component component in _galaxy.Components)
        {
            AddMassReportItem(component);
        }
    }

    private MassReportItem AddMassReportItem(Component component)
    {
        int index = _galaxy.GetComponentIndex(component);
        var massReportItem = new MassReportItem(component, LowRadius, HighRadius, FullMassModeEnabled, index);
        _massReportItems.Add(massReportItem);
        massReportItem.MassChanged += UpdateGalaxyMass;
        massReportItem.EnabledChanged += UpdateGalaxyMass;

        return massReportItem;
    }

    private void Galaxy_ComponentRemoved(object sender, ComponentEventArgs e)
    {
        MassReportItem vm = _massReportItems.First(item => item.Component == e.Component);
        _massReportItems.Remove(vm);
        UpdateGalaxyMass();
        UpdateReportItemsNumbers();
        OnMassReportItemRemoved(vm);
    }

    private void OnMassReportItemRemoved(MassReportItem massReportItem)
    {
        MassReportItemRemoved.Invoke(this, new MassReportItemEventArgs(massReportItem));
    }

    private void UpdateGalaxyMass()
    {
        GalaxyMass = _massReportItems.Where(item => item.Enabled).Sum(item => item.Mass);
    }

    private void UpdateMassMode()
    {
        foreach (MassReportItem item in _massReportItems)
            item.FullMassModeEnabled = FullMassModeEnabled;
    }

    private void UpdateReport()
    {
        foreach (MassReportItem item in _massReportItems)
        {
            item.LowRadius = LowRadius;
            item.HighRadius = HighRadius;
        }

        UpdateGalaxyMass();
    }

    private void UpdateReportItemsNumbers()
    {
        foreach (MassReportItem item in _massReportItems)
            item.Index = _galaxy.GetComponentIndex(item.Component);
    }
}
