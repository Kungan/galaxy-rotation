﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galaxy_Rotation_4.Model;

public class Curve
{
    /// <summary>Кривая.</summary>
    private IReadOnlyDictionary<double, double> _dots;

    private bool _isEnabled;

    public Curve()
    {
        _dots = new Dictionary<double, double>();
    }

    public Curve(Dictionary<double, double> dots, bool isEnabled = false)
    {
        _dots = dots;
        _isEnabled = isEnabled;
    }

    public IReadOnlyDictionary<double, double> Dots
    {
        get => _dots;
        set
        {
            _dots = value;
            DotsChanged?.Invoke();
        }
    }

    /// <summary>Флаг активности графика.</summary>
    public bool IsEnabled
    {
        get => _isEnabled;

        set
        {
            if (_isEnabled == value)
                return;

            _isEnabled = value;
            IsEnabledChanged?.Invoke();
        }
    }

    public event Action DotsChanged;
    public event Action IsEnabledChanged;

    public override string ToString()
    {
        StringBuilder s = new();

        foreach ((double key, double value) in _dots)
            s.Append(key + " " + value + "\n");

        return s.ToString();
    }
}
