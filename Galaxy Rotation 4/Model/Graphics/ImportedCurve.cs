﻿using System;
using System.Collections.Generic;

namespace Galaxy_Rotation_4.Model.Graphics;

public class ImportedCurve : Curve
{
    private string _name;

    public ImportedCurve(Dictionary<double, double> dots, string name = "") : base(dots, true)
    {
        _name = name;
    }

    public string Name
    {
        get => _name;
        set
        {
            if (value == _name)
                return;

            _name = value;
            NameChanged?.Invoke();
        }
    }

    public event Action NameChanged;
}
