﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Meta.Numerics.Functions;

namespace Galaxy_Rotation_4.Model.Components.Bulge;

/// <summary>Модель распределения плотности на основе закона Серсика.</summary>
[ComponentCategory(ComponentCategory.Bulge)]
internal class SersicLaw : ScaleCentralDensityComponent
{
    private double _sersicIndex;
    private double _sersicScaleFactor;

    /// <summary>Конструктор.</summary>
    public SersicLaw() : base(Dimension.Surface)
    {
        UpdateSersicScaleFactor();
        MassCalculator.GetFullNotTruncatedMass = GetFullNotTruncatedMass;
    }

    /// <summary>Индекс Серсика.</summary>
    [Controlled]
    public double SersicIndex
    {
        get => _sersicIndex;
        set
        {
            if (_sersicIndex != value)
            {
                _sersicIndex = value;
                UpdateSersicScaleFactor();
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Параметр, представляющий множитель в законе Серсика.</summary>
    [DisplayOnly]
    public double SersicScaleFactor
    {
        get => _sersicScaleFactor;
        private set
        {
            if (_sersicScaleFactor != value)
            {
                _sersicScaleFactor = value;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Не усеченный закон распределения плотности.</summary>
    /// <param name="r">Рассматриваемый радиус</param>
    /// <returns>Плотность на рассматриваемом радиусе</returns>
    public override double DensityLaw(double r) => CentralDensityPerKiloparsecs * Math.Exp(-GetExponent(r));

    private double GetExponent(double r) => _sersicScaleFactor * Math.Pow(r / Scale, 1 / _sersicIndex);

    /// <summary>Обновляет множитель Серсика так, чтобы в пределах эффективного радиуса (шкалы компонента) излучалась половина светимости.</summary>
    private void UpdateSersicScaleFactor()
    {
        SersicScaleFactor = 2 * _sersicIndex - 1.0 / 3 + 4 / (405 * _sersicIndex) + 46 / (25515 * _sersicIndex * _sersicIndex);
    }

    /// <summary>Возвращает полную массу неусеченного компонента.</summary>
    /// <returns>Полная масса неусеченного компонента.</returns>
    private double GetFullNotTruncatedMass() => 2 *
        Math.PI *
        CentralDensityPerKiloparsecs *
        _sersicIndex *
        Scale *
        Scale *
        AdvancedMath.Gamma(2 * _sersicIndex) /
        Math.Pow(_sersicScaleFactor, 2 * _sersicIndex);

    // ToDo: Почему-то неправильно работает. Пока используется обычное интегрирование.
    /*protected override double CalcMainMass(double r)
    {
        return 2 * Math.PI * sersicIndex * centralDensity * scale * scale
               * AdvancedMath.Gamma(GetExponent(r), 2 * sersicIndex);

    }*/
}
