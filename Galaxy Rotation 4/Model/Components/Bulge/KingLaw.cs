﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Bulge;

/// <summary>Модель распределения на основе закона Кинга.</summary>
[ComponentCategory(ComponentCategory.Bulge)]
public class KingLaw : ScaleCentralDensityComponent
{
    public KingLaw() : base(Dimension.Volume)
    {
        MassCalculator.GetMainMass = CalcMainMass;
    }

    /// <summary>Закон распределения Кинга.</summary>
    /// <param name="r"></param>
    /// <returns></returns>
    public override double DensityLaw(double r)
    {
        double t = 1 + r * r / (Scale * Scale);

        return CentralDensityPerKiloparsecs / Math.Sqrt(t * t * t);
    }

    /// <summary>Возвращает массу в пределах заданного радиуса. Интеграл вычислен аналитически.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Масса.</returns>
    private double CalcMainMass(double r)
    {
        double t = r / Scale;
        double root = Math.Sqrt(1 + t * t);

        return 4 * Math.PI * CentralDensityPerKiloparsecs * Scale * Scale * Scale * (Math.Log(t + root) - t / root);
    }
}
