﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Bulge;

/// <summary>Модель распределения на основе закона Вакулера.</summary>
[ComponentCategory(ComponentCategory.Bulge)]
internal class Plummer : ScalableComponent
{
    private double _mass;

    public Plummer() : base(Dimension.Volume)
    {
        MassCalculator.GetMainMass = CalcMainMass;
    }

    /// <summary>Шкала в килопарсеках.</summary>
    [Controlled]
    public double FullMass
    {
        get => _mass;
        set
        {
            if (_mass != value)
            {
                _mass = value;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Профиль плотности Пламмера.</summary>
    /// <param name="r"></param>
    /// <returns></returns>
    public override double DensityLaw(double r)
    {
        double scaledRadius = r / Scale;
        double t = 1 + scaledRadius * scaledRadius;

        return 0.75 * FullMass / (Math.PI * Scale * Scale * Scale * Math.Sqrt(t * t * t * t * t));
    }

    /// <summary>Возвращает массу в пределах заданного радиуса. Интеграл вычислен аналитически.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Масса.</returns>
    private double CalcMainMass(double r)
    {
        double t = r * r + Scale * Scale;

        return FullMass * r * r * r / Math.Sqrt(t * t * t);
    }
}
