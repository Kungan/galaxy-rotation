﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Bulge;

/// <summary>Модель распределения на основе закона Вакулера.</summary>
[ComponentCategory(ComponentCategory.Bulge)]
internal class Vaucouleurs : ScaleCentralDensityComponent
{
    private const double V = -7.67;
    private const double V1 = 1 / V;
    private const double V2 = 1 / (V * V);
    private const double V3 = 1 / (V * V * V);
    private const double V4 = 1 / (V * V * V * V);
    private const double V5 = 1 / (V * V * V * V * V);
    private const double V6 = 1 / (V * V * V * V * V * V);
    private const double V7 = 1 / (V * V * V * V * V * V * V);
    private const double V8 = 1 / (V * V * V * V * V * V * V * V);

    public Vaucouleurs() : base(Dimension.Surface)
    {
        MassCalculator.GetFullNotTruncatedMass = GetFullNotTruncatedMass;
        MassCalculator.GetMainMass = CalcMainMass;
    }

    private double CalcMainMass(double r)
    {
        // Интегралы от плотности без учета постоянных.
        // Для радиуса равного 0.
        double Fa = -20160 * V8 * Scale * Scale;

        // Для радиуса равного r.
        double Fb = Math.Exp(V * Math.Pow(r / Scale, 0.25)) *
        (
            -20160 * V8 * Scale * Scale +
            20160 * V7 * Math.Pow(Scale, 1.75) * Math.Pow(r, 0.25) -
            10080 * V6 * Math.Pow(Scale, 1.50) * Math.Pow(r, 0.50) +
            3360 * V5 * Math.Pow(Scale, 1.25) * Math.Pow(r, 0.75) -
            840 * V4 * Scale * r +
            168 * V3 * Math.Pow(Scale, 0.75) * Math.Pow(r, 1.25) -
            28 * V2 * Math.Pow(Scale, 0.50) * Math.Pow(r, 1.50) +
            4 * V1 * Math.Pow(Scale, 0.25) * Math.Pow(r, 1.75)
        );

        // Вычисляем определенный интеграл, умножаем на вынесенные за интеграл константы.
        return 2 * Math.PI * CentralDensityPerKiloparsecs * (Fb - Fa);
    }

    public override double DensityLaw(double r) => CentralDensityPerKiloparsecs * Math.Exp(V * Math.Pow(r / Scale, 0.25));

    private double GetFullNotTruncatedMass() => 0.0106 * CentralDensityPerKiloparsecs * Scale * Scale;
}
