﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Halo;

[ComponentCategory(ComponentCategory.Halo)]
internal class ExponentialDarkHalo : ScalableComponent
{
    /// <summary>Центральная плотность.</summary>
    private double _darkMass;

    public ExponentialDarkHalo() : base(Dimension.Volume)
    {
        MassCalculator.GetMainMass = CalcMainMass;
    }

    /// <summary>Масса в солнечных массах.</summary>
    [Controlled]
    public double DarkMass
    {
        get => _darkMass; // Проверить, правильный ли коэфициент
        set
        {
            if (_darkMass != value)
            {
                _darkMass = value;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Закон распределения плотности.</summary>
    /// <param name="r">Радиус (парсек).</param>
    /// <returns>Плотность (M☉ / парсек^3).</returns>
    public override double DensityLaw(double r) //
        => _darkMass * Math.Exp(-r / Scale) / (8 * Math.PI * Scale * Scale * Scale * Math.E);

    /// <summary>Возвращает массу в пределах заданного радиуса. Интеграл вычислен аналитически.</summary>
    /// <param name="r"></param>
    /// <returns></returns>
    private double CalcMainMass(double r)
    {
        double t = r / Scale;

        return _darkMass * (1 / Math.E - Math.Exp(-1 - t) * (t * t / 2 + t + 1));
    }
}
