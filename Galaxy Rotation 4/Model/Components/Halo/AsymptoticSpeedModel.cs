﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Halo;

[ComponentCategory(ComponentCategory.Halo)]
internal class AsymptoticSpeedModel : ScalableComponent
{
    private double _asymptoticSpeed;

    /// <summary>Центральная плотность.</summary>
    private double _centralDensity;

    public AsymptoticSpeedModel() : base(Dimension.Volume)
    {
        MassCalculator.GetMainMass = CalcMainMass;
    }

    [Controlled]
    public double AsymptoticSpeed
    {
        get => _asymptoticSpeed;

        set
        {
            if (_asymptoticSpeed == value)
                return;

            _asymptoticSpeed = value;
            OnMainParameterChanged();
        }
    }

    private void RefreshCentralDensity()
    {
        _centralDensity = _asymptoticSpeed * _asymptoticSpeed / (4 * Math.PI * Scale * Scale * Galaxy.G);
    }

    /// <summary>Закон распределения плотности.</summary>
    /// <param name="r">Радиус (парсек).</param>
    /// <returns>Плотность (M☉ / парсек^3).</returns>
    public override double DensityLaw(double r) //
        => _centralDensity / (1 + r * r / (Scale * Scale));

    /// <summary>Возвращает массу в пределах заданного радиуса. Интеграл вычислен аналитически.</summary>
    /// <param name="r"></param>
    /// <returns></returns>
    private double CalcMainMass(double r)
    {
        double t = r / Scale;
        return 4 * Math.PI * _centralDensity * Scale * Scale * Scale * (t - Math.Atan(t));
    }

    protected override void OnMainParameterChanged()
    {
        RefreshCentralDensity();
        base.OnMainParameterChanged();
    }
}
