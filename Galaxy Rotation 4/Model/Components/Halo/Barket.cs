﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Halo;

[ComponentCategory(ComponentCategory.Halo)]
internal class Barket : ScaleCentralDensityComponent
{
    public Barket() : base(Dimension.Volume)
    {
        MassCalculator.GetMainMass = CalcMainMass;
    }

    /// <summary>Закон распределения плотности.</summary>
    /// <param name="r">Радиус (парсек).</param>
    /// <returns>Плотность (M☉ / парсек^3).</returns>
    public override double DensityLaw(double r) //
    {
        double t = r / Scale;

        return CentralDensityPerKiloparsecs / ((1 + t) * (1 + t * t));
    }

    /// <summary>Возвращает массу в пределах заданного радиуса. Интеграл вычислен аналитически.</summary>
    /// <param name="r"></param>
    /// <returns></returns>
    private double CalcMainMass(double r) => Math.PI *
        Scale *
        Scale *
        Scale *
        CentralDensityPerKiloparsecs *
        (Math.Log(Scale * Scale + r * r) +
            2 * Math.Log(Scale + r) -
            2 * Math.Atan(r / Scale) -
            4 * Math.Log(Scale));
}
