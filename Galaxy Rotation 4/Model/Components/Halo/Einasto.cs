﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Halo;

[ComponentCategory(ComponentCategory.Halo)]
internal class Einasto : ScaleCentralDensityComponent
{
    /// <summary>Шкала компонента в килопарсеках.</summary>
    private double _n;

    /// <summary>Шкала в килопарсеках.</summary>
    [Controlled]
    public double N
    {
        get => _n;
        set
        {
            if (_n != value)
            {
                _n = value;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Закон распределения плотности.</summary>
    /// <param name="r">Радиус (парсек).</param>
    /// <returns>Плотность (M☉ / парсек^3).</returns>
    public override double DensityLaw(double r) //
        => CentralDensityPerKiloparsecs * Math.Exp(2 * (1 - Math.Pow(r / Scale, _n)) / _n);
    
    /* TODO : посчитать интеграл
    protected override double CalcMainMass(double r)
    {
    }
    */
    public Einasto() : base(Dimension.Volume)
    {
    }
}
