﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Halo;

[ComponentCategory(ComponentCategory.Halo)]
internal class LogarithmicPotential : ScalableComponent
{
    private double _asympthoticSpeed;

    public LogarithmicPotential() : base(Dimension.Volume, new[] { TruncationType.Sharp, TruncationType.Linear, TruncationType.Exponential })
    {
        MassCalculator.GetMainMass = CalcMainMass;
    }

    /// <summary>Асимптотическая скорость.</summary>
    [Controlled]
    public double AsymptoticSpeed
    {
        get => _asympthoticSpeed;
        set
        {
            if (_asympthoticSpeed != value)
            {
                _asympthoticSpeed = value;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Закон распределения плотности в квазиизотермической модели.</summary>
    /// <param name="r">Радиус (парсек).</param>
    /// <returns>Плотность (M☉ / парсек^3).</returns>
    public override double DensityLaw(double r)
    {
        // ToDo: почему-то отрицательные плотности
        double A2 = Scale * Scale;
        double R2 = r * r;
        double t = A2 + R2;

        return _asympthoticSpeed * _asympthoticSpeed * (A2 - R2) / (2 * Math.PI * Galaxy.G * t * t);
    }

    /// <summary>Возвращает квадрат скорости на определенном расстоянии от центра.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Квадрат скорости.</returns>
    public override double CalcSquaredSpeed(double r) => 2 * _asympthoticSpeed * _asympthoticSpeed * r * r / (Scale * Scale + r * r);

    /// <summary>Возвращает массу в пределах заданного радиуса. Интеграл вычислен аналитически.</summary>
    private double CalcMainMass(double r)
    {
        //ToDo: проверить, почему не сходится c CalcSquaredSpeed Возможно, производная от потенциала вычислена неверно
        double A2 = Scale * Scale;
        return 2 * _asympthoticSpeed * _asympthoticSpeed * (2 * Scale * Math.Atan(r / Scale) - r * (1 + A2 / (A2 + r * r))) / Galaxy.G;
    }
}
