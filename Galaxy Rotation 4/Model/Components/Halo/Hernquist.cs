﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Halo;

[ComponentCategory(ComponentCategory.Halo)]
internal class Hernquist : ScalableComponent
{
    /// <summary>Масса.</summary>
    private double _mass;

    public Hernquist() : base(Dimension.Volume)
    {
        MassCalculator.GetMainMass = CalcMainMass;
    }

    // ToDo: Можно ли использовать в качестве полной массы?
    /// <summary>Масса в солнечных массах.</summary>
    [Controlled]
    public double Mass
    {
        get => _mass; // Проверить, правильный ли коэфициент
        set
        {
            if (_mass != value)
            {
                _mass = value;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Закон распределения плотности.</summary>
    /// <param name="r">Радиус (парсек).</param>
    /// <returns>Плотность (M☉ / парсек^3).</returns>
    public override double DensityLaw(double r) //
    {
        double t = r + Scale;
        return _mass * Scale / (2 * Math.PI * r * t * t * t);
    }

    /// <summary>Возвращает массу в пределах заданного радиуса. Интеграл вычислен аналитически.</summary>
    private double CalcMainMass(double r)
    {
        double t = Scale + r;
        return _mass * (1 - Scale * (Scale + 2 * r) / (t * t));
    }
}
