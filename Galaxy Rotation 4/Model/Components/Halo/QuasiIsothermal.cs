﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Halo;

[ComponentCategory(ComponentCategory.Halo)]
internal class QuasiIsothermal : ScaleCentralDensityComponent
{
    public QuasiIsothermal() : base(Dimension.Volume)
    {
        MassCalculator.GetMainMass = CalcMainMass;
    }

    /// <summary>Закон распределения плотности.</summary>
    /// <param name="r">Радиус (парсек).</param>
    /// <returns>Плотность (M☉ / парсек^3).</returns>
    public override double DensityLaw(double r) //
        => CentralDensityPerKiloparsecs / (1 + r * r / (Scale * Scale));

    /// <summary>Возвращает массу в пределах заданного радиуса. Интеграл вычислен аналитически.</summary>
    /// <param name="r"></param>
    /// <returns></returns>
    private double CalcMainMass(double r)
    {
        double t = r / Scale;
        return 4 * Math.PI * CentralDensityPerKiloparsecs * Scale * Scale * Scale * (t - Math.Atan(t));
    }
}
