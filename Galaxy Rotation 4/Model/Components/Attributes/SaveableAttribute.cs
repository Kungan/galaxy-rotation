﻿using System;

namespace Galaxy_Rotation_4.Model.Components.Attributes;

/// <summary>Свойства, которые помечены этим атрибутом (или его наследниками) будут сохранены.</summary>
[AttributeUsage(AttributeTargets.Property)]
public class SaveableAttribute : Attribute
{
}
