﻿using System;

namespace Galaxy_Rotation_4.Model.Components.Attributes;

[AttributeUsage(AttributeTargets.Property)]
internal class CanBeInfinity : Attribute
{
}
