﻿using System;

namespace Galaxy_Rotation_4.Model.Components.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field)]
internal class ComponentCategoryAttribute : Attribute
{
    public ComponentCategoryAttribute(ComponentCategory category)
    {
        Category = category;
    }

    public ComponentCategory Category { get; set; }
}
