﻿using System;

namespace Galaxy_Rotation_4.Model.Components.Attributes;

/// <summary>Атрибут свойств, контролирующих параметры усечения.</summary>
[AttributeUsage(AttributeTargets.Property)]
internal class ControlledTruncationAttribute : SaveableAttribute
{
}
