﻿using System;

namespace Galaxy_Rotation_4.Model.Components.Attributes;

/// <summary>Свойства, контролирующие параметры компонента (кроме параметров усчения).</summary>
[AttributeUsage(AttributeTargets.Property)]
internal class ControlledAttribute : SaveableAttribute
{
}
