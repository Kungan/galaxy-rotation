﻿namespace Galaxy_Rotation_4.Model.Components.Attributes;

public enum ComponentCategory
{
    Bulge,
    Halo,
    ThinDisk,
    WideDisk,
}
