﻿using System.Collections.Generic;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components;

public abstract class ScalableComponent : Component
{
    /// <summary>Шкала компонента в килопарсеках.</summary>
    private double _scale;

    protected ScalableComponent(Dimension dimension, IEnumerable<TruncationType> nonAvailableTruncTypes = null)
        : base(dimension, nonAvailableTruncTypes)
    {
    }

    /// <summary>Шкала в килопарсеках.</summary>
    [Controlled]
    public double Scale
    {
        get => _scale;
        set
        {
            if (_scale != value)
            {
                _scale = value;
                OnMainParameterChanged();
            }
        }
    }
}
