﻿using System;
using System.Collections.Generic;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.Model.Components.MassCalculation;
using Galaxy_Rotation_4.Model.Interfaces;

namespace Galaxy_Rotation_4.Model.Components;

/// <summary> Родительский класс для всех компонентов, описывающих распределение вещества в галактике. </summary>
public abstract class Component : IParameterizable
{
    /// <summary>Включен ли компонент.</summary>
    private bool _isEnabled;

    protected Component(Dimension dimension, IEnumerable<TruncationType> nonAvailableTruncTypes = null)
    {
        Dimension = dimension;
        Truncation = new Truncation(DensityLaw, nonAvailableTruncTypes);
        Truncation.TypeChanged += OnAnyParameterChanged;
        Truncation.ScaleChanged += OnAnyParameterChanged;
        Truncation.RadiusChanged += OnMainParameterChanged;

        // ToDo: использовать библиотеку интегрирования, настройки точности вынести в настройки
        double Integrate(Func<double, double> func, double left, double right) => Functions.Integrate(func, 0, right, PlotParams.Step);

        switch (dimension)
        {
            case Dimension.Surface:
                MassCalculator = new SurfaceMassCalculator(Truncation, DensityLaw, Integrate);
                DensityCoefficient = Math.Pow(1000, 2);
                break;
            case Dimension.Volume:
                MassCalculator = new VolumeMassCalculator(Truncation, DensityLaw, Integrate);
                DensityCoefficient = Math.Pow(1000, 3);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(dimension), dimension, "Unsupported dimensions");
        }
    }

    public Dimension Dimension { get; }

    /// <summary>Параметры усечения.</summary>
    public Truncation Truncation { get; }

    /// <summary>Параметры построения.</summary>
    public PlotParams PlotParams { get; set; }

    public Curve SpeedCurve { get; } = new();

    public Curve SquaredSpeedCurve { get; } = new();

    public Curve DensityCurve { get; } = new();

    /// <summary>
    ///     Законы распределения плотности возвращают плотность в массах Солнца на кубический или квадратный КИЛОПАРСЕК. Делением на данный коэффициент
    ///     плотность приводится к массе Солнца на кубический или квадратный ПАРСЕК.
    /// </summary>
    protected double DensityCoefficient { get; }

    protected MassCalculator MassCalculator { get; }

    [Saveable]
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (_isEnabled == value)
                return;

            _isEnabled = value;
            OnEnabledChanged();
        }
    }

    /// <summary>Хранит тип усечения.</summary>
    [ControlledTruncation]
    public TruncationType TruncationType
    {
        get => Truncation.Type;
        set => Truncation.Type = value;
    }

    /// <summary>Радиус усечения в килопарсеках.</summary>
    [ControlledTruncation]
    public virtual double TruncationRadius
    {
        get => Truncation.Radius;
        set => Truncation.Radius = value;
    }

    /// <summary>Шкала усечения в килопарсеках.</summary>
    [ControlledTruncation]
    public double TruncationScale
    {
        get => Truncation.Scale;
        set => Truncation.Scale = value;
    }

    public event Action ParametersChanged;
    public event Action EnabledChanged;
    public event Action<IntegrationException> IntegrationErrorOccured;

    /// <summary>Закон распределения плотности.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Плотность в массах солнца на кубический или квадратный КИЛОпарсек.</returns>
    public abstract double DensityLaw(double r);

    /// <summary>Возвращает полную массу компонента.</summary>
    public double GetFullMass() => MassCalculator.GetFullMass();

    /// <summary>Возвращает массу компонента в заданном радиусе.</summary>
    public double GetMass(double r) => MassCalculator.GetMass(r);

    /// <summary>Усеченный закон распределения плотности.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Усеченная плотность (масс Солнца на кубический / квадратный ПАРСЕК)</returns>
    private double CalcTruncatedDensity(double r) => Truncation.CalcTruncatedDensity(r) / DensityCoefficient;

    private bool IsDensityEnabled()
    {
        return Dimension switch
        {
            Dimension.Surface => PlotParams.SurfaceDensityEnabled,
            Dimension.Volume => PlotParams.VolumeDensityEnabled,
            _ => throw new NotSupportedException($"Dimension type {Dimension} is not supprted."),
        };
    }

    private void OnEnabledChanged()
    {
        SpeedCurve.IsEnabled = IsEnabled;
        SquaredSpeedCurve.IsEnabled = IsEnabled;
        DensityCurve.IsEnabled = IsEnabled;

        EnabledChanged?.Invoke();

        // Если компонент стал из неактивного активным, то нужно перестроить кривые.
        if (IsEnabled)
            Plot();
    }

    /// <summary>Вычисляет квадрат скорости на определенном расстоянии от центра.</summary>
    /// <param name="r">Радиус (килопарсек).</param>
    /// <returns>Квадрат скорости.</returns>
    public virtual double CalcSquaredSpeed(double r)
    {
        // ToDo: Костыль, возможно потребуется исправить.
        if (r == 0)
            return 0;

        // ToDo: неверно для дисковых компонент. Пока метод в них переопределяется, но надо пофиксить.
        return Galaxy.G * GetMass(r) / r;
    }

    /// <summary>Возвращает плотность на заданном радиусе от центра.</summary>
    /// <param name="r">Радиус (килопарсек).</param>
    /// <returns>Плотность (масс Солнца на кубический / квадратный парсек).</returns>
    public double GetTruncatedDensity(double r)
    {
        // ToDo: добавить аппроксимацию. аналогично для скорости.
        if (DensityCurve.Dots.TryGetValue(r, out double density))
            return density;

        return CalcTruncatedDensity(r);
    }

    public void Plot()
    {
        if (!IsEnabled)
            return;

        if (PlotParams.SpeedEnabled)
            RePlotSpeed();

        if (IsDensityEnabled())
            RePlotDensity();
    }

    /// <summary>
    ///     Вызывается при изменении основных параметров компонента, влияющих на его часть до радиуса усечения включительно. Не нужно вызывать при
    ///     изменении параметров усечения (кроме радиуса усечения).
    /// </summary>
    protected virtual void OnMainParameterChanged()
    {
        Truncation.UpdateStartDensity();
        OnAnyParameterChanged();
    }

    /// <summary>Вызывается при изменении любых параметров компонента.</summary>
    private void OnAnyParameterChanged()
    {
        Plot();
        ParametersChanged?.Invoke();
    }

    private void RePlotDensity()
    {
        var dots = new Dictionary<double, double>();

        for (double r = 0; r <= PlotParams.RadiusMax; r += PlotParams.Step)
            dots.Add(r, CalcTruncatedDensity(r));

        DensityCurve.Dots = dots;
    }

    /// <summary>Строит график распределения квадратов скоростей если компонент активен.</summary>
    private void RePlotSpeed()
    {
        // ToDo: исправить в случае, если появятся компоненты с ненулевой скоростью в 0.
        var dotsSquared = new Dictionary<double, double> { { 0, 0 } };
        var dots = new Dictionary<double, double> { { 0, 0 } };

        for (double r = PlotParams.Step; r <= PlotParams.RadiusMax; r += PlotParams.Step)
        {
            try
            {
                double squaredSpeed = CalcSquaredSpeed(r);
                dotsSquared.Add(r, squaredSpeed);
                dots.Add(r, Math.Sqrt(squaredSpeed));
            }
            catch (IntegrationException e)
            {
                IntegrationErrorOccured?.Invoke(e);
            }
        }

        SquaredSpeedCurve.Dots = dotsSquared;
        SpeedCurve.Dots = dots;
    }
}
