﻿namespace Galaxy_Rotation_4.Model;

// ToDo: change to flags
/// <summary>Тип усечения.</summary>
public enum TruncationType
{
    None,
    Sharp,
    Linear,
    Exponential,
}
