﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.ViewModel.Resources;

namespace Galaxy_Rotation_4.Model.Components;

public static class ComponentExtension
{
    /// <summary>Возвращает название закона распределения плотности.</summary>
    public static ComponentCategory GetCategory(this Component c)
    {
        Type type = c.GetType();
        Type attrType = typeof(ComponentCategoryAttribute);

        if (Attribute.IsDefined(type, attrType)) // проверка на существование атрибута
        {
            var attribute = Attribute.GetCustomAttribute(type, attrType) as ComponentCategoryAttribute; // получаем значение атрибута

            return attribute.Category; // возвращаем значение атрибута
        }

        throw new ArgumentException("Attrubute was not found.");
    }

    /// <summary>Возвращает названия и значения свойств компонента, управляемых пользователем (помеченных аттрибутом [Controlled]).</summary>
    public static string ControlledParametersToString(this Component component)
    {
        Type componentType = component.GetType();

        IEnumerable<PropertyInfo> controlledProperties =
            componentType.GetProperties().Where(property => property.IsDefined(typeof(ControlledAttribute)));

        var result = new StringBuilder();

        foreach (PropertyInfo property in controlledProperties)
        {
            object propertyValue = property.GetValue(component);
            Type propertyType = propertyValue.GetType();
            string value;

            if (propertyType.IsEnum)
            {
                value = ((Enum)propertyValue).GetLocalizedName();
            }
            else
            {
                value = property.GetValue(component).ToString();
            }

            string propertyName = PropertyLocalizator.GetLocalizedPropertyName(property);
            result.Append($"{propertyName}: {value}\r\n");
        }

        return result.ToString();
    }

    /// <summary>Полное имя компонента: "Тип компонента, закон распределения (тип усечения)".</summary>
    public static string GetFullName(this Component component)
    {
        string truncName = component.TruncationType.GetLocalizedName();
        string truncWord = truncName == "нет" ? "усечения" : "усечение";
        string categoryName = component.GetCategory().GetLocalizedName();
        string componentName = Functions.DeCapitalizeFirstLetter(GetShortName(component));

        return $"{categoryName}, {componentName}, {truncName}  {truncWord}";
    }

    /// <summary>Краткое имя компонента (закон распределения плотности).</summary>
    public static string GetShortName(this Component c) => ComponentLocalizator.GetLocalizedName(c.GetType());
}
