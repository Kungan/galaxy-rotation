using System;
using System.Collections.Generic;
using System.Linq;

namespace Galaxy_Rotation_4.Model.Components;

/// <summary>Представляет собой усечение</summary>
public class Truncation
{
    private double _radius;
    private double _scale;
    private double _truncatedDensityAtR;
    private TruncationType _type;

    public Truncation(Func<double, double> densityLaw, IEnumerable<TruncationType> nonAvailableTypes)
    {
        DensityLaw = densityLaw;
        NonAvailableTypes = nonAvailableTypes ?? Array.Empty<TruncationType>();
        Type = AvailableTypes.First();
    }

    private Func<double, double> DensityLaw { get; }

    /// <summary>Виды усечения, недоступные для компонента.</summary>
    public IEnumerable<TruncationType> NonAvailableTypes { get; }

    public IEnumerable<TruncationType> AvailableTypes => Enum.GetValues(typeof(TruncationType)).Cast<TruncationType>().Except(NonAvailableTypes);

    /// <summary>Хранит тип усечения.</summary>
    public TruncationType Type
    {
        get => _type;
        set
        {
            if (_type == value)
                return;

            if (NonAvailableTypes.Contains(value))
                throw new ArgumentException($"Truncation type {value} is not available.");

            _type = value;
            TypeChanged?.Invoke();
        }
    }

    /// <summary>Радиус усечения в килопарсеках.</summary>
    public double Radius
    {
        get => _radius;
        set
        {
            if (_radius == value)
                return;

            _radius = value;
            RadiusChanged?.Invoke();
        }
    }

    /// <summary>Шкала усечения в килопарсеках.</summary>
    public double Scale
    {
        get => _scale;
        set
        {
            if (_scale == value)
                return;

            _scale = value;
            ScaleChanged?.Invoke();
        }
    }

    /// <summary>Плотность на радиусе усечения. Вынесена в отдельное поле, чтобы каждый раз не пересчитывать.</summary>
    public double StartDensity
    {
        get => _truncatedDensityAtR;
        private set
        {
            if (_truncatedDensityAtR == value)
                return;

            _truncatedDensityAtR = value;
            StartDensityChanged?.Invoke();
        }
    }

    public event Action TypeChanged;
    public event Action RadiusChanged;
    public event Action ScaleChanged;
    public event Action StartDensityChanged;

    public void UpdateStartDensity()
    {
        StartDensity = DensityLaw(Radius);
    }
    
    /// <summary>Усеченный закон распределения плотности.</summary>
    /// <param name="r">Радиус.</param>
    public double CalcTruncatedDensity(double r)
    {
        double result;

        if (r <= Radius)
        {
            result = DensityLaw(r);
        }
        else
        {
            result = Type switch
            {
                TruncationType.None => DensityLaw(r),
                TruncationType.Sharp => 0,
                TruncationType.Linear => GetLinearlyTruncatedDensity(r),
                TruncationType.Exponential => GetExponentiallyTruncatedDensity(r),
                _ => throw new Exception($"Unexpected truncation type {Type}"),
            };
        }

        return result;
    }

    /// <summary>Возвращает плотность вещества на заданном радиусе для линейного усечения.</summary>
    /// <param name="r">Радиус (килопарсек)</param>
    /// <returns>Плотность.</returns>
    /// <exception cref="ArgumentException">Выбрасывается, если переданный радиус меньше радиуса, с которого начинается усечение.</exception>
    private double GetLinearlyTruncatedDensity(double r)
    {
        if (r < Radius)
            throw new ArgumentException($"Truncation cannot be calculated on radius {r} smaller than truncation radius {Radius}");

        if (r > Radius + Scale)
            return 0;

        return StartDensity * (1 - (r - Radius) / Scale);
    }

    /// <summary>Возвращает плотность вещества на заданном радиусе для экспоненциального усечения.</summary>
    /// <param name="r">Радиус (килопарсек)</param>
    /// <returns>Плотность.</returns>
    /// <exception cref="ArgumentException">Выбрасывается, если переданный радиус меньше радиуса, с которого начинается усечение.</exception>
    private double GetExponentiallyTruncatedDensity(double r)
    {
        if (r < Radius)
            throw new ArgumentException($"Truncation cannot be calculated on radius {r} smaller than truncation radius {Radius}");

        return StartDensity * Math.Exp((Radius - r) / Scale);
    }
}
