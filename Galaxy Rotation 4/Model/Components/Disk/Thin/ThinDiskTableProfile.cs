﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Meta.Numerics.Analysis;
using Meta.Numerics.Functions;

namespace Galaxy_Rotation_4.Model.Components.Disk.Thin;

/// <summary>Модель тонкого диска с табличным распределением плотности.</summary>
[ComponentCategory(ComponentCategory.ThinDisk)]
public class ThinDiskTableProfile : Component
{
    private readonly TableProfile _tableProfile;
    private double _coefficient = 1;
    private int _stepsCount;

    public ThinDiskTableProfile() : base(Dimension.Surface)
    {
        _tableProfile = new TableProfile(DensityCoefficient);
        MassCalculator.GetFullNotTruncatedMass = () => MassCalculator.GetMainMass(_tableProfile.RaduisMax);
    }

    // График зависимости скорости от радиуса.
    [Controlled]
    public Curve DensityProfile
    {
        get => _tableProfile.DensityProfile;
        set
        {
            _tableProfile.DensityProfile = value;
            OnMainParameterChanged();
        }
    }

    [Controlled]
    public int StepsCount
    {
        get => _stepsCount;
        set
        {
            _stepsCount = value;
            OnMainParameterChanged();
        }
    }

    // Коэфициент, на который умножается плотность из графика.
    [Controlled]
    public double Coefficient
    {
        get => _coefficient;
        set
        {
            if (_coefficient != value)
            {
                _coefficient = value;
                OnMainParameterChanged();
            }
        }
    }

    public override double CalcSquaredSpeed(double r)
    {
        if (_tableProfile == null || _tableProfile.RaduisMax == 0)
        {
            return 0;
        }

        IntegrationResult result = FunctionMath.Integrate(x => IntegrationDelegate(x, r),
            0,
            _tableProfile.RaduisMax,
            new IntegrationSettings { EvaluationBudget = 500000, RelativePrecision = 1.0E-6 });

        return result.Value;
    }

    public override double DensityLaw(double r)
    {
        if (_tableProfile == null)
        {
            return 0;
        }

        return _coefficient * _tableProfile.GetValueAt(r);
    }

    //Формула Грина. Вычисляет эквивалент интеграла от 0 до бесконечности следующего выражения: 
    //  ∫t * J0(r * t) * J1(R * t)dt,
    //  где J0 и J1 - функции Бесселя 0-го и 1-го порядка соответственно.
    private static double GreensFormula(double r, double R)
    {
        double result;
        double k;
        double K;
        double E;

        if (r == R)
        {
            return 0;
        }

        if (R < r)
        {
            k = R / r;
            K = AdvancedMath.EllipticK(k);
            E = AdvancedMath.EllipticE(k);
            result = 2 * (K - E / (1 - k * k)) / (Math.PI * r * R);
        }
        else
        {
            k = r / R;
            E = AdvancedMath.EllipticE(k);
            result = 2 * E / (Math.PI * R * R * (1 - k * k));
        }

        return result;
    }

    private double IntegrationDelegate(double r, double R)
    {
        double density = DensityLaw(r);
        double result = r * density * GreensFormula(r, R);

        return result;
    }
}
