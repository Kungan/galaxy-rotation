﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.Model.Components.MassCalculation;
using static alglib;

namespace Galaxy_Rotation_4.Model.Components.Disk.Thin;

/// <summary>Модель распределения тонкого экспоненциального диска.</summary>
[ComponentCategory(ComponentCategory.ThinDisk)]
internal class ThinExponentialDisk : ScaleCentralDensityComponent
{
    public ThinExponentialDisk()  : base(Dimension.Surface, new[] { TruncationType.Sharp, TruncationType.Linear, TruncationType.Exponential })
    {
        MassCalculator.GetFullNotTruncatedMass = GetFullNotTruncatedMass;
        MassCalculator.GetMainMass = CalcMainMass;
    }

    public override double CalcSquaredSpeed(double r)
    {
        double y = r / (2 * Scale);
        return Math.PI * Galaxy.G * CentralDensityPerKiloparsecs * r * r * (besseli0(y) * besselk0(y) - besseli1(y) * besselk1(y)) / Scale;
    }

    public override double DensityLaw(double r) => CentralDensityPerKiloparsecs * Math.Exp(-r / Scale);

    private double CalcMainMass(double r) => ExponentialDiskBehavior.CalcMainMass(r, CentralDensityPerKiloparsecs, Scale);

    private double GetFullNotTruncatedMass() => ExponentialDiskBehavior.GetNotTruncatedMass(CentralDensityPerKiloparsecs, Scale);
}
