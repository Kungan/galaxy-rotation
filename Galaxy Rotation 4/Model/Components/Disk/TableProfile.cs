﻿using System.Linq;

namespace Galaxy_Rotation_4.Model.Components.Disk;

/// <summary>Класс табличного профиля с интерполяцией кубическим сплайном.</summary>
internal class TableProfile
{
    private readonly double _convertingCoefficient;
    private Curve _densityProfile;
    private alglib.spline1dinterpolant _spline;

    public TableProfile(double densityCoefficient)
    {
        _convertingCoefficient = densityCoefficient;
    }

    public Curve DensityProfile
    {
        get => ConvertGraphicUnits(_densityProfile, 1 / _convertingCoefficient);
        set
        {
            Curve convertedCurve = ConvertGraphicUnits(value, _convertingCoefficient);
            _densityProfile = convertedCurve;
            RefreshSpline();
            RefreshRadiusMax();
        }
    }

    public double RaduisMax { get; private set; }

    private Curve ConvertGraphicUnits(Curve curve, double coefficient)
    {
        return new Curve(curve.Dots.ToDictionary(dot => dot.Key, dot => dot.Value * coefficient));
    }

    public double GetValueAt(double r)
    {
        if (_spline == null)
        {
            return 0;
        }

        double result = alglib.spline1dcalc(_spline, r);

        return result;
    }

    private void RefreshSpline()
    {
        if (IsEmpty())
        {
            _spline = null;

            return;
        }

        double[] keys = _densityProfile.Dots.Keys.ToArray();
        double[] values = _densityProfile.Dots.Values.ToArray();
        alglib.spline1dbuildcubic(keys, values, out _spline);
    }

    private bool IsEmpty() => !_densityProfile?.Dots?.Any() ?? true;

    //return densityProfile == null || densityProfile.Dots == null || !densityProfile.Dots.Any();
    public double GetDerivation(double r)
    {
        if (_spline == null)
        {
            return 0;
        }

        alglib.spline1ddiff(_spline,
            r,
            out _,
            out double ds,
            out _);

        return ds;
    }

    private void RefreshRadiusMax()
    {
        if (_spline == null)
        {
            RaduisMax = 0;

            return;
        }

        RaduisMax = _densityProfile.Dots.Keys.Max();
    }
}
