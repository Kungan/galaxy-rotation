﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Meta.Numerics;
using Meta.Numerics.Analysis;
using Meta.Numerics.Functions;

namespace Galaxy_Rotation_4.Model.Components.Disk.Wide;

public abstract class CasertanoDisk : WideDisk
{
    private readonly IntegrationSettings _integrationSettings = new() { RelativePrecision = 1E-5 };
    private IntegralPrecision _precision = IntegralPrecision.Good;

    protected CasertanoDisk(IEnumerable<TruncationType> nonAvailableTruncTypes = null) : base(nonAvailableTruncTypes)
    {
    }

    [Controlled]
    public IntegralPrecision Precision
    {
        get => _precision;
        set
        {
            _integrationSettings.RelativePrecision = value switch
            {
                IntegralPrecision.Best => 1E-6,
                IntegralPrecision.Good => 1E-5,
                IntegralPrecision.Middle => 1E-4,
                IntegralPrecision.BelowMiddle => 1E-3,
                IntegralPrecision.Bad => 1E-2,
                IntegralPrecision.Worst => 1E-1,
                _ => throw new ArgumentException($"Precision {value} is not currently supported."),
            };

            _precision = value;
            OnMainParameterChanged();
        }
    }

    public override double CalcSquaredSpeed(double r)
    {
        if (r == 0)
            return 0;

        return -r * CasertanoFormula(r);
    }

    /// <summary>Распределение объемной плотности диска в плоскости z = 0.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Значение плотности на расстоянии r. В массах Солнца на кубический парсек.</returns>
    protected abstract double MainDensityProfileDerivation(double r);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private double CasertanoFormula(double r)
    {
        try
        {
            IntegrationResult integral = FunctionMath.Integrate(u => RadialIntegrationDelegate(u, r),
                0,
                RadialIntegrationLimit,
                _integrationSettings);

            double result = 4 * Math.PI * Galaxy.G * integral.Value;

            return result;
        }
        catch (NonconvergenceException e)
        {
            throw new IntegrationException("Интеграл не сходится. Попробуйте понизить точность интегрирования.", e);
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private double GetTruncatedDerivation(double r)
    {
        if (TruncationType == TruncationType.None || r <= TruncationRadius)
        {
            return MainDensityProfileDerivation(r);
        }

        switch (TruncationType)
        {
            case TruncationType.Sharp:
                return 0;
            case TruncationType.Linear:
                if (r >= TruncationRadius + TruncationScale)
                    return 0;

                return -VolumeDensityAtTruncRadius / TruncationScale;
            case TruncationType.Exponential:
                return -VolumeDensityAtTruncRadius * Math.Exp((TruncationRadius - r) / TruncationScale) / TruncationScale;
            case TruncationType.None:
            default:
                throw new Exception("Не могу вычислить производную для данного типа усечения.");
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private double PartialDerivation(double r, double z)
    {
        double derivation = GetTruncatedDerivation(r);
        double zCoefficient = VerticalDistribution(z);

        return derivation * zCoefficient;
    }

    private double RadialIntegrationDelegate(double u, double currentR)
    {
        IntegrationResult integral = FunctionMath.Integrate(z => VerticalIntegrationFormula(u, z, currentR),
            0,
            VerticalIntegrationLimit,
            _integrationSettings);

        return u * integral.Value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private double VerticalIntegrationFormula(double u, double z, double currentR)
    {
        double x = (currentR * currentR + u * u + z * z) / (2 * currentR * u);
        double p = x - Math.Sqrt(x * x - 1);
        double partialDerivation = PartialDerivation(u, z);

        //ToDo: Эллиптические интегралы от 1 равны бесконечности. Что делать?
        //p = 1 при r = u (к примеру, 49.5), z = 0
        double K = AdvancedMath.EllipticK(p);
        double E = AdvancedMath.EllipticE(p);
        double divider = Math.PI * Math.Sqrt(currentR * u * p);
        double result = partialDerivation * 2 * (K - E) / divider;

        return result;
    }
}
