﻿namespace Galaxy_Rotation_4.Model.Components.Disk.Wide;

public enum IntegralPrecision
{
    Best,
    Good,
    Middle,
    BelowMiddle,
    Bad,
    Worst,
}
