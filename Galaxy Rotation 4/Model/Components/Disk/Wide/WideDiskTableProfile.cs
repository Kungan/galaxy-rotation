﻿using System;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Disk.Wide;

/// <summary>Модель толстого диска с табличным распределением плотности.</summary>
[ComponentCategory(ComponentCategory.WideDisk)]
public class WideDiskTableProfile : CasertanoDisk
{
    private readonly TableProfile _tableProfile;
    private double _coefficient;

    /// <summary>
    ///     Не разрешаем компоненту быть не усеченным, так как не понятно, какая плотность у неусеченного компонента может быть за пределами максимального
    ///     радиуса загруженного табличного профиля плотности.
    /// </summary>
    public WideDiskTableProfile() : base(new[] { TruncationType.None })
    {
        _tableProfile = new TableProfile(DensityCoefficient);
        MassCalculator.GetFullNotTruncatedMass = () => MassCalculator.GetMainMass(_tableProfile.RaduisMax);
    }

    /// <summary>
    ///     Радиус усечения в килопарсеках. Переопределение базового свойства необходимо для того, чтобы не позволять ему быть больше максимального радиуса
    ///     загруженного табличного профиля плотности.
    /// </summary>
    public override double TruncationRadius
    {
        get => base.TruncationRadius;
        set => base.TruncationRadius = Math.Min(value, _tableProfile.RaduisMax);
    }

    /// <summary>График зависимости плотности от радиуса.</summary>
    [Controlled]
    public Curve DensityProfile
    {
        get => _tableProfile.DensityProfile;
        set
        {
            _tableProfile.DensityProfile = value;
            RadialIntegrationLimit = _tableProfile.RaduisMax;
            TruncationRadius = _tableProfile.RaduisMax;
            OnMainParameterChanged();
        }
    }

    /// <summary>Коэфициент, на который умножается плотность из графика.</summary>
    [Controlled]
    public double Coefficient
    {
        get => _coefficient;
        set
        {
            if (_coefficient == value)
                return;

            _coefficient = value;
            OnMainParameterChanged();
        }
    }

    public override double DensityLaw(double r) => _coefficient * _tableProfile?.GetValueAt(r) ?? 0;

    /// <summary>Распределение объемной плотности диска в плоскости z = 0.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Производная плотности.</returns>
    protected override double MainDensityProfileDerivation(double r)
    {
        if (_tableProfile.GetValueAt(r) < 0)
            return 0;

        return _coefficient * _tableProfile.GetDerivation(r) / (2 * VerticalScale);
    }
}
