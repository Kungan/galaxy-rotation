﻿using System;
using System.Runtime.CompilerServices;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.Model.Components.MassCalculation;

namespace Galaxy_Rotation_4.Model.Components.Disk.Wide;

/// <summary>Модель распределения толстого диска.</summary>
[ComponentCategory(ComponentCategory.WideDisk)]
public class WideExponentialDisk : CasertanoDisk
{
    /// <summary>Центральная плотность на квадратный КИЛОПАРСЕК.</summary>
    private double _centralDensityPerSquaredKpc;

    /// <summary>Шкала компонента в килопарсеках.</summary>
    private double _scale;

    public WideExponentialDisk()
    {
        MassCalculator.GetFullNotTruncatedMass = GetFullNotTruncatedMass;
        MassCalculator.GetMainMass = CalcMainMass;
    }

    /// <summary>Центральная плотность в солнечных массах на квадратный ПАРСЕК (из-за этого множитель).</summary>
    [Controlled]
    public double CentralDensity
    {
        // Возвращаем плотность в массах Солнца на квадратный ПАРСЕК.
        get => _centralDensityPerSquaredKpc / DensityCoefficient;
        set
        {
            // Устанавливаем плотность в массах солнца на квадратный КИЛОПАРСЕК.
            double newValue = value * DensityCoefficient;

            if (_centralDensityPerSquaredKpc == newValue)
            {
                return;
            }

            _centralDensityPerSquaredKpc = newValue;
            OnMainParameterChanged();
        }
    }

    // Шкала в килопарсеках.
    [Controlled]
    public double Scale
    {
        get => _scale;
        set
        {
            if (_scale != value)
            {
                _scale = value;
                OnMainParameterChanged();
            }
        }
    }

    private double CalcMainMass(double r) => ExponentialDiskBehavior.CalcMainMass(r, _centralDensityPerSquaredKpc, _scale);

    /// <summary>
    ///     Закон распределения поверхностной плотности. Внимание: это не закон распределения объемной плотности в нулевой плоскости диска. Это именно
    ///     интегральная величина по всей толщине диска.
    /// </summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Плотность в массах Солнца на квадратный килопарсек.</returns>
    public override double DensityLaw(double r) => _centralDensityPerSquaredKpc * Math.Exp(-r / _scale);

    private double GetFullNotTruncatedMass() => ExponentialDiskBehavior.GetNotTruncatedMass(_centralDensityPerSquaredKpc, _scale);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected override double MainDensityProfileDerivation(double r)
    {
        double centralVolumeDensity = _centralDensityPerSquaredKpc / (2 * VerticalScale);

        return centralVolumeDensity * Math.Exp(-r / _scale) / -_scale;
    }
}
