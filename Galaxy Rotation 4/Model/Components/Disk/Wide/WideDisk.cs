﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components.Disk.Wide;

/// <summary>Базовый класс, представляющий толстый диск.</summary>
public abstract class WideDisk : Component
{
    private double _radialIntegrationLimit = double.PositiveInfinity;
    private double _verticalIntegrationLimit = double.PositiveInfinity;
    private double _verticalScale;

    protected WideDisk(IEnumerable<TruncationType> nonAvailableTruncTypes = null) : base(Dimension.Surface, nonAvailableTruncTypes)
    {
        Truncation.StartDensityChanged += UpdateVolumeDensityAtTruncRadius;
    }

    /// <summary>Вертикальная шкала в килопарсеках.</summary>
    [Controlled]
    public double VerticalScale
    {
        get => _verticalScale;
        set
        {
            if (_verticalScale != value)
            {
                _verticalScale = value;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Вертикальный предел интегрирования в килопарсеках.</summary>
    [Controlled]
    [CanBeInfinity]
    public double VerticalIntegrationLimit
    {
        get => _verticalIntegrationLimit;
        set
        {
            if (_verticalIntegrationLimit != value)
            {
                _verticalIntegrationLimit = value;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Радиальный предел интегрирования в килопарсеках.</summary>
    [Controlled]
    [CanBeInfinity]
    public double RadialIntegrationLimit
    {
        get => _radialIntegrationLimit;
        set
        {
            if (_radialIntegrationLimit != value)
            {
                _radialIntegrationLimit = value;
                OnMainParameterChanged();
            }
        }
    }

    protected double VolumeDensityAtTruncRadius { get; private set; }
    

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected double VerticalDistribution(double z)
    {
        double hiperbolicCosine = Functions.Ch(z / _verticalScale);
        double zCoefficient = 1 / (hiperbolicCosine * hiperbolicCosine);

        return zCoefficient;
    }

    private void UpdateVolumeDensityAtTruncRadius()
    {
        VolumeDensityAtTruncRadius = Truncation.StartDensity / (2 * _verticalScale);
    }
}
