namespace Galaxy_Rotation_4.Model.Components;

/// <summary>
///     Представляет собой компонент, использующий модифицированную ньютоновскую динамику (MOND). Модифцирует поведение обчного ньютоновского компонента.
/// </summary>
public sealed class ComponentMond : Component
{
    private readonly Component _component;

    public ComponentMond(Component component) : base(component.Dimension, component.Truncation.NonAvailableTypes)
    {
        _component = component;
    }

    public override double DensityLaw(double r) => _component.DensityLaw(r);

    public override double CalcSquaredSpeed(double r)
    {
        // ToDo: Костыль, возможно потребуется исправить.
        if (r == 0)
            return 0;

        return GalaxyMond.ConvertToSquaredMondSpeed(_component.CalcSquaredSpeed(r), r);
    }
}
