namespace Galaxy_Rotation_4.Model.Components;

public enum Dimension
{
    Surface,
    Volume
}
