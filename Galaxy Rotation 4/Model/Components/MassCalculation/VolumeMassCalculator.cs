using System;

namespace Galaxy_Rotation_4.Model.Components.MassCalculation;

public sealed class VolumeMassCalculator: MassCalculator
{
    /// <summary>Возвращает основную массу компонента в заданном радиусе.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Масса.</returns>
    protected override double IntegrateMainMass(double r)
    {
        double Func(double x) => 4 * Math.PI * x * x * DensityLaw(x);

        //ToDo: использовать рассчитанную плотность при необходимости
        return Integrator(Func, 0, r);
    }

    protected override double GetTruncationMass(double r)
    {
        if (r <= Truncation.Radius)
            return 0;

        switch (Truncation.Type)
        {
            case TruncationType.None:
                return 0;
            case TruncationType.Sharp:
                return 0;
            case TruncationType.Linear:
                if (r >= Truncation.Radius + Truncation.Scale)
                    return GetFullTruncationMass();

                return Math.PI *
                    Truncation.StartDensity *
                    (r * r * r * (4 * (Truncation.Scale + Truncation.Radius) - 3 * r) -
                        Truncation.Radius * Truncation.Radius * Truncation.Radius * (4 * Truncation.Scale + Truncation.Radius)) /
                    (3 * Truncation.Scale);

            case TruncationType.Exponential:
                double squaredScale = Truncation.Scale * Truncation.Scale;
                double rScaleSum = Truncation.Scale + r;
                double scaleRadiusSum = Truncation.Scale + Truncation.Radius;

                return 4 *
                    Math.PI *
                    Truncation.Scale *
                    Truncation.StartDensity *
                    (squaredScale +
                        scaleRadiusSum * scaleRadiusSum -
                        Math.Exp((Truncation.Radius - r) / Truncation.Scale) * (squaredScale + rScaleSum * rScaleSum));
            default:
                return 0;
        }
    }

    protected override double GetFullTruncationMass()
    {
        switch (Truncation.Type)
        {
            case TruncationType.None:
                return 0;
            case TruncationType.Sharp:
                return 0;
            case TruncationType.Linear:
                return Math.PI *
                    Truncation.Scale *
                    Truncation.StartDensity *
                    (Truncation.Scale * (Truncation.Scale + 4 * Truncation.Radius) + 6 * Truncation.Radius * Truncation.Radius) /
                    3;
            case TruncationType.Exponential:
                double scaleRadiusSum = Truncation.Scale + Truncation.Radius;
                return 4 * Math.PI * Truncation.StartDensity * Truncation.Scale * (Truncation.Scale * Truncation.Scale + scaleRadiusSum * scaleRadiusSum);
            default:
                return 0;
        }
    }

    public VolumeMassCalculator(Truncation truncation, Func<double, double> densityLaw, Integrate integrator) : base(truncation, densityLaw, integrator)
    {
    }
}
