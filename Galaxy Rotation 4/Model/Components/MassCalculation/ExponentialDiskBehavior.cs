﻿using System;

namespace Galaxy_Rotation_4.Model.Components.MassCalculation;

/// <summary>Содержит методы вычисления массы, общие для экспоненциальных дисков (толстого и тонкого).</summary>
public static class ExponentialDiskBehavior
{
    public static double CalcMainMass(double r, double centralDensity, double scale)
    {
        double scaledRadius = r / scale;

        return 2 * Math.PI * centralDensity * scale * scale * (1 - (scaledRadius + 1) * Math.Exp(-scaledRadius));
    }

    public static double GetNotTruncatedMass(double centralDensity, double scale) => 2 * Math.PI * centralDensity * scale * scale;
}
