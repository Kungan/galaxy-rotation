using System;

namespace Galaxy_Rotation_4.Model.Components.MassCalculation;

public sealed class SurfaceMassCalculator: MassCalculator
{
    /// <summary>Возвращает основную массу компонента в заданном радиусе.</summary>
    /// <param name="r">Радиус.</param>
    /// <returns>Масса.</returns>
    protected override double IntegrateMainMass(double r)
    {
        // ToDo: использовать рассчитанную плотность при необходимости
        double Func(double x) => 2 * Math.PI * x * DensityLaw(x);
        return Integrator(Func, 0, r);
    }

    protected override double GetTruncationMass(double r)
    {
        if (r <= Truncation.Radius)
        {
            return 0;
        }

        switch (Truncation.Type)
        {
            case TruncationType.None:
                return 0;
            case TruncationType.Sharp:
                return 0;
            case TruncationType.Linear:
                if (r >= Truncation.Radius + Truncation.Scale)
                    return GetFullTruncationMass();

                return Math.PI *
                    Truncation.StartDensity *
                    (r - Truncation.Radius) *
                    ((3 * Truncation.Scale + Truncation.Radius) * (r + Truncation.Radius) - 2 * r * r) /
                    (3 * Truncation.Scale);

            case TruncationType.Exponential:
                return 2 *
                    Math.PI *
                    Truncation.Scale *
                    Truncation.StartDensity *
                    (Truncation.Scale + Truncation.Radius - (Truncation.Scale + r) * Math.Exp((Truncation.Radius - r) / Truncation.Scale));
            default:
                return 0;
        }
    }

    protected override double GetFullTruncationMass()
    {
        return Truncation.Type switch
        {
            TruncationType.None => 0,
            TruncationType.Sharp => 0,
            TruncationType.Linear => Math.PI * Truncation.StartDensity * Truncation.Scale * (Truncation.Scale + 3 * Truncation.Radius) / 3,
            TruncationType.Exponential => 2 * Math.PI * Truncation.StartDensity * Truncation.Scale * (Truncation.Scale + Truncation.Radius),
            _ => 0,
        };
    }

    public SurfaceMassCalculator(Truncation truncation, Func<double, double> densityLaw, Integrate integrator) 
        : base(truncation, densityLaw, integrator)
    {
    }
}
