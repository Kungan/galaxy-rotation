using System;

namespace Galaxy_Rotation_4.Model.Components.MassCalculation;

public abstract class MassCalculator
{
    public delegate double Integrate(Func<double, double> function, double leftBound, double rightBound);

    protected MassCalculator(Truncation truncation, Func<double, double> densityLaw, Integrate integrator)
    {
        DensityLaw = densityLaw;
        Integrator = integrator;
        Truncation = truncation;
        GetMainMass = IntegrateMainMass;
    }

    /// <summary>Возвращает полную неусеченную массу.</summary>
    public Func<double> GetFullNotTruncatedMass { get; set; } = () => double.PositiveInfinity;

    public Func<double, double> GetMainMass { get; set; }

    protected Truncation Truncation { get; }

    protected Func<double, double> DensityLaw { get; }

    protected Integrate Integrator { get; }

    /// <summary>Возвращает основную массу компонента в заданном радиусе.</summary>
    protected abstract double IntegrateMainMass(double r);

    /// <summary>Возвращает полную остаточную массу компонента за пределами радиуса усечения.</summary>
    protected abstract double GetFullTruncationMass();

    /// <summary>
    ///     Возвращает остаточную массу компонента за пределами радиуса усечения. Должны использоваться аналитически посчитанные интегралы от функции
    ///     усечения.
    /// </summary>
    /// <param name="r">Радиус, в пределах которого нужно получить массу.</param>
    protected abstract double GetTruncationMass(double r);

    /// <summary>Возвращает массу компонента в заданном радиусе.</summary>
    public double GetMass(double r)
    {
        if (r <= 0)
            return 0;

        if (r < Truncation.Radius || Truncation.Type == TruncationType.None)
            return GetMainMass(r);

        // ToDo: оптимизировать: полную усеченную массу хранить в отдельном поле.
        return GetMainMass(Truncation.Radius) + GetTruncationMass(r);
    }

    /// <summary>Возвращает полную массу компонента.</summary>
    public double GetFullMass()
    {
        if (Truncation.Type == TruncationType.None)
            return GetFullNotTruncatedMass();

        return GetMainMass(Truncation.Radius) + GetFullTruncationMass();
    }
}
