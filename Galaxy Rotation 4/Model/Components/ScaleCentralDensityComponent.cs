﻿using System.Collections.Generic;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.Model.Components;

public abstract class ScaleCentralDensityComponent : ScalableComponent
{
    /// <summary>
    ///     Центральная плотность в солнечных массах на кубический парсек (в случае объемной плотности) или на квадратный парсек (для поверхностной
    ///     плотности).
    /// </summary>
    [Controlled]
    public double CentralDensity
    {
        get => CentralDensityPerKiloparsecs / DensityCoefficient;
        set
        {
            double newValue = value * DensityCoefficient;

            if (CentralDensityPerKiloparsecs != newValue)
            {
                CentralDensityPerKiloparsecs = newValue;
                OnMainParameterChanged();
            }
        }
    }

    /// <summary>Центральная плотность в массах Солнца на кубический / квадратный КИЛОпарсек.</summary>
    protected double CentralDensityPerKiloparsecs { get; private set; }

    protected ScaleCentralDensityComponent(Dimension dimension, IEnumerable<TruncationType> nonAvailableTruncTypes = null)
        : base(dimension, nonAvailableTruncTypes)
    {
    }
}
