﻿using System;

namespace Galaxy_Rotation_4.Model.Components;

public class IntegrationException : Exception
{
    public IntegrationException()
    {
    }

    public IntegrationException(string message) : base(message)
    {
    }

    public IntegrationException(string message, Exception innerException) : base(message, innerException)
    {
    }
}
