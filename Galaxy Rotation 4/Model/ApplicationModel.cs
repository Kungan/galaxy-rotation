﻿using System;
using System.Collections.Generic;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Model.Interfaces;
using Galaxy_Rotation_4.Model.MassReportNS;
using Galaxy_Rotation_4.ViewModel.Serializing;

namespace Galaxy_Rotation_4.Model;

public class ApplicationModel : IObservable
{
    private string _name;

    public ApplicationModel(Galaxy galaxy, string name, Dictionary<Component, ComponentDescription> componentsToDescriptions)
    {
        Galaxy = galaxy;
        Name = name;
        ComponentsToDescriptions = componentsToDescriptions;
        Initialize();
    }

    public ApplicationModel()
    {
        Galaxy = new Galaxy();
        NonameModelCounter++;
        Name = $"Новая модель {NonameModelCounter}";
        Initialize();
    }

    private static int NonameModelCounter { get; set; }

    public string Name
    {
        get => _name;
        set
        {
            _name = value;
            OnNameChanged();
        }
    }

    public Dictionary<Component, ComponentDescription> ComponentsToDescriptions { get; }

    /// <summary>
    ///     Признак того, что модель хотя бы один раз сохраняли. При этом не обязательно сохранение должно соответствовать текущему состоянию
    ///     модели.
    /// </summary>
    public bool IsSaved { get; set; }

    public Galaxy Galaxy { get; }

    public MassReport MassReport { get; private set; }

    public Deviations DeviationsSpeed { get; private set; }

    public Deviations DeviationsVolumeDensity { get; private set; }

    public Deviations DeviationsSurfaceDensity { get; private set; }

    public Observation ObservationSpeed { get; } = new();

    public Observation ObservationVolumeDensity { get; } = new();

    public Observation ObservationSurfaceDensity { get; } = new();

    public event Action NameChanged;

    private void OnNameChanged() => NameChanged?.Invoke();

    private void Initialize()
    {
        DeviationsSpeed = new Deviations(Galaxy.SpeedCalculator.TotalSpeed,
            Galaxy.CalcSpeed,
            ObservationSpeed);

        DeviationsVolumeDensity = new Deviations(Galaxy.TotalVolumeDensity,
            r => Galaxy.CalcDensity(r, Dimension.Volume),
            ObservationVolumeDensity);

        DeviationsSurfaceDensity = new Deviations(Galaxy.TotalSurfaceDensity,
            r => Galaxy.CalcDensity(r, Dimension.Surface),
            ObservationSurfaceDensity);

        MassReport = new MassReport(Galaxy);
    }
}
