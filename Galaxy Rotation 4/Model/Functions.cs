﻿using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace Galaxy_Rotation_4.Model;

internal static class Functions
{
    //ToDo: Ускорить метод за счет использования предыдущих значений

    /// <summary>Интегрирование методом Симпсона</summary>
    /// <param name="f">Подынтегральная функция</param>
    /// <param name="leftBound">Левая граница</param>
    /// <param name="rightBound">Правая граница</param>
    /// <param name="step">Шаг</param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static double Integrate(Func<double, double> f, double leftBound, double rightBound, double step)
    {
        if (leftBound == rightBound)
        {
            return 0;
        }

        if ((leftBound > rightBound && step > 0) ||
            (leftBound < rightBound && step < 0) ||
            step == 0)
        {
            throw new ArgumentException("Неверные параметры интегрирования");
        }

        int stepsCount;

        if (Math.Abs(step) >= Math.Abs(rightBound - leftBound))
        {
            stepsCount = 1;
        }
        else
        {
            stepsCount = (int)((rightBound - leftBound) / step);
        }

        return IntegrateByStepsCount(f, leftBound, rightBound, stepsCount);
    }

    /// <summary>Интегрирование методом Симпсона</summary>
    /// <param name="f">Подынтегральная функция</param>
    /// <param name="leftBound">Левая граница</param>
    /// <param name="rightBound">Правая граница</param>
    /// <param name="stepsCount">Количество шагов</param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static double IntegrateByStepsCount(Func<double, double> f, double leftBound, double rightBound, int stepsCount)
    {
        if (stepsCount % 2 == 1)
        {
            //Если N нечетное, увеличиваем его
            stepsCount++;
        }

        double newStep = (rightBound - leftBound) / stepsCount;
        double x = leftBound;

        //ToDo: Удалить после тестирования.
        //double deleteAfterTest = f(leftBound);
        double total = f(leftBound);

        for (var i = 1; i < stepsCount; i++)
        {
            x += newStep;
            double increment = f(x) * (2 + i % 2 * 2);
            total += increment; //если i четное, умножаем на 2, если нечетное - на 4
        }

        total += f(rightBound);
        total *= newStep / 3;

        return total;
    }

    //Гиперболический косинус
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static double Ch(double x) => (Math.Exp(x) + Math.Exp(-x)) / 2;

    /// <summary>Делает первую букву строки прописной.</summary>
    /// <param name="s">Исходная строка.</param>
    /// <returns>Строка с прописной первой буквой.</returns>
    public static string CapitalizeFirstLetter(string s)
    {
        var str = new StringBuilder(s);
        str[0] = char.ToUpper(str[0]);

        return str.ToString();
    }

    /// <summary>Делает первую букву строки строчной.</summary>
    /// <param name="s">Исходная строка.</param>
    /// <returns>Строка со строчной первой буквой.</returns>
    public static string DeCapitalizeFirstLetter(string s)
    {
        var str = new StringBuilder(s);
        str[0] = char.ToLower(str[0]);

        return str.ToString();
    }
}
