using System;
using System.Collections.Generic;
using Galaxy_Rotation_4.Model.Components;

namespace Galaxy_Rotation_4.Model;

public sealed class GalaxyMond : Galaxy
{
    public static double ConvertToSquaredMondSpeed(double squaredNewtonSpeed, double radius)
    {
        const double parameter = 1.2;
        double alpha = 3086 * parameter * radius / squaredNewtonSpeed;
        return squaredNewtonSpeed * Math.Sqrt(0.5 * (1 + Math.Sqrt(1 + 4 * alpha * alpha)));
    }

    protected override double GetTotalSquaredSpeed(double r) => ConvertToSquaredMondSpeed(base.GetTotalSquaredSpeed(r), r);

    protected override double CalcSquaredTotalSpeed(IReadOnlyCollection<Component> components, double r) =>
        ConvertToSquaredMondSpeed(base.CalcSquaredTotalSpeed(components, r), r);
}
