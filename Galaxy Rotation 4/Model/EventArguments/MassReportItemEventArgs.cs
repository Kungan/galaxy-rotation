﻿using System;
using Galaxy_Rotation_4.Model.MassReportNS;

namespace Galaxy_Rotation_4.Model;

public class MassReportItemEventArgs : EventArgs
{
    public MassReportItemEventArgs(MassReportItem massReportItem)
    {
        MassReportItem = massReportItem;
    }

    public MassReportItem MassReportItem { get; }
}
