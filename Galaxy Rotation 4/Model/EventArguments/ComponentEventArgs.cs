﻿using System;
using Galaxy_Rotation_4.Model.Components;

namespace Galaxy_Rotation_4.Model;

public class ComponentEventArgs : EventArgs
{
    public ComponentEventArgs(Component component)
    {
        Component = component;
    }

    public Component Component { get; }
}
