﻿using System;

namespace Galaxy_Rotation_4.Model;

public class StandardDeviationEventArgs : EventArgs
{
    public StandardDeviationEventArgs(StandardDeviation standardDeviation)
    {
        StandardDeviation = standardDeviation;
    }

    public StandardDeviation StandardDeviation { get; }
}
