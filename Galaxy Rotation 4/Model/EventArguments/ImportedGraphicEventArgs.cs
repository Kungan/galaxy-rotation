﻿using System;
using Galaxy_Rotation_4.Model.Graphics;

namespace Galaxy_Rotation_4.Model;

public class ImportedGraphicEventArgs : EventArgs
{
    public ImportedGraphicEventArgs(ImportedCurve curve)
    {
        Curve = curve;
    }

    public ImportedCurve Curve { get; }
}
