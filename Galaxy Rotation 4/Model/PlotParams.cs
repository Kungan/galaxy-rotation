﻿using System;

namespace Galaxy_Rotation_4.Model;

/// <summary>
///     Класс, представляющий собой параметры построения графиков. Связывает три величины: шаг, количество точек и максимальный радиус. Также определяет,
///     какие графики нужно строить (скорость, плотность).
/// </summary>
public class PlotParams
{
    private const double MinimumStep = 0.01;
    private const int MinimumDotsCount = 2;

    private bool _volumeDensityEnabled;
    private int _dotsCount;
    private bool _isStepUsing;
    private double _radiusMax;
    private bool _speedEnabled;
    private double _step;
    private bool _surfaceDensityEnabled;

    public PlotParams(PlotParams original)
    {
        CopyParameters(original);
    }

    public PlotParams()
    {
        _radiusMax = 15;
        _dotsCount = 251;
        _step = 0.06;
        IsStepUsing = true;
    }

    public event Action Changed;
    public event Action IsStepUsingChanged;
    public event Action StepChanged;
    public event Action DotsCountChanged;
    public event Action RadiusMaxChanged;

    /// <summary>Флаг использования шага / количества точек.</summary>
    public bool IsStepUsing
    {
        get => _isStepUsing;
        set
        {
            _isStepUsing = value;
            OnIsStepUsingChanged();
        }
    }

    /// <summary>Шаг расчета в килопарсеках. ToDo: придумать более быстрый способ интегрирования для построения кривой.</summary>
    public double Step
    {
        get => _step;
        set
        {
            if (value < MinimumStep)
            {
                value = MinimumStep;
            }

            if (_step != value)
            {
                _step = Math.Round(value, 3);
                DotsCount = (int)(_radiusMax / _step) + 1;
                OnChanged();
                OnStepChanged();
            }
        }
    }

    /// <summary>Возвращает или устанавливает количество точек на графике.</summary>
    public int DotsCount
    {
        get => _dotsCount;
        set
        {
            if (value < MinimumDotsCount)
            {
                value = MinimumDotsCount;
            }

            if (_dotsCount != value)
            {
                _dotsCount = value;
                Step = Math.Round(_radiusMax / (value - 1), 3);
                OnChanged();
                OnDotsCountChanged();
            }
        }
    }

    /// <summary>Радиус построения графика в килопарсеках.</summary>
    public double RadiusMax
    {
        get => _radiusMax;
        set
        {
            _radiusMax = value;

            if (IsStepUsing)
                _dotsCount = (int)(_radiusMax / _step) + 1;
            else
                _step = Math.Round(_radiusMax / (_dotsCount - 1), 3);

            if (_step == 0)
                _step = 0.001;

            OnChanged();
            RadiusMaxChanged?.Invoke();
        }
    }

    /// <summary>Строить ли график скорости.</summary>
    public bool SpeedEnabled
    {
        get => _speedEnabled;
        set
        {
            if (_speedEnabled == value)
                return;

            _speedEnabled = value;
            OnChanged();
        }
    }

    /// <summary>Строить ли график плотности.</summary>
    public bool VolumeDensityEnabled
    {
        get => _volumeDensityEnabled;
        set
        {
            if (_volumeDensityEnabled == value)
                return;

            _volumeDensityEnabled = value;
            OnChanged();
        }
    }

    public bool SurfaceDensityEnabled
    {
        get => _surfaceDensityEnabled;
        set
        {
            if (_surfaceDensityEnabled == value)
                return;

            _surfaceDensityEnabled = value;
            OnChanged();
        }
    }

    public void Update(PlotParams original)
    {
        CopyParameters(original);
        OnChanged();
    }

    private void CopyParameters(PlotParams original)
    {
        if (_radiusMax != original._radiusMax)
        {
            _radiusMax = original._radiusMax;
        }

        if (_dotsCount != original._dotsCount)
        {
            _dotsCount = original._dotsCount;
            OnDotsCountChanged();
        }

        if (_step != original._step)
        {
            _step = original._step;
            OnStepChanged();
        }

        if (_isStepUsing != original._isStepUsing)
        {
            _isStepUsing = original.IsStepUsing;
            OnIsStepUsingChanged();
        }

        _volumeDensityEnabled = original.VolumeDensityEnabled;
        _surfaceDensityEnabled = original.SurfaceDensityEnabled;
        _speedEnabled = original.SpeedEnabled;

        OnChanged();
    }

    private void OnChanged() => Changed?.Invoke();

    private void OnDotsCountChanged() => DotsCountChanged?.Invoke();

    private void OnIsStepUsingChanged() => IsStepUsingChanged?.Invoke();

    private void OnStepChanged() => StepChanged?.Invoke();
}
