﻿namespace Galaxy_Rotation_4.Model.Interfaces;

public interface IObservable
{
    Observation ObservationSpeed { get; }
    Observation ObservationVolumeDensity { get; }
    Observation ObservationSurfaceDensity { get; }
}
