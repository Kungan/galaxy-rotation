﻿using System;

namespace Galaxy_Rotation_4.Model.Interfaces;

/// <summary>Представляет собой интерфейс объекта с изменяющимися параметрами.</summary>
public interface IParameterizable
{
    event Action ParametersChanged;
}
