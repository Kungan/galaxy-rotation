﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Model.SpeedCalculation;

namespace Galaxy_Rotation_4.Model;

public class Galaxy
{
    /// <summary>Модифицированная гравитационная постоянная для использования с другими единицами массы, расстояния и скорости
    /// (массы Солнца вместо килограмма, килопарсеки вместо метров и км/сек вместо м/с соответственно).</summary>
    public const double G = 6.67408E-11 * SolarMass / (Kiloparsec * Kilometre * Kilometre);
    private const double Kilometre = 1000;
    private const double Parsec = 3.0856776E+16;
    private const double Kiloparsec = 1000 * Parsec;
    private const double SolarMass = 1.98892E+30;

    private readonly List<Component> _components = new();

    //Используется для того чтобы не перестраивать суммарную кривую при изменении всех компонентов одновременно
    private bool _doNotRefreshTotalGraph;

    public Galaxy()
    {
        PlotParams.Changed += PlotAllComponents;
        SpeedCalculator = new GalaxySpeedCalculator(PlotParams);
    }

    public PlotParams PlotParams { get; } = new();
    
    public GalaxySpeedCalculator SpeedCalculator { get; }
    public Curve TotalVolumeDensity { get; } = new();
    public Curve TotalSurfaceDensity { get; } = new();

    public ReadOnlyCollection<Component> Components => _components.AsReadOnly();
    
    private IEnumerable<Component> EnabledComponents => _components.Where(c => c.IsEnabled);

    private IEnumerable<Component> EnabledVolumeComponents => _components.Where(c => c.IsEnabled && c.Dimension == Dimension.Volume);

    private IEnumerable<Component> EnabledSurfaceComponents => _components.Where(c => c.IsEnabled && c.Dimension == Dimension.Surface);

    public event EventHandler<ComponentEventArgs> ComponentAdded;
    public event EventHandler<ComponentEventArgs> ComponentRemoved;

    protected virtual double GetTotalSquaredSpeed(double r) => EnabledComponents.Sum(component => component.CalcSquaredSpeed(r));

    public double CalcSpeed(double r) => Math.Sqrt(GetTotalSquaredSpeed(r));

    public int GetComponentIndex(Component component) => _components.IndexOf(component) + 1;

    protected virtual double CalcSquaredTotalSpeed(IReadOnlyCollection<Component> components, double r) =>
        components.Sum(component => component.SquaredSpeedCurve.Dots[r]);

    public void AddComponent(Component component)
    {
        _components.Add(component);
        ComponentAdded?.Invoke(this, new ComponentEventArgs(component));
        SpeedCalculator.Add(component);
        Subscribe(component);
        UpdateAllTotalCurves();
    }

    public double CalcDensity(double r, Dimension dimension)
    {
        return EnabledComponents
            .Where(c => c.Dimension == dimension)
            .Sum(c => c.GetTruncatedDensity(r));
    }

    /// <summary>Перерисовывает суммарную кривую.</summary>
    private void PlotAllComponents()
    {
        // Т. к. меняются параметры построения всех компонентов, суммарную кривую можно строить
        // только после того, как будут перестроены графики всех компонентов.
        _doNotRefreshTotalGraph = true;
        _components.ForEach(component => component.Plot());
        _doNotRefreshTotalGraph = false;

        PlotTotalVolumeDensity();
        PlotTotalSurfaceDensity();
    }

    private void PlotTotalVolumeDensity()
    {
        if (!PlotParams.VolumeDensityEnabled || _doNotRefreshTotalGraph)
            return;

        List<Component> volumeComponents = EnabledVolumeComponents.ToList();

        var dots = new Dictionary<double, double>();

        for (double r = 0; r <= PlotParams.RadiusMax; r += PlotParams.Step)
            dots.Add(r, volumeComponents.Sum(component => component.GetTruncatedDensity(r)));

        TotalVolumeDensity.Dots = dots;
    }

    private Dictionary<double, double> CalculateTotalSpeedCurve(IReadOnlyCollection<Component> components)
    {
        // Возможно, для вновь созданных компонентов это может оказаться неправильным.
        // Тогда стоит перенести вычисление скорости в нулевой точке внутрь класса Component и позволить переопределять это поведение.
        var dots = new Dictionary<double, double> { { 0, 0 } };

        for (double r = PlotParams.Step; r <= PlotParams.RadiusMax; r += PlotParams.Step)
        {
            double squaredTotalSpeed = CalcSquaredTotalSpeed(components, r);
            dots.Add(r, Math.Sqrt(squaredTotalSpeed));
        }

        return dots;
    }

    private void PlotTotalSurfaceDensity()
    {
        if (!PlotParams.SurfaceDensityEnabled || _doNotRefreshTotalGraph)
            return;

        List<Component> surfaceComponents = EnabledSurfaceComponents.ToList();

        var dots = new Dictionary<double, double>();

        for (double r = 0; r <= PlotParams.RadiusMax; r += PlotParams.Step)
            dots.Add(r, surfaceComponents.Sum(c => c.GetTruncatedDensity(r)));

        TotalSurfaceDensity.Dots = dots;
    }

    public void RemoveComponent(Component component)
    {
        UnSubscribe(component);
        _components.Remove(component);
        SpeedCalculator.Remove(component);
        UpdateAllTotalCurves();
        ComponentRemoved?.Invoke(this, new ComponentEventArgs(component));
    }

    private void Subscribe(Component component)
    {
        Curve density = component.DensityCurve;

        switch (component.Dimension)
        {
            case Dimension.Surface:
                density.IsEnabledChanged += UpdateTotalSurfaceDensity;
                density.DotsChanged += PlotTotalSurfaceDensity;
                break;
            case Dimension.Volume:
                density.IsEnabledChanged += UpdateTotalVolumeDensity;
                density.DotsChanged += PlotTotalVolumeDensity;
                break;
            default:
                throw new NotSupportedException($"Dimensity type {component.Dimension} is not supported.");
        }
    }

    private void UnSubscribe(Component component)
    {
        Curve density = component.DensityCurve;

        switch (component.Dimension)
        {
            case Dimension.Surface:
                density.IsEnabledChanged -= UpdateTotalSurfaceDensity;
                density.DotsChanged -= PlotTotalSurfaceDensity;
                break;
            case Dimension.Volume:
                density.IsEnabledChanged -= UpdateTotalVolumeDensity;
                density.DotsChanged -= PlotTotalVolumeDensity;
                break;
            default:
                throw new NotSupportedException($"Dimensity type {component.Dimension} is not supported.");
        }
    }

    private void UpdateAllTotalCurves()
    {
        UpdateTotalVolumeDensity();
        UpdateTotalSurfaceDensity();
    }

    private void UpdateTotalVolumeDensity()
    {
        TotalVolumeDensity.IsEnabled = EnabledVolumeComponents.Count() > 1;
        PlotTotalVolumeDensity();
    }

    private void UpdateTotalSurfaceDensity()
    {
        TotalSurfaceDensity.IsEnabled = EnabledSurfaceComponents.Count() > 1;
        PlotTotalSurfaceDensity();
    }
}
