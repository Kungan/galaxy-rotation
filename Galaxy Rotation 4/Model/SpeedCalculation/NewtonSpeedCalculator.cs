using System;

namespace Galaxy_Rotation_4.Model.Components.SpeedCalculation;

// interface ISpeedCalculator
// {
//     NewtonSpeedCalculator.GetSquaredSpeed
// }

public class NewtonSpeedCalculator
{
    public GetSquaredSpeed SquaredSpeedGetter { get; } = DefaultGetSphereSymmetricalSquaredSpeed;

    /// <summary>
    /// Вычисляет квадрат скорости для сферически симметричных компоненов.
    /// </summary>
    /// <param name="r">Радиус (килопарсек)</param>
    /// <param name="mass">Масса внутри заданного радиуса.</param>
    private static double DefaultGetSphereSymmetricalSquaredSpeed(double r, double mass)
    {
        // ToDo: Костыль, возможно потребуется исправить.
        if (r == 0)
            return 0;

        return Galaxy.G * mass / r;
    }

    /// <summary>
    /// Вычисляет cкорость для сферически симметричных компоненов.
    /// </summary>
    /// <param name="r">Радиус (килопарсек)</param>
    /// <param name="mass">Масса внутри заданного радиуса.</param>
    /// <param name="squaredSpeed">Возвращается квадрат скорости</param>
    public double GetSphereSymmetricalSpeed(double r, double mass, out double squaredSpeed)
    {
        squaredSpeed = SquaredSpeedGetter(r, mass);
        return Math.Sqrt(squaredSpeed);
    }
    
    public delegate double GetSquaredSpeed(double r, double mass);
}
