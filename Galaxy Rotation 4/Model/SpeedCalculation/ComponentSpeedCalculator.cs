using System;
using System.Collections.Generic;
using Galaxy_Rotation_4.Model.Components;

namespace Galaxy_Rotation_4.Model.SpeedCalculation;

public class ComponentSpeedCalculator
{
    private readonly PlotParams _plotParams;

    public event Action<IntegrationException> IntegrationErrorOccured;

    public ComponentSpeedCalculator(Component component, PlotParams plotParams)
    {
        _plotParams = plotParams;
        Component = component;
        component.EnabledChanged += RequestCalculating;
        component.EnabledChanged += UpdateCurveActivity;
        component.ParametersChanged += RequestCalculating;
        UpdateCurveActivity();
    }

    private void UpdateCurveActivity()
    {
        SpeedCurve.IsEnabled = Component.IsEnabled;
    }

    public Component Component { get; }

    public Curve SpeedCurve { get; } = new();

    public Curve SquaredSpeedCurve { get; } = new();

    public void RequestCalculating()
    {
        if (!Component.IsEnabled)
            return;

        if (_plotParams.SpeedEnabled)
            RePlotSpeed();
    }

    /// <summary>Строит график распределения квадратов скоростей если компонент активен.</summary>
    private void RePlotSpeed()
    {
        // ToDo: исправить в случае, если появятся компоненты с ненулевой скоростью в 0.
        var dotsSquared = new Dictionary<double, double> { { 0, 0 } };
        var dots = new Dictionary<double, double> { { 0, 0 } };

        for (double r = _plotParams.Step; r <= _plotParams.RadiusMax; r += _plotParams.Step)
        {
            try
            {
                double squaredSpeed = Component.CalcSquaredSpeed(r);
                dotsSquared.Add(r, squaredSpeed);
                dots.Add(r, Math.Sqrt(squaredSpeed));
            }
            catch (IntegrationException e)
            {
                IntegrationErrorOccured?.Invoke(e);
            }
        }

        SquaredSpeedCurve.Dots = dotsSquared;
        SpeedCurve.Dots = dots;
    }
}