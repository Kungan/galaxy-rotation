using System;
using System.Collections.Generic;
using System.Linq;
using Galaxy_Rotation_4.Model.Components;

namespace Galaxy_Rotation_4.Model.SpeedCalculation;

public class GalaxySpeedCalculator
{
    private readonly List<ComponentSpeedCalculator> _calculators = new();
    private readonly PlotParams _plotParams;

    public GalaxySpeedCalculator(PlotParams plotParams)
    {
        _plotParams = plotParams;
        _plotParams.Changed += RecalculateAll;
    }

    public Curve TotalSpeed { get; } = new();

    private IEnumerable<Curve> EnabledCurves => _calculators.Select(calculator => calculator.SquaredSpeedCurve).Where(curve => curve.IsEnabled);

    private bool IsEnabled
    {
        get => TotalSpeed.IsEnabled;
        set => TotalSpeed.IsEnabled = value;
    }

    public event Action<ComponentSpeedCalculator> SpeedCalculatorAdded;

    public event Action<ComponentSpeedCalculator[]> SpeedCalculatorRemoved;

    public void Add(Component component)
    {
        component.SquaredSpeedCurve.DotsChanged += RequestCalculating;
        component.SquaredSpeedCurve.IsEnabledChanged += UpdateTotalSpeed;
        ComponentSpeedCalculator calculator = CreateSpeedCalculator(component);
        _calculators.Add(calculator);
        // ToDo: Порядок важен, пофиксить это
        SpeedCalculatorAdded?.Invoke(calculator);
        UpdateTotalSpeed();
    }

    public void Remove(Component component)
    {
        component.SquaredSpeedCurve.DotsChanged -= RequestCalculating;
        component.SquaredSpeedCurve.IsEnabledChanged -= UpdateTotalSpeed;
        ComponentSpeedCalculator[] calculatorsToRemove = _calculators.Where(c => c.Component == component).ToArray();
        _calculators.RemoveAll(calculator => calculatorsToRemove.Contains(calculator));
        SpeedCalculatorRemoved?.Invoke(calculatorsToRemove);
        UpdateTotalSpeed();
    }

    private void UpdateTotalSpeed()
    {
        IsEnabled = EnabledCurves.Count() > 1;
        RequestCalculating();
    }

    protected virtual ComponentSpeedCalculator CreateSpeedCalculator(Component component) => new(component, _plotParams);

    protected virtual double CalculateSquaredTotalSpeed(IEnumerable<Curve> speedCurves, double r) => speedCurves.Sum(curve => curve.Dots[r]);

    /// <summary>Перерисовывает суммарную кривую.</summary>
    private void RecalculateAll()
    {
        // Т. к. меняются параметры построения всех компонентов, суммарную кривую можно строить
        // только после того, как будут перестроены графики всех компонентов.
        IsEnabled = false;
        _calculators.ForEach(calculator => calculator.RequestCalculating());
        IsEnabled = true;

        RequestCalculating();
    }

    private void RequestCalculating()
    {
        if (!_plotParams.SpeedEnabled || !IsEnabled)
            return;

        TotalSpeed.Dots = CalculateTotalSpeedCurve(EnabledCurves.ToArray());
    }

    private Dictionary<double, double> CalculateTotalSpeedCurve(IReadOnlyCollection<Curve> curves)
    {
        // Возможно, для вновь созданных компонентов это может оказаться неправильным.
        // Тогда стоит перенести вычисление скорости в нулевой точке внутрь класса Component и позволить переопределять это поведение.
        var dots = new Dictionary<double, double> { { 0, 0 } };

        for (double r = _plotParams.Step; r <= _plotParams.RadiusMax; r += _plotParams.Step)
        {
            dots.Add(r, Math.Sqrt(CalculateSquaredTotalSpeed(curves, r)));
        }

        return dots;
    }
}
