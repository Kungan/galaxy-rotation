﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Galaxy_Rotation_4.Model;

public class Deviations
{
    private readonly Func<double, double> _alternativeMethod;
    private readonly Curve _standard;
    private readonly List<StandardDeviation> _standardDeviations = new();

    public Deviations(Curve standard, Func<double, double> alternativeMethod, Observation observation)
    {
        _standard = standard;
        _alternativeMethod = alternativeMethod;
        observation.GraphicImported += Observation_GraphicImported;
        observation.GraphicRemoved += Observation_GraphicRemoved;
    }

    public ReadOnlyCollection<StandardDeviation> StandardDeviations => _standardDeviations.AsReadOnly();

    public event EventHandler<StandardDeviationEventArgs> StandardDeviationAdded;
    public event EventHandler<StandardDeviationEventArgs> StandardDeviationRemoved;

    private void OnStandardDeviationAdded(StandardDeviation standardDeviation)
    {
        StandardDeviationAdded?.Invoke(this, new StandardDeviationEventArgs(standardDeviation));
    }

    private void OnStandardDeviationRemoved(StandardDeviation standardDeviation)
    {
        StandardDeviationRemoved?.Invoke(this, new StandardDeviationEventArgs(standardDeviation));
    }

    private void Observation_GraphicImported(object sender, ImportedGraphicEventArgs e)
    {
        var std = new StandardDeviation(e.Curve, _standard, _alternativeMethod);
        _standardDeviations.Add(std);
        OnStandardDeviationAdded(std);
    }

    private void Observation_GraphicRemoved(object sender, ImportedGraphicEventArgs e)
    {
        StandardDeviation standardDeviation = _standardDeviations.First(std => std.Curve == e.Curve);
        _standardDeviations.Remove(standardDeviation);
        OnStandardDeviationRemoved(standardDeviation);
    }
}
