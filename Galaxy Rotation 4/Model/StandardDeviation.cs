﻿using System;
using System.Collections.Generic;
using Galaxy_Rotation_4.Model.Graphics;

namespace Galaxy_Rotation_4.Model;

public class StandardDeviation
{
    private readonly Func<double, double> _alternativeMethod;
    private readonly Curve _standard;

    public StandardDeviation(ImportedCurve curve, Curve standard, Func<double, double> alternativeMethod)
    {
        Curve = curve;
        _standard = standard;
        _standard.DotsChanged += UpdateStandardDeviation;
        _alternativeMethod = alternativeMethod;
        UpdateStandardDeviation();
    }

    public ImportedCurve Curve { get; }

    public double AbsoluteStd { get; private set; }
    public double RelativeStd { get; private set; }

    public event Action StdChanged;

    //ToDo: Выяснить, как считается отклонение в процентах
    //        Почему появляется отклонение от 100% при сравнение с нулем.

    /// <summary>Вычисляет среднеквадратичное отклонение.</summary>
    /// <param name="f">Входная функция.</param>
    /// <returns>Среднеквадратичное отклонение (std - относительное, relativeStd - абсолютное значение.</returns>
    private void UpdateStandardDeviation()
    {
        double deviationsSum = 0;
        double squaredSum = 0;

        foreach ((double key, double value) in Curve.Dots)
        {
            squaredSum += value * value;
            double t = GetStandardValue(key) - value;
            deviationsSum += t * t;
        }

        int n = Curve.Dots.Count;
        //Наверное, отклонение от 100% из-за того, что summ делится на n, а averageSpeed на n - 1
        AbsoluteStd = Math.Sqrt(deviationsSum / (n - 1));
        double average = Math.Sqrt(squaredSum / (n - 1));
        RelativeStd = AbsoluteStd / average;
        OnSTDChanged();
    }

    private void OnSTDChanged()
    {
        StdChanged?.Invoke();
    }

    /// <summary>Возвращает скорость с учетом всех активных компонентов.</summary>
    /// <param name="r">Радиус</param>
    /// <returns>Скорость</returns>
    private double GetStandardValue(double r)
    {
        IReadOnlyDictionary<double, double> dots = _standard.Dots;
        double left = 0;
        double right = 0;
        var hasLeftNeighbor = false;
        var hasRightNeighbor = false;

        if (dots.TryGetValue(r, out double value))
        {
            return value;
        }

        foreach (double key in dots.Keys)
        {
            // Находим "соседей", ближайшие точки к координате r слева и справа
            if (key < r)
            {
                left = key;
                hasLeftNeighbor = true;
            }
            else
            {
                right = key;
                hasRightNeighbor = true;

                break;
            }
        }

        // Если есть соседние точки, то вычисляем требуемое значение, используя линейную аппроксимацию.
        if (hasLeftNeighbor && hasRightNeighbor)
        {
            return dots[left] + (dots[right] - dots[left]) * (r - left) / (right - left);
        }

        // Если соседей нет, то вычисляем альтернативным методом 
        return _alternativeMethod(r);
    }
}
