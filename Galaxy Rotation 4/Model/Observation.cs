﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Galaxy_Rotation_4.Model.Graphics;

namespace Galaxy_Rotation_4.Model;

public class Observation
{
    private readonly List<ImportedCurve> _graphicsList = new();

    public ReadOnlyCollection<ImportedCurve> GraphicsList => _graphicsList.AsReadOnly();

    public event EventHandler<ImportedGraphicEventArgs> GraphicImported;
    public event EventHandler<ImportedGraphicEventArgs> GraphicRemoved;

    public void AddImportedGraphic(Dictionary<double, double> dots, string name)
    {
        var graphic = new ImportedCurve(dots, name);
        _graphicsList.Add(graphic);
        OnGraphicImported(graphic);
    }

    public void RemoveImportedGraphic(ImportedCurve curve)
    {
        _graphicsList.Remove(curve);
        OnGraphicRemoved(curve);
    }

    private void OnGraphicImported(ImportedCurve curve)
    {
        GraphicImported?.Invoke(this, new ImportedGraphicEventArgs(curve));
    }

    private void OnGraphicRemoved(ImportedCurve curve)
    {
        GraphicRemoved?.Invoke(this, new ImportedGraphicEventArgs(curve));
    }
}
