﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Galaxy_Rotation_4.ViewModel.Serializing;

namespace Galaxy_Rotation_4.Properties;

[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class ComponentControlsSettings : SerializableDictionary<string, SettingsForControl>, ICloneable
{
    public object Clone()
    {
        var settings = new ComponentControlsSettings();

        foreach (KeyValuePair<string, SettingsForControl> item in this)
        {
            settings.Add((string)item.Key.Clone(), (SettingsForControl)item.Value.Clone());
        }

        return settings;
    }
}
