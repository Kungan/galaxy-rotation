﻿using System.Configuration;

namespace Galaxy_Rotation_4.Properties;

/// <summary>Настройки для NumericUpDown.</summary>
[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class SettingForNumericUpDown : SettingsForControl
{
    public SettingForNumericUpDown()
    {
    }

    public SettingForNumericUpDown(decimal defaultValue, int decimalPlaces, decimal maximum, decimal minimum, decimal increment)
    {
        DefaultValue = defaultValue;
        DecimalPlaces = decimalPlaces;
        Maximum = maximum;
        Minimum = minimum;
        Increment = increment;
    }

    public decimal DefaultValue { get; set; }
    public decimal Maximum { get; set; }
    public decimal Minimum { get; set; }
    public decimal Increment { get; set; }
    public int DecimalPlaces { get; set; }

    public override object Clone() => new SettingForNumericUpDown(DefaultValue, DecimalPlaces, Maximum, Minimum, Increment);

    public override object GetDefaultValue() => (double)DefaultValue;
}
