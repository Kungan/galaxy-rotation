﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Galaxy_Rotation_4.ViewModel.Serializing;

namespace Galaxy_Rotation_4.Properties;

[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class AllComponentsSettings : SerializableDictionary<string, ComponentControlsSettings>, ICloneable
{
    public ComponentControlsSettings ComponentControlsSettings
    {
        get => throw new NotImplementedException();

        set { }
    }

    public object Clone()
    {
        var settings = new AllComponentsSettings();

        foreach (KeyValuePair<string, ComponentControlsSettings> item in this)
        {
            settings.Add((string)item.Key.Clone(), (ComponentControlsSettings)item.Value.Clone());
        }

        return settings;
    }
}
