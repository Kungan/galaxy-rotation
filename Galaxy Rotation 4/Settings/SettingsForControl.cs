﻿using System;
using System.Configuration;
using System.Xml.Serialization;

namespace Galaxy_Rotation_4.Properties;

[SettingsSerializeAs(SettingsSerializeAs.Xml)]
[XmlInclude(typeof(SettingForComboBox))]
[XmlInclude(typeof(SettingForNumericUpDown))]
public abstract class SettingsForControl : ICloneable
{
    public abstract object Clone();
    public abstract object GetDefaultValue();
}
