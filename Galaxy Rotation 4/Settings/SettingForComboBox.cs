﻿using System.Configuration;

namespace Galaxy_Rotation_4.Properties;

[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class SettingForComboBox : SettingsForControl
{
    public SettingForComboBox()
    {
    }

    public SettingForComboBox(string defaultText)
    {
        DefaultText = defaultText;
    }

    public string DefaultText { get; set; }

    public override object Clone() => new SettingForComboBox(DefaultText);

    public override object GetDefaultValue() => DefaultText;
}
