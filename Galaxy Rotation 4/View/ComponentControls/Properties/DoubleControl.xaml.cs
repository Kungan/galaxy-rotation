﻿using System.Windows.Controls;
using System.Windows.Input;

namespace Galaxy_Rotation_4.View.ComponentControls.Properties;

/// <summary>Логика взаимодействия для DoubleControl.xaml</summary>
public partial class DoubleControl : UserControl
{
    public DoubleControl()
    {
        InitializeComponent();
    }

    private void StopEventPropagation(object sender, MouseButtonEventArgs e)
    {
        e.Handled = true;
    }
}
