﻿using System.Windows;

namespace Galaxy_Rotation_4;

/// <summary>Логика взаимодействия для MainWindow.xaml</summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        StandardDeviations.IsVisible = false;
        LineDesigner.IsVisible = false;
    }
}
