﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace Galaxy_Rotation_4.Services;

public abstract class AllTextFileService : AllFileService
{
    public AllTextFileService(Dictionary<string, string[]> extensions, bool addAllFilesFilter) : base(extensions, addAllFilesFilter)
    {
    }

    public AllTextFileService(Dictionary<string, string[]> extensions) : base(extensions)
    {
    }

    public string LastSavedFileFullName { get; private set; }

    public (string fileName, string content) Open()
    {
        if (!OpenFileDialog.ShowDialog().Value)
        {
            return (null, null);
        }

        string fileContent = null;

        try
        {
            Stream file = OpenFileDialog.OpenFile();

            if (file == null)
            {
                return (null, null);
            }

            using (file)
            {
                using var reader = new StreamReader(file);
                fileContent = reader.ReadToEnd();
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show($"Ошибка: Невозможно прочитать файл. Произошла следующая ошибка: {ex.Message}");
        }

        return (OpenFileDialog.SafeFileName, fileContent);
    }

    public string SaveAs(string content, string defaultFileName = "")
    {
        SaveFileDialog.FileName = defaultFileName;

        if (SaveFileDialog.ShowDialog().Value)
        {
            if (Save(content, SaveFileDialog.FileName))
            {
                return SaveFileDialog.SafeFileName;
            }
        }

        return null;
    }

    public bool Save(string content, string fileName)
    {
        try
        {
            File.WriteAllText(fileName, content);
            LastSavedFileFullName = fileName;

            return true;
        }
        catch (Exception e)
        {
            WindowsManager.Instanse.ShowMessage($"Ошибка при записи в файл: {e.Message}");

            return false;
        }
    }
}
