﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Galaxy_Rotation_4.View;
using Galaxy_Rotation_4.View.Windows;
using Galaxy_Rotation_4.ViewModel.Windows;
using Galaxy_Rotation_4.ViewModel.Windows.CurvesSaver;

namespace Galaxy_Rotation_4.Services;

public class WindowsManager
{
    private readonly Dictionary<Type, Type> _registredWindowTypes = new();
    private readonly List<WindowVm> _viewModels = new();
    private readonly List<Window> _windows = new();

    private WindowsManager()
    {
        Register<MainWindowVm, MainWindow>();
        Register<PlotAreaTuningVm, PlotAreaTuning>();
        Register<ThemeWindowVm, ThemeWindow>();
        Register<CurvesSaverVm, CurvesSaver>();
    }

    public static WindowsManager Instanse { get; } = new();

    public void Close(WindowVm vm)
    {
        List<Window> windowsToClose = _windows.Where(window => window.DataContext == vm).ToList();

        foreach (Window window in windowsToClose)
        {
            window.Closed -= Window_Closed;
            _windows.Remove(window);
            window.Close();
        }
    }

    public void ShowMessage(string message)
    {
        MessageBox.Show(message);
    }

    public void Open(WindowVm viewModel)
    {
        if (_registredWindowTypes.TryGetValue(viewModel.GetType(), out Type windowType))
        {
            viewModel.Closed += ViewModel_Closed;
            var window = Activator.CreateInstance(windowType) as Window;
            window.DataContext = viewModel;
            window.Closed += Window_Closed;
            window.Show();
            _windows.Add(window);
        }
        else
        {
            throw new ArgumentException($"{viewModel.GetType().Name} не зарегистрирован в качестве viewmodel для открытия окна.");
        }

        ;
    }

    private void Register<TViewModel, TWindow>()
        where TViewModel : WindowVm
        where TWindow : Window
    {
        _registredWindowTypes.Add(typeof(TViewModel), typeof(TWindow));
    }

    private void ViewModel_Closed(object sender, EventArgs e)
    {
        Close(sender as WindowVm);
    }

    private void Window_Closed(object sender, EventArgs e)
    {
        var window = sender as Window;
        window.Closed -= Window_Closed;
        _windows.Remove(window);
        _viewModels.Remove(window.DataContext as WindowVm);
    }
}
