﻿using System.IO;
using System.Reflection;
using System.Text;
using Microsoft.Win32;

namespace Galaxy_Rotation_4.Services;

public static class FileRegistrationHelper
{
    public static void SetFileAssociation(string extension, string progId)
    {
        // Create extension subkey
        SetValue(Registry.ClassesRoot, extension, progId);

        // Create progid subkey
        string assemblyFullPath = Assembly.GetExecutingAssembly().Location.Replace("/", @"\");
        var sbShellEntry = new StringBuilder();
        sbShellEntry.AppendFormat("\"{0}\" \"%1\"", assemblyFullPath);
        SetValue(Registry.ClassesRoot, progId + @"\shell\open\command", sbShellEntry.ToString());
        var sbDefaultIconEntry = new StringBuilder();
        sbDefaultIconEntry.AppendFormat("\"{0}\",0", assemblyFullPath);
        SetValue(Registry.ClassesRoot, progId + @"\DefaultIcon", sbDefaultIconEntry.ToString());

        // Create application subkey
        SetValue(Registry.ClassesRoot, @"Applications\" + Path.GetFileName(assemblyFullPath), "", "NoOpenWith");
    }

    private static void SetValue(RegistryKey root, string subKey, object keyValue)
    {
        SetValue(root, subKey, keyValue, null);
    }

    private static void SetValue(RegistryKey root, string subKey, object keyValue, string valueName)
    {
        bool hasSubKey = subKey != null && subKey.Length > 0;
        RegistryKey key = root;

        try
        {
            if (hasSubKey)
                key = root.CreateSubKey(subKey);

            key.SetValue(valueName, keyValue);
        }
        finally
        {
            if (hasSubKey && key != null)
                key.Close();
        }
    }
}
