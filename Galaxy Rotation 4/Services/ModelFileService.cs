﻿using System.Collections.Generic;

namespace Galaxy_Rotation_4.Services;

public class ModelFileService : AllTextFileService
{
    public ModelFileService() : base(new Dictionary<string, string[]> { { "Модель Galaxy Rotation", new[] { "grm" } } },
        false)
    {
    }
}
