using System;

namespace Galaxy_Rotation_4.Services;

public class CurveParsingException
    : Exception
{
    public CurveParsingException(string message, Exception e): base(message, e)
    {
    }
}
