﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;

namespace Galaxy_Rotation_4.Services;

public abstract class AllFileService
{
    protected AllFileService(Dictionary<string, string[]> extensions, bool addAllFilesFilter = true)
    {
        string filter = CreateFilterString(extensions, addAllFilesFilter);

        SaveFileDialog = new SaveFileDialog { Filter = filter };

        OpenFileDialog = new OpenFileDialog { Filter = filter };
    }

    protected OpenFileDialog OpenFileDialog { get; }
    protected SaveFileDialog SaveFileDialog { get; }

    /// <summary>Создает строку фильтра для открытия файлов.</summary>
    /// <param name="extensions">Словарь фильтров. Ключ - отображаемое название фильтра. Значение - массив соответствующих расширений.</param>
    /// <param name="addAllFilesFilter">Добавлять ли возможность отображения всех файлов.</param>
    /// <returns>Строка вида "Изображения (*.bmp;*.gif;*.jpg;*.jpeg;*.png;*.tiff)|*.bmp;*.gif;*.jpg;*.jpeg;*.png;*.tiff|Все файлы (*.*)|*.*"</returns>
    private static string CreateFilterString(Dictionary<string, string[]> extensions, bool addAllFilesFilter)
    {
        var modifiedExtentions = new Dictionary<string, string[]>(extensions);

        if (addAllFilesFilter)
        {
            // Добавляем возможность фильтрации по всем файлам
            modifiedExtentions.Add("Все файлы", new[] { "*" });
        }

        return string.Join("|",
            modifiedExtentions.Select(pair =>
            {
                string description = pair.Key;

                string extensionGroup = string.Join(";",
                    pair.Value.Select(extension => $"*.{extension}"));

                return $"{description}  | {extensionGroup}";
            }).ToArray());
    }
}
