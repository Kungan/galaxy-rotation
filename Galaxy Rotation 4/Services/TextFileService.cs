﻿using System.Collections.Generic;

namespace Galaxy_Rotation_4.Services;

public class TextFileService : AllTextFileService
{
    public TextFileService() : base(new Dictionary<string, string[]> { { "Текст", new[] { "txt" } } })
    {
    }
}
