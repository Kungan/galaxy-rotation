﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Galaxy_Rotation_4.Model;

namespace Galaxy_Rotation_4.Services;

public static class CurveParser
{
    /// <summary>Распознает график в виде точек по одной на строке (одна точка - две цифры).</summary>
    public static Dictionary<double, double> Parse(string fileContent)
    {
        string separator = CultureInfo.InvariantCulture.NumberFormat.CurrencyDecimalSeparator;

        switch (separator)
        {
            case ".":
                fileContent = fileContent.Replace(",", separator);
                separator = $@"\{separator}";
                break;
            case ",":
                fileContent = fileContent.Replace(".", separator);
                break;
        }

        //Поиск цифр (положительных/отрицательных, целых/дробных).
        var coordinatePattern = $@"-?\d+{separator}?\d*";
        string coordinatesPattern = coordinatePattern + @"[ \t]+" + coordinatePattern;

        MatchCollection matches = Regex.Matches(fileContent, coordinatesPattern);
        var dots = new Dictionary<double, double>();

        foreach (Match dot in matches)
        {
            MatchCollection coordinates = Regex.Matches(dot.Value, coordinatePattern);

            if (coordinates.Count != 2)
                continue;

            double abscissa = double.Parse(coordinates[0].Value, CultureInfo.InvariantCulture);
            double ordinate = double.Parse(coordinates[1].Value, CultureInfo.InvariantCulture);

            try
            {
                dots.Add(abscissa, ordinate);
            }
            catch (ArgumentException e)
            {
                throw new CurveParsingException($"Parsing error. Cannot add coordinates pair \"{abscissa}, {ordinate}\" to curve: \"{e.Message}\"", e);
            }
        }

        return dots;
    }

    /// <summary>Формирует текст со значениями всех графиков. Важно: все графики должны иметь одинаковые значения абсцисс!</summary>
    /// <param name="items">Коллекция пар имя => график. Тип List используется так как он гарантирует одинаковый порядок перебора.</param>
    /// <returns>Текст с таблицей значений.</returns>
    internal static string ConvertToStringAll(List<(string name, Curve graphic)> items)
    {
        var stringBuilder = new StringBuilder();
        
        // Добавляем заголовок для первого столбца с ключами.
        stringBuilder.Append("Радиус");

        foreach ((string name, _) in items)
            stringBuilder.Append($"\t{name}");

        stringBuilder.Append("\r\n");

        IEnumerable<double> keys = items.First().graphic.Dots.Keys;
        
        // Добавляем кривые.
        foreach (double key in keys)
        {
            stringBuilder.Append(key.ToString(CultureInfo.InvariantCulture));
            
            foreach ((_, Curve graphic) in items)
                stringBuilder.Append($"\t{graphic.Dots[key].ToString(CultureInfo.InvariantCulture)}");

            stringBuilder.Append(Environment.NewLine);
        }

        return stringBuilder.ToString();
    }

    public static string ConvertToString(Curve curve)
    {
        var stringBuilder = new StringBuilder();

        foreach ((double key, double value) in curve.Dots)
        {
            stringBuilder.Append(key.ToString(CultureInfo.InvariantCulture));
            stringBuilder.Append('\t');
            stringBuilder.Append(value.ToString(CultureInfo.InvariantCulture));
            stringBuilder.Append("\r\n");
        }

        return stringBuilder.ToString();
    }
}