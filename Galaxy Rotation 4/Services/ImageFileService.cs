﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Galaxy_Rotation_4.Services;

internal class ImageFileService : AllFileService
{
    public ImageFileService() : base(new Dictionary<string, string[]> { { "Изображения", new[] { "bmp", "gif", "jpg", "png", "tiff" } } })
    {
    }

    public (BitmapSource, string) Open()
    {
        if (OpenFileDialog.ShowDialog() == true)
        {
            var bitmap = new BitmapImage();

            try
            {
                Stream stream = OpenFileDialog.OpenFile();

                if (stream != null)
                {
                    using (stream)
                    {
                        bitmap.BeginInit();
                        bitmap.StreamSource = stream;
                        bitmap.CacheOption = BitmapCacheOption.OnLoad;
                        bitmap.EndInit();

                        return (bitmap, OpenFileDialog.SafeFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка. Не могу открыть файл. Текст ошибки: " + ex.Message);
            }
        }

        return (null, null);
    }
}
