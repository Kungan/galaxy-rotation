﻿using System;
using System.Collections.ObjectModel;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components.Attributes;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls;

public class ControlsPaneVm : DockWindowVm
{
    public ControlsPaneVm()
    {
    }

    public ControlsPaneVm(ApplicationModel appModel)
    {
        Array componentCategories = typeof(ComponentCategory).GetEnumValues();
        Sections = new ObservableCollection<ControlsSectionVm>();

        foreach (ComponentCategory category in componentCategories)
        {
            Sections.Add(new ControlsSectionVm(category, appModel));
        }
    }

    public ObservableCollection<ControlsSectionVm> Sections { get; }
}
