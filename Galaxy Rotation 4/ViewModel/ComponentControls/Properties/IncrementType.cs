﻿namespace Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

public enum IncrementType
{
    Absolute,
    RelativeOneTenthPercent,
    RelativeTenPercents,
    RelativeOnePercent,
    RelativeManual,
}
