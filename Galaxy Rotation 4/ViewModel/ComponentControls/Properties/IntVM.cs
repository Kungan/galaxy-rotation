﻿using System.Reflection;
using Galaxy_Rotation_4.Model.Interfaces;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

public class IntVm : ControlVm
{
    public IntVm()
    {
    }

    public IntVm(IParameterizable source, PropertyInfo propertyInfo) : base(source, propertyInfo)
    {
    }

    public int Maximum { get; } = int.MaxValue;
    public int Minimum { get; } = 0;
    public int Increment { get; } = 1;
}
