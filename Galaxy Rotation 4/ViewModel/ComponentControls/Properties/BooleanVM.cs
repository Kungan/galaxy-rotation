﻿using System.Reflection;
using Galaxy_Rotation_4.Model.Interfaces;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

internal class BooleanVm : ControlVm
{
    public BooleanVm()
    {
    }

    public BooleanVm(IParameterizable source, PropertyInfo propertyInfo) : base(source, propertyInfo)
    {
    }
}
