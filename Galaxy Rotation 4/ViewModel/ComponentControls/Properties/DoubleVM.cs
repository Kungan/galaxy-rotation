﻿using System;
using System.ComponentModel;
using System.Reflection;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.Model.Interfaces;
using Galaxy_Rotation_4.Properties;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

public class DoubleVm : ControlVm
{
    private double _absoluteIncrement = 0.1;
    private IncrementType _incrementType;
    private double _oldValue;
    private double _relativeIncrementPercents = 50;
    private RelayCommand _resetCommand;
    private RelayCommand _saveAsDefaultCommand;

    public DoubleVm()
    {
    }

    public DoubleVm(IParameterizable source, PropertyInfo property) : base(source, property)
    {
        PropertyChanged += DoubleVM_PropertyChanged;
        IsPositiveInfinityAllowed = property.IsDefined(typeof(CanBeInfinity));
    }

    public bool IsPositiveInfinityAllowed { get; }

    public bool IsPositiveInfinitySet
    {
        get => IsPositiveInfinity();
        set
        {
            if (value != IsPositiveInfinity())
            {
                if (value)
                {
                    Value = double.PositiveInfinity;
                }
                else
                {
                    Value = _oldValue;
                }

                OnPropertyChanged();
            }
        }
    }

    public double Minimum { get; set; }

    public double Maximum { get; } = double.PositiveInfinity;

    public double Increment
    {
        get
        {
            return _incrementType switch
            {
                IncrementType.Absolute => AbsoluteIncrement,
                IncrementType.RelativeOneTenthPercent => 0.001 * (double)Value,
                IncrementType.RelativeOnePercent => 0.01 * (double)Value,
                IncrementType.RelativeTenPercents => 0.1 * (double)Value,
                IncrementType.RelativeManual => _relativeIncrementPercents * 0.01 * (double)Value,
                _ => throw new Exception("Не предусмотренный тип инкремента"),
            };
        }
    }

    public IncrementType TypeOfIncrement
    {
        get => _incrementType;
        set
        {
            _incrementType = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(Increment));
        }
    }

    public double AbsoluteIncrement
    {
        get => _absoluteIncrement;
        set
        {
            _absoluteIncrement = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(Increment));
        }
    }

    public double RelativeIncrementPercents
    {
        get => _relativeIncrementPercents;
        set
        {
            _relativeIncrementPercents = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(Increment));
        }
    }

    public RelayCommand SaveAsDefaultCommand => _saveAsDefaultCommand ??= new RelayCommand(_ => SaveAsDefault());

    public RelayCommand ResetCommand => _resetCommand ??= new RelayCommand(_ => Reset());

    private bool IsPositiveInfinity() => double.IsPositiveInfinity((double)Value);

    private void SaveAsDefault()
    {
        Settings.Default.Save();
    }

    public void Reset()
    {
    }

    private void DoubleVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == nameof(Value))
        {
            if (!IsPositiveInfinity())
            {
                _oldValue = (double)Value;
                OnPropertyChanged(nameof(IsPositiveInfinitySet));
            }

            if (TypeOfIncrement != IncrementType.Absolute)
            {
                OnPropertyChanged(nameof(Increment));
            }
        }
    }
}
