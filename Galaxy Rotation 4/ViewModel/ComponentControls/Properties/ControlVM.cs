﻿using System;
using System.Reflection;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.Model.Interfaces;
using Galaxy_Rotation_4.ViewModel.Resources;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

public class ControlVm : BaseViewModel
{
    protected IParameterizable Source;

    public ControlVm()
    {
    }

    public ControlVm(IParameterizable source, PropertyInfo property)
    {
        Source = source;
        source.ParametersChanged += () => OnPropertyChanged(nameof(Value));
        PropertyInfo = property;
        Name = PropertyLocalizator.GetLocalizedPropertyName(property);
        IsDisplayableOnly = property.IsDefined(typeof(DisplayOnlyAttribute));
    }

    public string Name { get; }

    /// <summary>Признак того, что данное свойство не управляется пользователем, а только отображается ему.</summary>
    public bool IsDisplayableOnly { get; }

    public PropertyInfo PropertyInfo { get; }

    public object Value
    {
        get => PropertyInfo.GetValue(Source);

        set
        {
            if (value == null)
            {
                return;
            }

            PropertyInfo.SetValue(Source, Convert.ChangeType(value, PropertyInfo.PropertyType));
            OnPropertyChanged();
        }
    }
}
