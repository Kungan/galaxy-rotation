﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Model.Interfaces;
using Galaxy_Rotation_4.ViewModel.Resources;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

public class TruncationTypeVm : EnumVm
{
    public TruncationTypeVm()
    {
    }

    public TruncationTypeVm(IParameterizable source, PropertyInfo propertyInfo) : base(source, propertyInfo)
    {
    }

    protected override void AddAvailableValues()
    {
        var component = Source as Component;
        AvailableValues = new Dictionary<string, Enum>();

        foreach (TruncationType trunc in component.Truncation.AvailableTypes)
        {
            AvailableValues.Add(trunc.GetLocalizedName(), trunc);
        }
    }
}
