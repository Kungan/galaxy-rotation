﻿using System.Collections.Generic;
using System.Reflection;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Interfaces;
using Galaxy_Rotation_4.Services;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

public class GraphicVm : ControlVm
{
    private readonly TextFileService _fileService = new();
    private string _fileName;
    private RelayCommand _openFileCommand;

    public GraphicVm()
    {
    }

    public GraphicVm(IParameterizable source, PropertyInfo propertyInfo) : base(source, propertyInfo)
    {
    }

    public string FileName
    {
        get => _fileName;
        set
        {
            _fileName = value;
            OnPropertyChanged();
        }
    }

    public RelayCommand OpenFileCommand => _openFileCommand ??= new RelayCommand(_ => OpenFile());

    private void OpenFile()
    {
        (string fileName, string content) = _fileService.Open();
        FileName = fileName;

        if (content == null)
            return;

        try
        {
            Dictionary<double, double> dots = CurveParser.Parse(content);
            Value = new Curve(dots);
        }
        catch (CurveParsingException e)
        {
            WindowsManager.Instanse.ShowMessage(e.Message);
        }
    }
}
