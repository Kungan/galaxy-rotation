﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Galaxy_Rotation_4.Model.Interfaces;
using Galaxy_Rotation_4.ViewModel.Resources;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

public class EnumVm : ControlVm
{
    public EnumVm()
    {
    }

    public EnumVm(IParameterizable source, PropertyInfo propertyInfo) : base(source, propertyInfo)
    {
        AddAvailableValues();
    }

    public Dictionary<string, Enum> AvailableValues { get; protected set; }

    protected virtual void AddAvailableValues()
    {
        AvailableValues = EnumLocalizator.GetLocalizedNames(PropertyInfo.PropertyType);
    }
}
