﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.Model.Interfaces;
using Galaxy_Rotation_4.Services;
using Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;
using Galaxy_Rotation_4.ViewModel.Serializing;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls;

public class ComponentControlVm : BaseViewModel
{
    private readonly ComponentDescription _description;
    private readonly TextFileService _fileService = new();

    private readonly TruncationType[] _truncTypesWithTruncRadius = { TruncationType.Sharp, TruncationType.Linear, TruncationType.Exponential };

    private readonly TruncationType[] _truncTypesWithTruncScale = { TruncationType.Linear, TruncationType.Exponential };

    private int _index;
    private string _name;
    private RelayCommand _removeCommand;
    private RelayCommand _saveDensityCommand;
    private RelayCommand _saveSpeedCommand;

    public ComponentControlVm(Component component, int index, ComponentDescription description)
    {
        _description = description;
        Component = component;
        Index = index;
        UpdateName();
        AddControlledProperties(component);
        AddDisplayableProperties(component);
        AddTruncProperties(component);
        component.IntegrationErrorOccured += Component_IntegrationErrorOccured;
    }

    public Component Component { get; }

    public bool Enabled
    {
        get => Component.IsEnabled;
        set => Component.IsEnabled = value;
    }

    public int Index
    {
        get => _index;
        set
        {
            _index = value;
            UpdateName();
        }
    }

    public string Name
    {
        get => _name;
        private set
        {
            _name = value;
            OnPropertyChanged();
        }
    }

    public ObservableCollection<ControlVm> Properties { get; } = new();

    public RelayCommand RemoveCommand => _removeCommand ??= new RelayCommand(_ => Remove());
    public RelayCommand SaveDensityCommand => _saveDensityCommand ??= new RelayCommand(_ => SaveDensity());
    public RelayCommand SaveSpeedCommand => _saveSpeedCommand ??= new RelayCommand(_ => SaveSpeed());

    public event Action<ComponentControlVm> Removed;

    private void Component_IntegrationErrorOccured(IntegrationException e)
    {
        WindowsManager.Instanse.ShowMessage($"Ошибка: {e.Message}");
    }

    private void Remove() => Removed?.Invoke(this);

    private void AddControlledProperties(Component component)
    {
        IEnumerable<PropertyInfo> controlledProperties = component
            .GetType()
            .GetProperties()
            .Where(property => property.IsDefined(typeof(ControlledAttribute)));

        foreach (PropertyInfo property in controlledProperties)
            AddProperty(component, property);
    }

    private void AddDisplayableProperties(Component component)
    {
        IEnumerable<PropertyInfo> displayableProperties = component
            .GetType()
            .GetProperties()
            .Where(property => property.IsDefined(typeof(DisplayOnlyAttribute)));

        foreach (PropertyInfo property in displayableProperties)
        {
            AddProperty(component, property);
        }
    }

    private void AddTruncProperties(Component component)
    {
        IEnumerable<PropertyInfo> truncProperties = component.GetType().GetProperties().Where(
            property => property.IsDefined(typeof(ControlledTruncationAttribute)));

        TruncationType[] availableTypes = component.Truncation.AvailableTypes.ToArray();
        bool isTrunkRadiusRequired = availableTypes.Intersect(_truncTypesWithTruncRadius).Any();
        bool isTrunkScaleRequired = availableTypes.Intersect(_truncTypesWithTruncScale).Any();

        foreach (PropertyInfo property in truncProperties)
        {
            switch (property.Name)
            {
                case nameof(Component.TruncationRadius) when !isTrunkRadiusRequired:
                case nameof(Component.TruncationScale) when !isTrunkScaleRequired:
                    continue;
                default:
                    AddProperty(component, property);
                    break;
            }
        }
    }

    private void AddProperty(IParameterizable component, PropertyInfo property)
    {
        Type propertyType = property.PropertyType;
        ControlVm propertyViewModel;

        if (propertyType.IsEnum)
        {
            if (property.PropertyType == typeof(TruncationType))
            {
                propertyViewModel = new TruncationTypeVm(component, property);
            }
            else
            {
                propertyViewModel = new EnumVm(component, property);
            }
        }
        else if (propertyType == typeof(double))
        {
            propertyViewModel = CreateDoubleVm(component, property);
        }
        else if (propertyType == typeof(int))
        {
            propertyViewModel = new IntVm(component, property);
        }
        else if (propertyType == typeof(Curve))
        {
            propertyViewModel = new GraphicVm(component, property);
        }
        else if (propertyType == typeof(bool))
        {
            propertyViewModel = new BooleanVm(component, property);
        }
        else
        {
            throw new NotImplementedException($"Неподдерживаемый тип данных: {propertyType.Name}");
        }

        Properties.Add(propertyViewModel);
    }

    private DoubleVm CreateDoubleVm(IParameterizable component, PropertyInfo property)
    {
        DoubleValueContainer parameters = _description?.Properties
            .GetPropertiesOfType<DoubleValueContainer>()
            .GetValueOrDefault(property.Name);

        var doubleVm = new DoubleVm(component, property)
        {
            AbsoluteIncrement = parameters?.AbsoluteIncrement ?? 0.1,
            Minimum = parameters?.Minimum ?? 0,
            RelativeIncrementPercents = parameters?.RelativeIncrementPercents ?? 50,
            TypeOfIncrement = parameters?.IncrementType ?? IncrementType.Absolute,
        };

        return doubleVm;
    }

    private void SaveDensity()
    {
        string content = CurveParser.ConvertToString(Component.DensityCurve);
        string fileName = Component.GetFullName() + " (плотность)";
        _fileService.SaveAs(content, fileName);
    }

    private void SaveSpeed()
    {
        string content = CurveParser.ConvertToString(Component.SpeedCurve);
        string fileName = Component.GetFullName() + " (скорость)";
        _fileService.SaveAs(content, fileName);
    }

    private void UpdateName()
    {
        string name = Component.GetShortName();
        var index = Index.ToString();
        Name = $"{name} ({index})";
    }
}
