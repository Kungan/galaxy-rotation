﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.ViewModel.Resources;
using Galaxy_Rotation_4.ViewModel.Serializing;

namespace Galaxy_Rotation_4.ViewModel.ComponentControls;

public class ControlsSectionVm : BaseViewModel
{
    private readonly ApplicationModel _appModel;
    private readonly Galaxy _galaxy;

    public ControlsSectionVm()
    {
    }

    public ControlsSectionVm(ComponentCategory componentCategory, ApplicationModel appModel) : this()
    {
        CreateComponentCommand = new RelayCommand(AddComponentCommand_Executed);
        ComponentCategory = componentCategory;
        Header = componentCategory.GetLocalizedName();
        AvailableComponentsToCreate = GetComponentNames(componentCategory);
        ComponentControls = new ObservableCollection<ComponentControlVm>();
        _galaxy = appModel.Galaxy;
        _appModel = appModel;
        _galaxy.ComponentAdded += Galaxy_ComponentAdded;
        AddComponents();
    }

    public string Header { get; }

    /// <summary>Команда создания компонента</summary>
    public RelayCommand CreateComponentCommand { get; }

    public ComponentCategory ComponentCategory { get; }
    public ObservableCollection<KeyValuePair<string, Type>> AvailableComponentsToCreate { get; }

    public ObservableCollection<ComponentControlVm> ComponentControls { get; }

    private void AddComponents()
    {
        foreach (Component component in _galaxy.Components)
        {
            AddComponent(component);
        }
    }

    private void Galaxy_ComponentAdded(object sender, ComponentEventArgs e)
    {
        AddComponent(e.Component);
    }

    private void AddComponent(Component component)
    {
        if (component.GetCategory() != ComponentCategory)
            return;

        int index = _galaxy.GetComponentIndex(component);
        var vm = new ComponentControlVm(component, index, GetDescription(component));
        ComponentControls.Add(vm);
        vm.Removed += ControlViewModel_Removed;
    }

    private ComponentDescription GetDescription(Component component) => _appModel.ComponentsToDescriptions?.GetValueOrDefault(component);

    private void ControlViewModel_Removed(ComponentControlVm vm)
    {
        ComponentControls.Remove(vm);
        _galaxy.RemoveComponent(vm.Component);
        UpdateIndexes();
    }

    private void UpdateIndexes()
    {
        foreach (ComponentControlVm vm in ComponentControls)
        {
            vm.Index = _galaxy.GetComponentIndex(vm.Component);
        }
    }

    private ObservableCollection<KeyValuePair<string, Type>> GetComponentNames(ComponentCategory componentCategory)
    {
        IEnumerable<Type> types = Assembly.GetExecutingAssembly().GetTypes().Where(type => type
            .GetCustomAttributes(typeof(ComponentCategoryAttribute), false)
            .Cast<ComponentCategoryAttribute>()
            .Any(attribute => attribute.Category == componentCategory));

        var result = new ObservableCollection<KeyValuePair<string, Type>>();

        foreach (Type type in types)
        {
            string name = ComponentLocalizator.GetLocalizedName(type);
            result.Add(new KeyValuePair<string, Type>(name, type));
        }

        return result;
    }

    private void AddComponentCommand_Executed(object parameter)
    {
        Component component = ComponentsFactory.Create(parameter as Type, _galaxy);
        component.IsEnabled = true;
        _galaxy.AddComponent(component);
    }
}
