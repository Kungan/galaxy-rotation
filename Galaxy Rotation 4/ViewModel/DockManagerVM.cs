﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.ViewModel.ComponentControls;
using Galaxy_Rotation_4.ViewModel.DeviationsNS;
using Galaxy_Rotation_4.ViewModel.Diagrams;
using Galaxy_Rotation_4.ViewModel.DigitizeNS;
using Galaxy_Rotation_4.ViewModel.ImportedGraphicsControls;
using Galaxy_Rotation_4.ViewModel.LineDesignerNS;
using Galaxy_Rotation_4.ViewModel.MassReportNS;
using Xceed.Wpf.AvalonDock.Themes;

namespace Galaxy_Rotation_4.ViewModel;

public class DockManagerVm : BaseViewModel, IThemeChangeable
{
    #region Поля

    private Theme _currentTheme;

    #endregion поля

    #region Конструкторы

    public DockManagerVm(ApplicationModel appModel, IEnumerable<DockWindowVm> dockWindowViewModels, DiagramsGroupVm diagramsGroupVm)
    {
        CurrentTheme = Themes.First().Item;
        AddDocuments(dockWindowViewModels);

        ControlsVm = new ControlsPaneVm(appModel);
        ImportedControlsPaneVm = new PaneVm(appModel);
        ImportedControlsPaneVm.DigitizeBegan += ImportedControlsPaneVM_DigitizeBegan;

        MassReportVm = new MassReportVm(appModel.MassReport) { IsClosed = true };

        StandardDeviationsGroupVm = new StandardDeviationsGroupVm(
            new[] { appModel.DeviationsSpeed, appModel.DeviationsVolumeDensity, appModel.DeviationsSurfaceDensity }) { IsClosed = true };

        LineDesignerVm = new LineDesignerVm(diagramsGroupVm) { IsClosed = true };
    }

    #endregion конструкторы

    #region Свойства

    public ControlsPaneVm ControlsVm { get; }

    public Theme CurrentTheme
    {
        get => _currentTheme;
        set
        {
            _currentTheme = value;
            OnPropertyChanged();
        }
    }

    public ObservableCollection<DockWindowVm> Documents { get; } = new();
    public PaneVm ImportedControlsPaneVm { get; }
    public LineDesignerVm LineDesignerVm { get; }
    public MassReportVm MassReportVm { get; }
    public StandardDeviationsGroupVm StandardDeviationsGroupVm { get; }

    public ObservableCollection<NamedItemVm<Theme>> Themes { get; } = new()
    {
        new NamedItemVm<Theme>("Упрощенная", new GenericTheme()),
        new NamedItemVm<Theme>("Метро", new MetroTheme()),
        new NamedItemVm<Theme>("Aero", new AeroTheme()),
    };

    #endregion свойства

    #region Методы

    private void AddDocument(DockWindowVm document)
    {
        document.PropertyChanged += DocumentViewModel_PropertyChanged;

        if (!document.IsClosed)
        {
            Documents.Add(document);
        }
    }

    private void AddDocuments(IEnumerable<DockWindowVm> dockWindowViewModels)
    {
        foreach (DockWindowVm document in dockWindowViewModels)
        {
            AddDocument(document);
        }
    }

    #endregion методы

    #region Обработчики

    private void DocumentViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        var document = sender as DockWindowVm;

        if (e.PropertyName == nameof(DockWindowVm.IsClosed))
        {
            if (!document.IsClosed)
            {
                Documents.Add(document);
            }
            else
            {
                Documents.Remove(document);
            }
        }
    }

    private void ImportedControlsPaneVM_DigitizeBegan(object sender, DigitizeEventArgs e)
    {
        DigitizeVm vm = e.DigitizeViewModel;
        AddDocument(vm);
        //vm.IsActive = true;
    }

    #endregion обработчики
}
