﻿using Galaxy_Rotation_4.Model;

namespace Galaxy_Rotation_4.ViewModel.Windows;

internal class PlotAreaTuningVm : WindowVm
{
    private readonly Galaxy _galaxy;
    private readonly PlotParams _plotParams;
    private RelayCommand _applyCommand;
    private RelayCommand _cancel;
    private RelayCommand _okCommand;

    public PlotAreaTuningVm()
    {
    }

    public PlotAreaTuningVm(Galaxy galaxy) : base(true)
    {
        _galaxy = galaxy;
        _plotParams = new PlotParams(galaxy.PlotParams);
        _plotParams.IsStepUsingChanged += PlotParams_IsStepUsingChanged;
        _plotParams.RadiusMaxChanged += PlotParams_RadiusMaxChanged;
        _plotParams.StepChanged += PlotParams_StepChanged;
        _plotParams.DotsCountChanged += PlotParams_DotsCountChanged;
    }

    public bool IsStepUsing
    {
        get => _plotParams.IsStepUsing;
        set => _plotParams.IsStepUsing = value;
    }

    public double Step
    {
        get => _plotParams.Step;
        set => _plotParams.Step = value;
    }

    /// <summary>Возвращает или устанавливает количество точек на графике.</summary>
    public int DotsCount
    {
        get => _plotParams.DotsCount;
        set => _plotParams.DotsCount = value;
    }

    /// <summary>Радиус построения графика в килопарсеках.</summary>
    public double RadiusMax
    {
        get => _plotParams.RadiusMax;
        set => _plotParams.RadiusMax = value;
    }

    public RelayCommand OkCommand => _okCommand ??= new RelayCommand(_ => OkCommand_Executed());
    public RelayCommand ApplyCommand => _applyCommand ??= new RelayCommand(_ => SaveParameters());
    public RelayCommand Cancel => _cancel ??= new RelayCommand(_ => OnClosing());

    private void SaveParameters() => _galaxy.PlotParams.Update(_plotParams);

    private void PlotParams_IsStepUsingChanged() => OnPropertyChanged(nameof(IsStepUsing));

    private void PlotParams_DotsCountChanged() => OnPropertyChanged(nameof(DotsCount));

    private void PlotParams_StepChanged() => OnPropertyChanged(nameof(Step));

    private void PlotParams_RadiusMaxChanged() => OnPropertyChanged(nameof(RadiusMax));

    private void OkCommand_Executed()
    {
        SaveParameters();
        OnClosing();
    }
}
