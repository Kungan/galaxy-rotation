﻿using Galaxy_Rotation_4.Services;

namespace Galaxy_Rotation_4.ViewModel.Windows;

internal class FileAssociatorWindowVm : BaseViewModel
{
    public void Associate()
    {
        const string extension = ".grm";
        const string title = "Galaxy Rotation 4";
        const string extensionDescription = "Galaxy Rotation 4 model";
        FileRegistrationHelper.SetFileAssociation(extension, title + "." + extensionDescription);
    }
}
