﻿using System;

namespace Galaxy_Rotation_4.ViewModel.Windows;

public class WindowVm : BaseViewModel
{
    public WindowVm(bool isModal = false)
    {
        IsModal = isModal;
    }

    public bool IsModal { get; }

    public event EventHandler Closed;

    protected void OnClosing()
    {
        Closed.Invoke(this, EventArgs.Empty);
    }
}
