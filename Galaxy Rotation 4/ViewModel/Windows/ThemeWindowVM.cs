﻿using System.Collections.ObjectModel;
using Xceed.Wpf.AvalonDock.Themes;

namespace Galaxy_Rotation_4.ViewModel.Windows;

internal class ThemeWindowVm : WindowVm
{
    private readonly IThemeChangeable _themeChangeable;

    private Theme _currentTheme;

    public ThemeWindowVm(IThemeChangeable themeChangeable)
    {
        _themeChangeable = themeChangeable;
        Themes = themeChangeable.Themes;
        CurrentTheme = themeChangeable.CurrentTheme;
    }

    public ObservableCollection<NamedItemVm<Theme>> Themes { get; }

    public Theme CurrentTheme
    {
        get => _currentTheme;
        set
        {
            _currentTheme = value;
            OnPropertyChanged();
        }
    }

    #region Команды

    private RelayCommand _ok;
    public RelayCommand Ok => _ok ??= new RelayCommand(_ => OKCommand_Executed());

    private void OKCommand_Executed()
    {
        SaveParameters();
        OnClosing();
    }

    private void SaveParameters()
    {
        _themeChangeable.CurrentTheme = CurrentTheme;
    }

    private RelayCommand _apply;

    public RelayCommand Apply => _apply ??= new RelayCommand(_ => ApplyCommand_Executed(),
        _ => CurrentTheme != _themeChangeable.CurrentTheme);

    private void ApplyCommand_Executed()
    {
        SaveParameters();
    }

    private RelayCommand _cancel;
    public RelayCommand Cancel => _cancel ??= new RelayCommand(_ => CancelCommand_Executed());

    private void CancelCommand_Executed()
    {
        OnClosing();
    }

    #endregion команды
}
