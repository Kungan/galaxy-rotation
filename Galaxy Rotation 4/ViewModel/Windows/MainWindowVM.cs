﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Services;
using Galaxy_Rotation_4.ViewModel.Diagrams;
using Galaxy_Rotation_4.ViewModel.Menu;
using Galaxy_Rotation_4.ViewModel.Serializing;
using Galaxy_Rotation_4.ViewModel.Windows.CurvesSaver;

namespace Galaxy_Rotation_4.ViewModel.Windows;

public class MainWindowVm : WindowVm
{
    private readonly ModelFileService _modelFileService = new();
    private readonly TextFileService _textFileService = new();
    private readonly WindowsManager _windowManager = WindowsManager.Instanse;
    private ApplicationModel _applicationModel;
    private DockManagerVm _dockManagerVm;
    private MenuVm _menuGraphicsVm;

    public MainWindowVm()
    {
    }

    public MainWindowVm(ApplicationModel appModel)
    {
        LoadModel(appModel);
        appModel.NameChanged += () => OnPropertyChanged(nameof(Title));
        QuitCommand = new RelayCommand(QuitCommand_Executed);
        PlotAreaTuningCommand = new RelayCommand(PlotAreaTuningCommand_Executed);
        SaveModelCommand = new RelayCommand(SaveModelCommand_Executed);
        SaveModelAsCommand = new RelayCommand(SaveModelAsCommand_Executed);
        ExportSpeedCurvesCommand = new RelayCommand(ExportSpeedCurvesCommand_Executed);
        ExportVolumeDensityProfilesCommand = new RelayCommand(ExportVolumeDensityProfilesCommand_Executed);
        ExportSurfaceDensityProfilesCommand = new RelayCommand(ExportSurfaceDensityProfilesCommand_Executed);
        LoadModelCommand = new RelayCommand(LoadModelCommand_Executed);
        ThemeCommand = new RelayCommand(ThemeCommand_Executed);
    }

    public string Title => $"{_applicationModel.Name} — {Assembly.GetExecutingAssembly().GetName().Name}";

    public DockManagerVm DockManagerVm
    {
        get => _dockManagerVm;
        private set
        {
            _dockManagerVm = value;
            OnPropertyChanged();
        }
    }

    /// <summary>Команда открытия модели</summary>
    public RelayCommand LoadModelCommand { get; }

    public MenuVm MenuGraphicsVm
    {
        get => _menuGraphicsVm;
        private set
        {
            _menuGraphicsVm = value;
            OnPropertyChanged();
        }
    }

    /// <summary>Команда открытия окна настроек</summary>
    public RelayCommand PlotAreaTuningCommand { get; }

    /// <summary>Команда выхода из приложения</summary>
    public RelayCommand QuitCommand { get; }

    /// <summary>Команда сохранения модели без диалогового окна (если модель уже была сохранена).</summary>
    public RelayCommand SaveModelCommand { get; }

    /// <summary>Команда сохранения модели под другим именем.</summary>
    public RelayCommand SaveModelAsCommand { get; }

    /// <summary>Команда экспорта кривых вращения.</summary>
    public RelayCommand ExportSpeedCurvesCommand { get; }

    /// <summary>Команда экспорта профилей объемной плотности.</summary>
    public RelayCommand ExportVolumeDensityProfilesCommand { get; set; }

    /// <summary>Команда экспорта профилей поверхностной плотности.</summary>
    public RelayCommand ExportSurfaceDensityProfilesCommand { get; set; }

    /// <summary>Команда открытия окна изменения темы</summary>
    public RelayCommand ThemeCommand { get; }

    private void LoadModel(ApplicationModel appModel)
    {
        _applicationModel = appModel;
        var diagramsVm = new DiagramsGroupVm(appModel);

        var documents = new List<DockWindowVm> { diagramsVm.SpeedDiagramVm, diagramsVm.VolumeDensityDiagramVm, diagramsVm.SurfaceDensityDiagramVm };

        DockManagerVm = new DockManagerVm(appModel, documents, diagramsVm);
        MenuGraphicsVm = new MenuVm(documents);
    }

    private void ExportSpeedCurvesCommand_Executed(object obj)
    {
        Galaxy galaxy = _applicationModel.Galaxy;

        IEnumerable<CurveItemVm> curves = galaxy.Components
            .Where(component => component.IsEnabled)
            .Select(component => new CurveItemVm(component.GetFullName(), component.SpeedCurve))
            .Append(new CurveItemVm("Суммарная кривая вращения", galaxy.SpeedCalculator.TotalSpeed));

        var saverVm = new CurvesSaverVm(curves, "Экспорт кривых вращения", "Кривые вращения");
        WindowsManager.Instanse.Open(saverVm);
    }

    private void ExportVolumeDensityProfilesCommand_Executed(object obj)
    {
        Galaxy galaxy = _applicationModel.Galaxy;

        IEnumerable<CurveItemVm> curves = galaxy.Components
            .Where(component => component.IsEnabled && component.Dimension == Dimension.Volume)
            .Select(component => new CurveItemVm(component.GetFullName(), component.DensityCurve))
            .Append(new CurveItemVm("Суммарный профиль плотности", galaxy.TotalVolumeDensity));

        var saverVm = new CurvesSaverVm(curves, "Экспорт профилей объемной плотности", "Профили объемной плотности");
        WindowsManager.Instanse.Open(saverVm);
    }

    private void ExportSurfaceDensityProfilesCommand_Executed(object obj)
    {
        Galaxy galaxy = _applicationModel.Galaxy;

        IEnumerable<CurveItemVm> curves = galaxy.Components
            .Where(component => component.IsEnabled && component.Dimension == Dimension.Surface)
            .Select(component => new CurveItemVm(component.GetFullName(), component.DensityCurve))
            .Append(new CurveItemVm("Суммарный профиль плотности", galaxy.TotalSurfaceDensity));

        var saverVm = new CurvesSaverVm(curves, "Экспорт профилей поверхностной плотности", "Профили поверхностной плотности");
        WindowsManager.Instanse.Open(saverVm);
    }

    private void LoadModelCommand_Executed(object o)
    {
        (string name, string serializedModel) = _modelFileService.Open();

        if (serializedModel == null)
        {
            return;
        }

        try
        {
            ApplicationModel appModel = ApplicationModelManager.Open(serializedModel, name);
            var windowVm = new MainWindowVm(appModel);
            _windowManager.Open(windowVm);
        }
        catch (ComponentCreationException e)
        {
            _windowManager.ShowMessage("Неверный формат файла: " + e.Message);
        }
        catch (SerializerException e)
        {
            _windowManager.ShowMessage("Неверный формат файла: " + e.Message);
        }
    }

    private void PlotAreaTuningCommand_Executed(object o)
    {
        var vm = new PlotAreaTuningVm(_applicationModel.Galaxy);
        _windowManager.Open(vm);
    }

    private void QuitCommand_Executed(object o)
    {
        Application.Current.Shutdown();
    }

    private void SaveModelCommand_Executed(object o)
    {
        if (!_applicationModel.IsSaved)
        {
            SaveModelAs();

            return;
        }

        SaveSilently();
    }

    private void SaveModelAsCommand_Executed(object o)
    {
        SaveModelAs();
    }

    private void SaveSilently()
    {
        string serializedModel = GetSerializedModel();

        if (_modelFileService.Save(serializedModel, _modelFileService.LastSavedFileFullName))
            _applicationModel.IsSaved = true;
    }

    private void SaveModelAs()
    {
        string serializedModel = GetSerializedModel();
        string filename = _modelFileService.SaveAs(serializedModel, _applicationModel.Name);

        if (filename != null)
        {
            _applicationModel.Name = filename;
            _applicationModel.IsSaved = true;
        }
    }

    private string GetSerializedModel()
    {
        var description = new GalaxyDescription(_applicationModel.Galaxy, DockManagerVm.ControlsVm);

        return Serializer.Serialize(description);
    }

    private void ThemeCommand_Executed(object o)
    {
        var vm = new ThemeWindowVm(DockManagerVm);
        _windowManager.Open(vm);
    }
}
