﻿using Galaxy_Rotation_4.Model;

namespace Galaxy_Rotation_4.ViewModel.Windows.CurvesSaver;

public class CurveItemVm : BaseViewModel
{
    public CurveItemVm(string name, Curve curve)
    {
        Name = name;
        Curve = curve;
    }

    public string Name { get; }
    public Curve Curve { get; }
    public bool IsSelected { get; set; } = true;
}
