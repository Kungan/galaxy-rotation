﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Galaxy_Rotation_4.Services;

namespace Galaxy_Rotation_4.ViewModel.Windows.CurvesSaver;

public class CurvesSaverVm : WindowVm
{
    private readonly string _defaultFileName;
    private readonly TextFileService _textFileService = new();

    public CurvesSaverVm()
    {
    }

    public CurvesSaverVm(IEnumerable<CurveItemVm> curves, string title, string defaultFileName)
    {
        SaveCommand = new RelayCommand(SaveCommand_Executed);
        Title = title;
        _defaultFileName = defaultFileName;
        Curves = new ObservableCollection<CurveItemVm>(curves);
    }

    public string Title { get; }

    public bool DoSaveToOneFile { get; set; } = true;

    public RelayCommand SaveCommand { get; set; }

    public ObservableCollection<CurveItemVm> Curves { get; }

    private void SaveCommand_Executed(object obj)
    {
        IEnumerable<CurveItemVm> items = Curves.Where(item => item.IsSelected);

        if (DoSaveToOneFile)
        {
            string content = CurveParser.ConvertToStringAll(items.Select(item => (item.Name, Graphic: item.Curve)).ToList());
            _textFileService.SaveAs(content, _defaultFileName);
        }
        else
        {
            foreach (CurveItemVm item in items)
            {
                string content = CurveParser.ConvertToString(item.Curve);
                _textFileService.SaveAs(content, item.Name);
            }
        }
    }
}
