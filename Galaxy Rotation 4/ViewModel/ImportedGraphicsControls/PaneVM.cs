﻿using System;
using System.Collections.Generic;
using Galaxy_Rotation_4.Model.Interfaces;
using Galaxy_Rotation_4.ViewModel.DigitizeNS;

namespace Galaxy_Rotation_4.ViewModel.ImportedGraphicsControls;

public class PaneVm : DockWindowVm
{
    public PaneVm(IObservable observables)
    {
        var speedGraphics = new SectionVm("Скорость", observables.ObservationSpeed);
        speedGraphics.DigitizeBegan += SpeedGraphics_DigitizeBegan;

        var volumeDensityGraphics = new SectionVm("Объемная плотность", observables.ObservationVolumeDensity);
        volumeDensityGraphics.DigitizeBegan += SpeedGraphics_DigitizeBegan;

        var surfaceDensityGraphics = new SectionVm("Поверхностная плотность", observables.ObservationSurfaceDensity);
        surfaceDensityGraphics.DigitizeBegan += SpeedGraphics_DigitizeBegan;

        Sections = new List<SectionVm> { speedGraphics, volumeDensityGraphics, surfaceDensityGraphics };
    }

    public List<SectionVm> Sections { get; }

    public event EventHandler<DigitizeEventArgs> DigitizeBegan;

    private void SpeedGraphics_DigitizeBegan(object sender, DigitizeEventArgs e)
    {
        DigitizeBegan?.Invoke(this, e);
    }
}
