﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media.Imaging;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Graphics;
using Galaxy_Rotation_4.Services;
using Galaxy_Rotation_4.ViewModel.DigitizeNS;

namespace Galaxy_Rotation_4.ViewModel.ImportedGraphicsControls;

public class SectionVm : BaseViewModel
{
    public event EventHandler<DigitizeEventArgs> DigitizeBegan;

    private readonly ImageFileService _imageFileService = new();
    private readonly TextFileService _textFileService = new();
    private readonly Observation _observation;

    public SectionVm()
    {
        ImportCommand = new RelayCommand(_ => Import());
        DigitizeCommand = new RelayCommand(_ => Digitize());
        Title = "Название раздела с графиками";
    }

    public SectionVm(string title, Observation observation) : this()
    {
        Title = title;
        _observation = observation;
        AddGraphics(observation);
        observation.GraphicImported += OnGraphicImported;
        observation.GraphicRemoved += OnGraphicRemoved;
    }

    public ObservableCollection<ControlVm> Controls { get; } = new();
    public RelayCommand DigitizeCommand { get; }
    public RelayCommand ImportCommand { get; }
    public string Title { get; }

    private void Digitize()
    {
        (BitmapSource bitmap, string name) = _imageFileService.Open();
        var digitizeVm = new DigitizeVm(bitmap, name);
        DigitizeBegan?.Invoke(this, new DigitizeEventArgs(digitizeVm));
    }

    private void Import()
    {
        (string name, string content) = _textFileService.Open();

        if (content == null)
            return;

        try
        {
            Dictionary<double, double> dots = CurveParser.Parse(content);
            _observation.AddImportedGraphic(dots, name);
        }
        catch (CurveParsingException e)
        {
            WindowsManager.Instanse.ShowMessage(e.Message);
        }
    }

    private void AddGraphic(ImportedCurve curve)
    {
        var vm = new ControlVm(curve);
        Controls.Add(vm);
        vm.Removed += OnImportedGraphicVmRemoved;
    }

    private void AddGraphics(Observation observation)
    {
        foreach (ImportedCurve graphic in observation.GraphicsList)
        {
            AddGraphic(graphic);
        }
    }

    private void RemoveGraphic(ImportedCurve curve)
    {
        ControlVm vm = Controls.First(series => series.Curve == curve);
        Controls.Remove(vm);
    }

    private void OnImportedGraphicVmRemoved(object sender, ImportedGraphicEventArgs e)
    {
        _observation.RemoveImportedGraphic(e.Curve);
    }

    private void OnGraphicImported(object sender, ImportedGraphicEventArgs e)
    {
        AddGraphic(e.Curve);
    }

    private void OnGraphicRemoved(object sender, ImportedGraphicEventArgs e)
    {
        RemoveGraphic(e.Curve);
    }
}
