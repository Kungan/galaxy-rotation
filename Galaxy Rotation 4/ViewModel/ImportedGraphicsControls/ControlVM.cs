﻿using System;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Graphics;

namespace Galaxy_Rotation_4.ViewModel.ImportedGraphicsControls;

public class ControlVm : BaseViewModel
{
    private RelayCommand _removeCommand;

    public ControlVm()
    {
        Name = "Название графика";
    }

    public ControlVm(ImportedCurve curve)
    {
        Curve = curve;
        curve.NameChanged += Graphic_NameChanged;
        curve.IsEnabledChanged += GraphicIsEnabledChanged;
    }

    public string Name
    {
        get => Curve.Name;
        set
        {
            if (value != Curve.Name)
            {
                Curve.Name = value;
            }
        }
    }

    public bool Enabled
    {
        get => Curve.IsEnabled;
        set
        {
            Curve.IsEnabled = value;
            OnPropertyChanged();
        }
    }

    public ImportedCurve Curve { get; }
    public RelayCommand RemoveCommand => _removeCommand ??= new RelayCommand(_ => Remove());

    private void GraphicIsEnabledChanged()
    {
        OnPropertyChanged(nameof(Enabled));
    }

    private void Graphic_NameChanged()
    {
        OnPropertyChanged(nameof(Name));
    }

    public event EventHandler<ImportedGraphicEventArgs> Removed;

    private void Remove()
    {
        Removed?.Invoke(this, new ImportedGraphicEventArgs(Curve));
    }
}
