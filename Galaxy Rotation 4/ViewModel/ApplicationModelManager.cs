﻿using System.Collections.Generic;
using System.Linq;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.ViewModel.Serializing;

namespace Galaxy_Rotation_4.ViewModel;

internal class ApplicationModelManager
{
    public static ApplicationModel Open(string serializedModel, string name)
    {
        var galaxy = new Galaxy();

        var galaxyDescription = Serializer.Deserialize<GalaxyDescription>(serializedModel);

        Dictionary<Component, ComponentDescription> componentsToDescriptions = galaxyDescription.Descriptions
            .ToDictionary(componentDescription => ComponentsFactory.Create(componentDescription, galaxy));

        foreach (Component component in componentsToDescriptions.Keys)
        {
            galaxy.AddComponent(component);
        }

        return new ApplicationModel(galaxy, name, componentsToDescriptions);
    }
}
