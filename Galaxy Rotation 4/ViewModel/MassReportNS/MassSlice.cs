﻿using OxyPlot.Series;

namespace Galaxy_Rotation_4.ViewModel.MassReportNS;

//TODO: Похоже, можно удалить.
public class MassSlice : PieSlice
{
    public MassSlice(MassReportItemVm massReportItem) : base(massReportItem.Name, massReportItem.RelativeMass)
    {
        MassReportItem = massReportItem;
    }

    public MassReportItemVm MassReportItem { get; }
}
