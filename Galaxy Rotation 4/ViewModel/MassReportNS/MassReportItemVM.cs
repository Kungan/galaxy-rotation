﻿using System.Windows;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Model.MassReportNS;
using Galaxy_Rotation_4.ViewModel.Resources;

namespace Galaxy_Rotation_4.ViewModel.MassReportNS;

public class MassReportItemVm : BaseViewModel
{
    private double _relativeMass;

    public MassReportItemVm(MassReportItem massReportItem)
    {
        MassReportItem = massReportItem;
        massReportItem.EnabledChanged += () => OnPropertyChanged(nameof(Enabled));
        massReportItem.IndexChanged += MassReportItem_IndexChanged;
        massReportItem.MassChanged += () => OnPropertyChanged(nameof(Mass));
        CopyMassCommand = new RelayCommand(_ => CopyMass());
        CopyRelativeMassCommand = new RelayCommand(_ => CopyRelativeMass());
    }

    public string Category => MassReportItem.Component.GetCategory().GetLocalizedName();
    public RelayCommand CopyMassCommand { get; }
    public RelayCommand CopyRelativeMassCommand { get; }
    public bool Enabled => MassReportItem.Enabled;
    public int Index => MassReportItem.Index;
    public double Mass => MassReportItem.Mass;
    public MassReportItem MassReportItem { get; }
    public string Name => MassReportItem.Component.GetShortName();
    public string NameAndNumber => $"{Name} ({Index})";

    public double RelativeMass
    {
        get => _relativeMass;
        private set
        {
            _relativeMass = value;
            OnPropertyChanged();
        }
    }

    private void CopyMass()
    {
        Clipboard.SetText(Mass.ToString("F0"));
    }

    private void CopyRelativeMass()
    {
        Clipboard.SetText(RelativeMass.ToString("F2"));
    }

    private void MassReportItem_IndexChanged()
    {
        OnPropertyChanged(nameof(Index));
        OnPropertyChanged(nameof(NameAndNumber));
    }

    public void UpdateRelativeMass(double galaxyMass)
    {
        RelativeMass = 100 * Mass / galaxyMass;
    }
}
