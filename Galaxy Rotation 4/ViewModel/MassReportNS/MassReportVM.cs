﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.MassReportNS;

namespace Galaxy_Rotation_4.ViewModel.MassReportNS;

public class MassReportVm : DockWindowVm
{
    private readonly MassReport _massReport;

    public MassReportVm()
    {
    }

    public MassReportVm(MassReport massReport)
    {
        Title = "Отчет о массах";
        _massReport = massReport;
        massReport.GalaxyMassChanged += MassReport_GalaxyMassChanged;
        massReport.MassReportItemRemoved += MassReport_MassReportItemRemoved;
        massReport.MassReportItemAdded += MassReport_MassReportItemAdded;
        AddMassReportItems(massReport);
    }

    public ObservableCollection<MassReportItemVm> MassReportItems { get; } = new();
    public ObservableCollection<MassReportItemVm> MassReportDiagramItems { get; } = new();
    public double GalaxyMass => _massReport.GalaxyMass;

    public bool FullMassModeEnabled
    {
        get => _massReport.FullMassModeEnabled;
        set
        {
            _massReport.FullMassModeEnabled = value;
            UpdateRelativeMasses();
            OnPropertyChanged();
        }
    }

    public double LowRadius
    {
        get => _massReport.LowRadius;

        set
        {
            _massReport.LowRadius = value;
            OnPropertyChanged();
        }
    }

    public double HighRadius
    {
        get => _massReport.HighRadius;

        set
        {
            _massReport.HighRadius = value;
            OnPropertyChanged();
        }
    }

    private void AddMassReportItems(MassReport massReport)
    {
        foreach (MassReportItem item in massReport.MassReportItems)
        {
            AddItem(item);
        }
    }

    private void MassReport_MassReportItemAdded(object sender, MassReportItemEventArgs e)
    {
        AddItem(e.MassReportItem);
    }

    private void AddItem(MassReportItem item)
    {
        var massReportItem = new MassReportItemVm(item);
        MassReportItems.Add(massReportItem);

        if (massReportItem.Enabled)
        {
            MassReportDiagramItems.Add(massReportItem);
        }

        massReportItem.PropertyChanged += MassReportItem_PropertyChanged;
    }

    private void MassReport_MassReportItemRemoved(object sender, MassReportItemEventArgs e)
    {
        MassReportItemVm vm = MassReportItems.Where(item => item.MassReportItem == e.MassReportItem).First();
        MassReportItems.Remove(vm);
        MassReportDiagramItems.Remove(vm);
        vm.PropertyChanged -= MassReportItem_PropertyChanged;
    }

    private void MassReportItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName != nameof(MassReportItemVm.Enabled))
            return;

        if (sender is not MassReportItemVm massReportItemVm)
            return;

        bool alreadyAdded = MassReportDiagramItems.Contains(massReportItemVm);

        if (massReportItemVm.Enabled && !alreadyAdded)
            MassReportDiagramItems.Add(massReportItemVm);
        else if (!massReportItemVm.Enabled && alreadyAdded)
            MassReportDiagramItems.Remove(massReportItemVm);
    }

    private void MassReport_GalaxyMassChanged()
    {
        OnPropertyChanged(nameof(GalaxyMass));
        UpdateRelativeMasses();
    }

    private void UpdateRelativeMasses()
    {
        foreach (MassReportItemVm item in MassReportItems)
        {
            item.UpdateRelativeMass(GalaxyMass);
        }
    }
}
