﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using Galaxy_Rotation_4.Model;
using OxyPlot;
using OxyPlot.Series;

namespace Galaxy_Rotation_4.ViewModel.Diagrams;

public class LineVm : LineSeries
{
    public LineVm(Curve curve)
    {
        Curve = curve;
        curve.IsEnabledChanged += UpdateActivity;
        curve.DotsChanged += UpdateDots;
        MarkerOutline = Array.Empty<ScreenPoint>();
        UpdateActivity();
        UpdateDots();
    }

    public Curve Curve { get; }

    public event Action Changed;

    private void UpdateActivity()
    {
        IsVisible = Curve.IsEnabled;
        OnChanged();
    }

    private void UpdateDots()
    {
        Points.Clear();

        foreach (KeyValuePair<double, double> dot in Curve.Dots)
        {
            Points.Add(new DataPoint(dot.Key, dot.Value));
        }

        OnChanged();
    }

    private void OnChanged()
    {
        Changed?.Invoke();
    }

    private static OxyColor Convert(Color value) => OxyColor.FromArgb(value.A, value.R, value.G, value.B);

    private static Color Convert(OxyColor color) => Color.FromArgb(color.A, color.R, color.G, color.B);

    public void InvalidatePlot()
    {
        ((PlotVm)PlotModel)?.InvalidatePlot(true);
    }

    #region Переопределение унаследованных свойств

    public new LineJoin LineJoin
    {
        get => base.LineJoin;
        set
        {
            base.LineJoin = value;
            InvalidatePlot();
        }
    }

    public new LineStyle LineStyle
    {
        get => ActualLineStyle;
        set
        {
            base.LineStyle = value;
            InvalidatePlot();
        }
    }

    public new LineLegendPosition LineLegendPosition
    {
        get => base.LineLegendPosition;
        set
        {
            base.LineLegendPosition = value;
            InvalidatePlot();
        }
    }

    public new Color MarkerFill
    {
        get => Convert(ActualMarkerFill);
        set
        {
            base.MarkerFill = Convert(value);
            InvalidatePlot();
        }
    }

    public new ScreenPoint[] MarkerOutline
    {
        get => base.MarkerOutline;
        set
        {
            base.MarkerOutline = value;
            InvalidatePlot();
        }
    }

    public new int MarkerResolution
    {
        get => base.MarkerResolution;
        set
        {
            base.MarkerResolution = value;
            InvalidatePlot();
        }
    }

    public new double MarkerSize
    {
        get => base.MarkerSize;
        set
        {
            base.MarkerSize = value;
            InvalidatePlot();
        }
    }

    public new double MinimumSegmentLength
    {
        get => base.MinimumSegmentLength;
        set
        {
            base.MinimumSegmentLength = value;
            InvalidatePlot();
        }
    }

    public new double StrokeThickness
    {
        get => base.StrokeThickness;
        set
        {
            base.StrokeThickness = value;
            InvalidatePlot();
        }
    }

    public new Color MarkerStroke
    {
        get => Convert(base.MarkerStroke);
        set
        {
            base.MarkerStroke = Convert(value);
            InvalidatePlot();
        }
    }

    public new double MarkerStrokeThickness
    {
        get => base.MarkerStrokeThickness;
        set
        {
            base.MarkerStrokeThickness = value;
            InvalidatePlot();
        }
    }

    public new MarkerType MarkerType
    {
        get => base.MarkerType;
        set
        {
            base.MarkerType = value;
            InvalidatePlot();
        }
    }

    public new double LabelMargin
    {
        get => base.LabelMargin;
        set
        {
            base.LabelMargin = value;
            InvalidatePlot();
        }
    }

    public new double BrokenLineThickness
    {
        get => base.BrokenLineThickness;
        set
        {
            base.BrokenLineThickness = value;
            InvalidatePlot();
        }
    }

    public new LineStyle BrokenLineStyle
    {
        get => base.BrokenLineStyle;
        set
        {
            base.BrokenLineStyle = value;
            InvalidatePlot();
        }
    }

    public new Color BrokenLineColor
    {
        get => Convert(base.BrokenLineColor);
        set
        {
            base.BrokenLineColor = Convert(value);
            InvalidatePlot();
        }
    }

    public new Color Color
    {
        get => Convert(ActualColor);
        set
        {
            base.Color = Convert(value);
            InvalidatePlot();
        }
    }

    public new double[] Dashes
    {
        get => base.Dashes;
        set
        {
            base.Dashes = value;
            InvalidatePlot();
        }
    }

    #endregion
}
