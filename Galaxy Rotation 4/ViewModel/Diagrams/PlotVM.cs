﻿using System;
using OxyPlot;

namespace Galaxy_Rotation_4.ViewModel.Diagrams;

/// <summary>
///     Класс модели представления, основанный на OxyPlot.PlotModel, но при этом не выбрасывающий исключение при работе с AvalonDocks. При обновлении
///     библиотеки OxyPlot следует проверить, не появилось ли неучтенных использований переопределенных членов в базовом классе.
/// </summary>
public class PlotVm : PlotModel, IPlotModel
{
    private WeakReference _plotViewReference;

    public new IPlotView PlotView => (IPlotView)_plotViewReference?.Target;

    /// <summary>
    ///     Переопределенение метода базового класса. В базовом классе выбрасывается исключение в том случае, если plotViewReference уже хранит ссылку,
    ///     отличную от той, которую нужно сохранить в модели. Судя по всему, AvalonDocks выгружает не отображаемые в данный момент вкладки и заново создает
    ///     их, когда их нужно отобразить. При этом возникало исключение в данном методе. Новый метод присваивает новую ссылку не выбрасывая исключения.
    /// </summary>
    /// <param name="plotView"></param>
    public void AttachPlotView(IPlotView plotView)
    {
        _plotViewReference = plotView == null ? null : new WeakReference(plotView);
    }

    public new void InvalidatePlot(bool updateData)
    {
        PlotView?.InvalidatePlot(updateData);
    }
}
