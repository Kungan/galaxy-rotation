﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Graphics;
using OxyPlot.Axes;

namespace Galaxy_Rotation_4.ViewModel.Diagrams;

public class DiagramVm : DockWindowVm
{
    public DiagramVm()
    {
    }

    public DiagramVm(string title, Curve totalSeries, Observation observation)
    {
        Title = PlotModel.Title = title;
        PlotModel.Axes.Add(AxisX);
        PlotModel.Axes.Add(AxisY);
        AddTotalSeries(totalSeries);
        observation.GraphicImported += Observation_GraphicImported;
        observation.GraphicRemoved += Observation_GraphicRemoved;
        AddObservables(observation);
    }

    public LinearAxis AxisX { get; } = new()
    {
        Title = "Радиус (килопарсек)",
        Position = AxisPosition.Bottom,
        PositionAtZeroCrossing = true,
        ExtraGridlines = new[] { 0.0 },
        TitlePosition = 0.2,
    };

    public LinearAxis AxisY { get; } = new()
    {
        Position = AxisPosition.Left, PositionAtZeroCrossing = true, ExtraGridlines = new[] { 0.0 }, TitlePosition = 0.2,
    };

    public string AxisYTitle
    {
        get => AxisY.Title;
        set
        {
            if (AxisY.Title != value)
            {
                AxisY.Title = value;
                OnPropertyChanged();
            }
        }
    }

    public PlotVm PlotModel { get; } = new();

    public LineVm TotalVm { get; private set; }

    public ObservableCollection<ImportedGraphicVm> Observables { get; } = new();

    public void AddLineViewModel(LineVm lineVm)
    {
        PlotModel.Series.Add(lineVm);
        SubscribeOnChanging(lineVm);
    }

    private void AddObservables(Observation observation)
    {
        foreach (ImportedCurve graphic in observation.GraphicsList)
        {
            AddObservableGraphic(graphic);
        }
    }

    private void AddObservableGraphic(ImportedCurve curve)
    {
        var vm = new ImportedGraphicVm(curve);
        AddLineViewModel(vm);
        Observables.Add(vm);
    }

    private void AddTotalSeries(Curve curve)
    {
        TotalVm = new LineVm(curve) { Color = Colors.Red, StrokeThickness = 4, Title = "Сумма" };
        AddLineViewModel(TotalVm);
    }

    private void RefreshDiagram()
    {
        PlotModel.ResetAllAxes();
        PlotModel.InvalidatePlot(true);
    }

    public void RemoveLineViewModel(LineVm lineVm)
    {
        PlotModel.Series.Remove(lineVm);
        UnSubscribeOnChanging(lineVm);
    }

    private void SubscribeOnChanging(LineVm lineVm)
    {
        lineVm.Changed += RefreshDiagram;
        RefreshDiagram();
    }

    private void UnSubscribeOnChanging(LineVm lineVm)
    {
        lineVm.Changed -= RefreshDiagram;
        RefreshDiagram();
    }

    private void Observation_GraphicImported(object sender, ImportedGraphicEventArgs e)
    {
        AddObservableGraphic(e.Curve);
    }

    private void Observation_GraphicRemoved(object sender, ImportedGraphicEventArgs e)
    {
        var vm = PlotModel.Series.First(series => (series as ImportedGraphicVm)?.Curve == e.Curve) as ImportedGraphicVm;
        RemoveLineViewModel(vm);
        Observables.Remove(vm);
    }
}
