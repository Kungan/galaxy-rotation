﻿using Galaxy_Rotation_4.Model.Graphics;
using OxyPlot;

namespace Galaxy_Rotation_4.ViewModel.Diagrams;

public class ImportedGraphicVm : LineVm
{
    public ImportedGraphicVm(ImportedCurve curve) : base(curve)
    {
        Title = curve.Name;
        LineStyle = LineStyle.None;
        MarkerType = MarkerType.Square;
        curve.NameChanged += Graphic_NameChanged;
    }

    private void Graphic_NameChanged()
    {
        Title = (Curve as ImportedCurve).Name;
    }
}
