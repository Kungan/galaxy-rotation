﻿using System;
using Galaxy_Rotation_4.Model.Components;

namespace Galaxy_Rotation_4.ViewModel.Diagrams;

public class LineGroupVm : BaseViewModel
{
    private int _index;
    private string _name;
    private LineVm _speedVm;

    public event Action SpeedVmChanged;

    public LineGroupVm(Component component, int index)
    {
        Component = component;
        component.Truncation.TypeChanged += UpdateName;
        _index = index;
        DensityVm = new LineVm(component.DensityCurve);
        UpdateName();
    }

    public LineVm SpeedVm
    {
        get => _speedVm;
        set
        {
            if (Equals(value, _speedVm))
                return;
            
            _speedVm = value;
            SpeedVmChanged?.Invoke();
            OnPropertyChanged();
        }
    }

    public LineVm DensityVm { get; }
    public Component Component { get; }

    public string Name
    {
        get => _name;
        private set
        {
            _name = value;
            
            if (SpeedVm != null)
                SpeedVm.Title = value;
            
            if (DensityVm != null)
                DensityVm.Title = value;
            
            OnPropertyChanged();
        }
    }

    public int Index
    {
        get => _index;
        set
        {
            if (_index != value)
            {
                _index = value;
                UpdateName();
            }
        }
    }

    private void UpdateName()
    {
        string name = Component.GetFullName();
        Name = $"{name} ({_index})";
    }
}
