﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Model.SpeedCalculation;
using Component = Galaxy_Rotation_4.Model.Components.Component;

namespace Galaxy_Rotation_4.ViewModel.Diagrams;

public class DiagramsGroupVm
{
    #region Конструкторы

    public DiagramsGroupVm(ApplicationModel appModel)
    {
        _galaxy = appModel.Galaxy;
        _plotParams = _galaxy.PlotParams;
        _galaxy.ComponentAdded += Galaxy_ComponentAdded;
        _galaxy.ComponentRemoved += Galaxy_ComponentRemoved;

        SpeedDiagramVm = new DiagramVm("График скорости", _galaxy.SpeedCalculator.TotalSpeed, appModel.ObservationSpeed) { AxisYTitle = "Километров в секунду" };
        SpeedDiagramVm.PropertyChanged += SpeedDiagramVM_PropertyChanged;
        _galaxy.SpeedCalculator.SpeedCalculatorAdded += OnSpeedCalculatorAdded;
        UpdateSpeedActivity();

        VolumeDensityDiagramVm =
            new DiagramVm("График объемной плотности", _galaxy.TotalVolumeDensity, appModel.ObservationVolumeDensity)
            {
                AxisYTitle = "M☉ на кубический парсек",
            };

        VolumeDensityDiagramVm.PropertyChanged += VolumeDensityDiagramVM_PropertyChanged;
        UpdateVolumeDensityActivity();

        SurfaceDensityDiagramVm =
            new DiagramVm("График поверхностной плотноcти", _galaxy.TotalSurfaceDensity, appModel.ObservationSurfaceDensity)
            {
                AxisYTitle = "M☉ на квадратный парсек",
            };

        SurfaceDensityDiagramVm.PropertyChanged += SurfaceDensityDiagramVM_PropertyChanged;
        UpdateSurfaceDensityActivity();

        LineGroupVMs.CollectionChanged += LineGroupVMs_CollectionChanged;
        AddComponents();
    }

    private void OnSpeedCalculatorAdded(ComponentSpeedCalculator calculator)
    {
        var speedVm = new LineVm(calculator.SpeedCurve);
        SpeedDiagramVm.AddLineViewModel(speedVm);
        LineGroupVMs.First(lineGroup => lineGroup.Component == calculator.Component).SpeedVm = speedVm;
    }

    #endregion конструкторы

    #region Поля

    private readonly Galaxy _galaxy;
    private readonly PlotParams _plotParams;

    #endregion поля

    #region Свойства

    public DiagramVm VolumeDensityDiagramVm { get; }
    public ObservableCollection<LineGroupVm> LineGroupVMs { get; } = new();
    public DiagramVm SpeedDiagramVm { get; }
    public DiagramVm SurfaceDensityDiagramVm { get; }

    #endregion свойства

    #region Методы

    private void AddComponents()
    {
        foreach (Component component in _galaxy.Components)
        {
            AddComponent(component);
        }
    }

    private void AddComponent(Component component)
    {
        var lineGroupVm = new LineGroupVm(component, LineGroupVMs.Count + 1);

        switch (component.Dimension)
        {
            case Dimension.Volume:
                VolumeDensityDiagramVm.AddLineViewModel(lineGroupVm.DensityVm);
                break;
            case Dimension.Surface:
                SurfaceDensityDiagramVm.AddLineViewModel(lineGroupVm.DensityVm);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        // Добавлять в общую коллекцию моделей представления можно только после того, как каждая линия
        // была добавлена на диаграмму и получила свой цвет. Иначе при обработке события добавления
        // в коллекцию новой группы линий будет обработано с не до конца инициализированными линиями.
        // Поэтому выполняем это действие в конце.
        LineGroupVMs.Add(lineGroupVm);
    }

    private void UpdateVolumeDensityActivity()
    {
        _plotParams.VolumeDensityEnabled = !VolumeDensityDiagramVm.IsClosed;
    }

    private void UpdateSpeedActivity()
    {
        _plotParams.SpeedEnabled = !SpeedDiagramVm.IsClosed;
    }

    private void UpdateSurfaceDensityActivity()
    {
        _plotParams.SurfaceDensityEnabled = !SurfaceDensityDiagramVm.IsClosed;
    }

    #endregion методы

    #region Обработчики

    private void VolumeDensityDiagramVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == nameof(DiagramVm.IsClosed))
        {
            UpdateVolumeDensityActivity();
        }
    }

    private void Galaxy_ComponentAdded(object sender, ComponentEventArgs e)
    {
        AddComponent(e.Component);
    }

    private void Galaxy_ComponentRemoved(object sender, ComponentEventArgs e)
    {
        Component component = e.Component;
        LineGroupVm componentVm = LineGroupVMs.First(vm => vm.Component == component);

        SpeedDiagramVm.RemoveLineViewModel(componentVm.SpeedVm);

        switch (component.Dimension)
        {
            case Dimension.Surface:
                SurfaceDensityDiagramVm.RemoveLineViewModel(componentVm.DensityVm);
                break;
            case Dimension.Volume:
                VolumeDensityDiagramVm.RemoveLineViewModel(componentVm.DensityVm);
                break;
            default:
                throw new NotSupportedException($"Cannot recognize density type of component{component.GetType().FullName}");
        }

        LineGroupVMs.Remove(componentVm);
    }

    private void LineGroupVMs_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        var vmCollection = sender as ObservableCollection<LineGroupVm>;

        if (e.Action == NotifyCollectionChangedAction.Remove ||
            e.Action == NotifyCollectionChangedAction.Move ||
            e.Action == NotifyCollectionChangedAction.Replace)
        {
            foreach (LineGroupVm vm in vmCollection)
            {
                vm.Index = _galaxy.GetComponentIndex(vm.Component);
            }
        }
    }

    private void SpeedDiagramVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == nameof(DiagramVm.IsClosed))
        {
            UpdateSpeedActivity();
        }
    }

    private void SurfaceDensityDiagramVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == nameof(DiagramVm.IsClosed))
        {
            UpdateSurfaceDensityActivity();
        }
    }

    #endregion обработчики
}
