﻿namespace Galaxy_Rotation_4.ViewModel.DigitizeNS;

public class DigitizeEventArgs
{
    public DigitizeEventArgs(DigitizeVm digitizeViewModel)
    {
        DigitizeViewModel = digitizeViewModel;
    }

    public DigitizeVm DigitizeViewModel { get; }
}
