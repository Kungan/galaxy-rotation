﻿using System.Windows.Input;
using System.Windows.Media.Imaging;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Galaxy_Rotation_4.ViewModel.DigitizeNS;

public class DigitizeVm : DockWindowVm
{
    private readonly LinearAxis _axisX = new()
    {
        Title = "X",
        Position = AxisPosition.Bottom,
        PositionAtZeroCrossing = true,
        ExtraGridlines = new[] { 0.0 },
        TitlePosition = 0,
        Maximum = 600,
        Minimum = -600,
    };

    private readonly LinearAxis _axisY = new()
    {
        Title = "Y",
        Position = AxisPosition.Left,
        PositionAtZeroCrossing = true,
        ExtraGridlines = new[] { 0.0 },
        TitlePosition = 0,
        Maximum = 300,
        Minimum = -300,
    };

    private readonly LineSeries _series = new() { MarkerFill = OxyColors.Blue };

    public DigitizeVm()
    {
    }

    public DigitizeVm(BitmapSource bitmap, string title)
    {
        Title = title;
        Bitmap = bitmap;
        PlotModel.Axes.Add(_axisX);
        PlotModel.Axes.Add(_axisY);
        PlotModel.Series.Add(_series);
        PlotModel.MouseDown += PlotModel_MouseDown;
        _series.SelectionMode = SelectionMode.Single;
    }

    public BitmapSource Bitmap { get; }
    public PlotModel PlotModel { get; } = new();

    private void InvalidatePlot()
    {
        PlotModel.InvalidatePlot(true);
    }

    private void PlotModel_MouseDown(object sender, OxyMouseEventArgs e)
    {
        if (Mouse.LeftButton == MouseButtonState.Pressed)
        {
            double x = _axisX.InverseTransform(e.Position.X);
            double y = _axisY.InverseTransform(e.Position.Y);
            _series.Points.Add(new DataPoint(x, y));
            InvalidatePlot();
        }
    }
}
