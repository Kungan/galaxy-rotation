﻿using System;

namespace Galaxy_Rotation_4.ViewModel.Resources;

//TODO: Похоже, можно удалить.
public class EnumItem
{
    public EnumItem(string name, Enum value)
    {
        Name = name;
        Value = value;
    }

    public string Name { get; set; }
    public Enum Value { get; set; }
}
