﻿using System;
using System.Windows;

namespace Galaxy_Rotation_4.ViewModel.Resources;

public static class ComponentLocalizator
{
    public static string GetLocalizedName(Type type) => Application.Current.TryFindResource(type) as string ?? type.ToString();
}
