﻿using System;
using System.Reflection;
using System.Windows;

namespace Galaxy_Rotation_4.ViewModel.Resources;

internal static class PropertyLocalizator
{
    private static readonly ResourceDictionary _commonProperties = new()
    {
        Source = new Uri("pack://application:,,,/ViewModel/Resources/ComponentsLocalization/Properties/Common.xaml"),
    };

    private static readonly ResourceDictionary _allComponentsResources = new()
    {
        Source = new Uri("pack://application:,,,/ViewModel/Resources/ComponentsLocalization/ComponentPropertiesLocalization.xaml"),
    };

    public static string GetLocalizedPropertyName(PropertyInfo property)
    {
        string propertyName = property.Name;
        var properties = _allComponentsResources[property.DeclaringType] as ResourceDictionary;

        return properties?[propertyName] as string ?? _commonProperties[propertyName] as string ?? propertyName;
    }
}
