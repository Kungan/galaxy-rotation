﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Galaxy_Rotation_4.ViewModel.Resources;

public static class EnumLocalizator
{
    /// <summary>
    ///     Для использования необходимо создать файл ресурсов вида "Имя члена перечисления" -> "Локализованное название" в пространстве Resources с тем же
    ///     именем, что и название класса перечисления. Для не локализованных значений членов перечисления будут использованы их имена.
    /// </summary>
    public static Dictionary<string, Enum> GetLocalizedNames<TEnum>(this TEnum enumerate) where TEnum : Enum =>
        // Получаем все варианты перечисления.
        GetLocalizedNames(enumerate.GetType());

    public static Dictionary<string, Enum> GetLocalizedNames(Type enumType)
    {
        // Получаем все варианты перечисления.
        Array enumValues = enumType.GetEnumValues();
        var enumNamesAndValues = new Dictionary<string, Enum>();

        foreach (Enum e in enumValues)
        {
            string name = e.GetLocalizedName();
            enumNamesAndValues.Add(name, e);
        }

        return enumNamesAndValues;
    }

    public static string GetLocalizedName<TEnum>(this TEnum enumerate) where TEnum : Enum =>
        Application.Current.TryFindResource(enumerate) as string ?? enumerate.ToString();
}
