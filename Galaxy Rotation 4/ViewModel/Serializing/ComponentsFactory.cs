﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

internal class ComponentsFactory
{
    internal static Component Create(Type componentType, Galaxy galaxy)
    {
        if (!componentType.IsSubclassOf(typeof(Component)))
            throw new ComponentCreationException($"{componentType.Name} должен быть наследником {nameof(Component)}.");

        if (Activator.CreateInstance(componentType) is not Component component)
            throw new ComponentCreationException("Ошибка при создании компонента.");

        component.PlotParams = galaxy.PlotParams;

        return component;
    }

    internal static Component Create(ComponentDescription description, Galaxy galaxy)
    {
        Type componentType = Assembly.GetEntryAssembly().GetTypes().First(type => type.Name == description.ComponentType);

        Component component = Create(componentType, galaxy);
        List<PropertyInfo> controlledProperties = ComponentDescription.GetControlledProperties(component).ToList();

        foreach ((string propertyName, ValueContainer valueContainer) in description.Properties)
        {
            List<PropertyInfo> filteredProperties = controlledProperties
                .Where(property => property.Name == propertyName)
                .ToList();

            if (!filteredProperties.Any())
            {
                throw new ComponentCreationException($"Тип {componentType.Name} не содержит свойства {propertyName}.");
            }

            filteredProperties.First().SetValue(component, valueContainer.GetValue());
        }

        return component;
    }
}
