﻿using System;
using System.Collections.Generic;
using System.Linq;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.ViewModel.ComponentControls;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

/// <summary>Сериализуемый класс для хранения информации о галактике</summary>
[Serializable]
public class GalaxyDescription
{
    /// <summary>Конструктор для десериализации</summary>
    public GalaxyDescription()
    {
    }

    public GalaxyDescription(Galaxy galaxy, ControlsPaneVm controlsPaneVm)
    {
        Dictionary<Component, ComponentControlVm> componentsToControls = controlsPaneVm.Sections
            .SelectMany(controlsSectionVm => controlsSectionVm.ComponentControls)
            .ToDictionary(componentControl => componentControl.Component);

        foreach (Component component in galaxy.Components)
        {
            Descriptions.Add(new ComponentDescription(component, componentsToControls[component]));
        }
    }

    public List<ComponentDescription> Descriptions { get; } = new();
}
