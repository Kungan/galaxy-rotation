﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.Model.Components.Attributes;
using Galaxy_Rotation_4.ViewModel.ComponentControls;
using Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

/// <summary>Сериализуемый класс для хранения параметров компонента</summary>
[Serializable]
[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class ComponentDescription
{
    /// <summary>Конструктор для десериализации</summary>
    public ComponentDescription()
    {
    }

    public ComponentDescription(Component component, ComponentControlVm componentControlVm)
    {
        ComponentType = component.GetType().Name;
        Properties = GetProperties(component, componentControlVm);
    }

    public string ComponentType { get; set; }

    public PropertiesDictionary Properties { get; set; }

    public static IEnumerable<PropertyInfo> GetControlledProperties(Component component)
    {
        return component.GetType().GetProperties().Where(property => Attribute.IsDefined(property, typeof(SaveableAttribute)));
    }

    protected static PropertiesDictionary GetProperties(Component component, ComponentControlVm componentControlVm)
    {
        Dictionary<PropertyInfo, DoubleVm> propertyToDoubleVms = componentControlVm.Properties
            .OfType<DoubleVm>()
            .ToDictionary(doubleVm => doubleVm.PropertyInfo);

        IEnumerable<PropertyInfo> controlledProperties = GetControlledProperties(component);

        var dictionary = new PropertiesDictionary();

        foreach (PropertyInfo property in controlledProperties)
        {
            dictionary.AddProperty(component, property, propertyToDoubleVms.GetValueOrDefault(property));
        }

        return dictionary;
    }
}
