﻿using System;
using System.Configuration;
using Galaxy_Rotation_4.Model;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

[Serializable]
[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class SerializableGraphic : ValueContainer
{
    /// <summary>Конструктор без параметров для десериализации.</summary>
    public SerializableGraphic()
    {
    }

    public SerializableGraphic(Curve curve)
    {
        Dots = new SerializableDictionary<double, double>(curve.Dots);
    }

    public SerializableDictionary<double, double> Dots { get; set; }

    public override object GetValue() => new Curve(Dots);
}
