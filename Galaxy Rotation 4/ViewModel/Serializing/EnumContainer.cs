﻿using System;
using System.Configuration;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class EnumContainer : ValueContainer
{
    /// <summary>Конструктор без параметров для десерк</summary>
    public EnumContainer()
    {
    }

    public EnumContainer(Enum value)
    {
        Value = Convert.ChangeType(value, Enum.GetUnderlyingType(value.GetType()));
        ;
    }

    public object Value { get; set; }

    public override object GetValue() => Value;
}
