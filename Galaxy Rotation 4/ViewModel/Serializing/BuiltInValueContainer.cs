﻿using System.Configuration;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class BuiltInValueContainer : ValueContainer
{
    /// <summary>Конструктор без параметров для десериализации.</summary>
    public BuiltInValueContainer()
    {
    }

    /// <summary>В качестве значения можно передавать только встроенные типы данных. Иначе при сериализации произойдет ошибка.</summary>
    /// <param name="value">Значение</param>
    public BuiltInValueContainer(object value)
    {
        Value = value;
    }

    public object Value { get; set; }

    public override object GetValue() => Value;
}
