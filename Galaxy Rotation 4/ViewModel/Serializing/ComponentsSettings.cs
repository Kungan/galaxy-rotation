﻿using System;
using System.Configuration;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

[Serializable]
[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class ComponentsSettingsDictionary : SerializableDictionary<string, ComponentDescription>
{
}
