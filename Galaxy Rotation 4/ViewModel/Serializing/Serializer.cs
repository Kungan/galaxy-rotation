﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

public static class Serializer
{
    public static string Serialize<T>(T objectToSerialize)
    {
        var serializer = new XmlSerializer(typeof(T));
        using var writer = new StringWriter();
        serializer.Serialize(writer, objectToSerialize);

        return writer.ToString();
    }

    public static T Deserialize<T>(string serializedObject)
    {
        var serializer = new XmlSerializer(typeof(T));
        using var reader = new StringReader(serializedObject);

        try
        {
            return (T)serializer.Deserialize(reader);
        }
        catch (Exception e)
        {
            throw new SerializerException("Ошибка при десериализации.", e);
        }
    }
}
