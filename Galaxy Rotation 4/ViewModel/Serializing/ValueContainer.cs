﻿using System;
using System.Configuration;
using System.Xml.Serialization;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

/// <summary>Базовый класс для сериализации свойств компонентов.</summary>
[Serializable]
[SettingsSerializeAs(SettingsSerializeAs.Xml)]
[XmlInclude(typeof(SerializableGraphic))]
[XmlInclude(typeof(BuiltInValueContainer))]
[XmlInclude(typeof(DoubleValueContainer))]
[XmlInclude(typeof(EnumContainer))]
public abstract class ValueContainer
{
    public abstract object GetValue();
}
