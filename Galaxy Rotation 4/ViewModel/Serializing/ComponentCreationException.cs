﻿using System;
using System.Runtime.Serialization;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

[Serializable]
internal class ComponentCreationException : Exception
{
    public ComponentCreationException()
    {
    }

    public ComponentCreationException(string message) : base(message)
    {
    }

    public ComponentCreationException(string message, Exception innerException) : base(message, innerException)
    {
    }

    protected ComponentCreationException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}
