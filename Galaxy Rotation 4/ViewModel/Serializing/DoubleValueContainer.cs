﻿#nullable enable
using System;
using System.Configuration;
using Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

[Serializable]
[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class DoubleValueContainer : BuiltInValueContainer
{
    /// <summary>Конструктор для десериализации.</summary>
    public DoubleValueContainer()
    {
    }

    public DoubleValueContainer(double value, DoubleVm? doubleVm) : base(value)
    {
        Minimum = doubleVm?.Minimum;
        IncrementType = doubleVm?.TypeOfIncrement;
        AbsoluteIncrement = doubleVm?.AbsoluteIncrement;
        RelativeIncrementPercents = doubleVm?.RelativeIncrementPercents;
    }

    public double? AbsoluteIncrement { get; set; }
    public double? RelativeIncrementPercents { get; set; }
    public double? Minimum { get; set; }

    public IncrementType? IncrementType { get; set; }
}
