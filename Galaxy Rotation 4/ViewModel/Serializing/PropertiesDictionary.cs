﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using Galaxy_Rotation_4.Model;
using Galaxy_Rotation_4.Model.Components;
using Galaxy_Rotation_4.ViewModel.ComponentControls.ComponentProperties;

namespace Galaxy_Rotation_4.ViewModel.Serializing;

[SettingsSerializeAs(SettingsSerializeAs.Xml)]
public class PropertiesDictionary : SerializableDictionary<string, ValueContainer>
{
    /// <summary>Добавляет новое свойство в словарь.</summary>
    /// <param name="component">Компонент, к которому относится свойство.</param>
    /// <param name="property">Свойство.</param>
    public void AddProperty(Component component, PropertyInfo property, DoubleVm? doubleVm = null)
    {
        ValueContainer value = property.GetValue(component) switch
        {
            Curve graphic => new SerializableGraphic(graphic),
            Enum v => new EnumContainer(v),
            bool v => new BuiltInValueContainer(v),
            byte v => new BuiltInValueContainer(v),
            sbyte v => new BuiltInValueContainer(v),
            char v => new BuiltInValueContainer(v),
            decimal v => new BuiltInValueContainer(v),
            double v => new DoubleValueContainer(v, doubleVm),
            float v => new BuiltInValueContainer(v),
            int v => new BuiltInValueContainer(v),
            uint v => new BuiltInValueContainer(v),
            long v => new BuiltInValueContainer(v),
            ulong v => new BuiltInValueContainer(v),
            short v => new BuiltInValueContainer(v),
            ushort v => new BuiltInValueContainer(v),
            string v => new BuiltInValueContainer(v),
            _ => throw new ArgumentException("Параметр должен иметь тип из числа встроенных либо перечислением. " +
                $"Для сериализации других типов данных унаследуйтесь от класса {nameof(ValueContainer)}" +
                $"и добавьте к базовому классу аттрибут {nameof(XmlIncludeAttribute)} с названием класса-наследника"),
        };

        Add(property.Name, value);
    }

    public Dictionary<string, T> GetPropertiesOfType<T>()
        where T : ValueContainer => this
        .Where(entry => entry.Value is T)
        .ToDictionary(entry => entry.Key, entry => (T)entry.Value);
}
