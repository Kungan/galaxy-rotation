﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using Galaxy_Rotation_4.ViewModel.ControlsNS;
using Galaxy_Rotation_4.ViewModel.Diagrams;
using Galaxy_Rotation_4.ViewModel.Extensions;
using Galaxy_Rotation_4.ViewModel.Resources;
using OxyPlot;
using OxyPlot.Series;

namespace Galaxy_Rotation_4.ViewModel.LineDesignerNS;

public class LineDesignerVm : DockWindowVm
{
    private const string MarkerOutlineXColumnTitle = "X";
    private const string MarkerOutlineYColumnTitle = "Y";

    private LineVm _currentImportedGraphic;

    private LineVm _currentLine;
    private LineCollectionType _currentLineCollection;

    private LineGroupVm _currentLineGroup;

    private RenamedLineVm _currentTotalLine;

    private ArrayControlVm<double> _dashesArrayVm = new();

    private bool _isSpeedSelected = true;

    private KeyValueArrayVm<double, double> _markerOutlineVm = new()
    {
        KeyColumnTitle = MarkerOutlineXColumnTitle, ValueColumnTitle = MarkerOutlineYColumnTitle,
    };

    public LineDesignerVm()
    {
    }

    public LineDesignerVm(DiagramsGroupVm diagramsGroupVm)
    {
        LineGroups = diagramsGroupVm.LineGroupVMs;
        LineGroups.CollectionChanged += LineGroups_CollectionChanged;

        DashesArrayVm.PropertyChanged += ArrayControlVM_PropertyChanged;
        MarkerOutlineVm.PropertyChanged += MarkerOutlineVM_PropertyChanged;

        ObservableCollection<ImportedGraphicVm> speedObservables = diagramsGroupVm.SpeedDiagramVm.Observables;
        speedObservables.CollectionChanged += Observables_CollectionChanged;
        ObservableCollection<ImportedGraphicVm> volumeDensityObservables = diagramsGroupVm.VolumeDensityDiagramVm.Observables;
        volumeDensityObservables.CollectionChanged += Observables_CollectionChanged;
        ObservableCollection<ImportedGraphicVm> surfaceDensityObservables = diagramsGroupVm.SurfaceDensityDiagramVm.Observables;
        surfaceDensityObservables.CollectionChanged += Observables_CollectionChanged;

        ImportedGraphics = new CompositeCollection
        {
            new CollectionContainer { Collection = speedObservables },
            new CollectionContainer { Collection = volumeDensityObservables },
            new CollectionContainer { Collection = surfaceDensityObservables },
        };

        TotalLines = new ObservableCollection<RenamedLineVm>
        {
            new(diagramsGroupVm.SpeedDiagramVm.TotalVm, "скорость"),
            new(diagramsGroupVm.VolumeDensityDiagramVm.TotalVm, "объемная плотность"),
            new(diagramsGroupVm.SurfaceDensityDiagramVm.TotalVm, "поверхностная плотность"),
        };

        _currentTotalLine = TotalLines.First();
    }

    public LineCollectionType CurrentLineCollection
    {
        get => _currentLineCollection;
        set
        {
            _currentLineCollection = value;
            UpdateCurrentLine();
            OnPropertyChanged();
        }
    }

    public ObservableCollection<LineGroupVm> LineGroups { get; }
    public ObservableCollection<RenamedLineVm> TotalLines { get; }
    public CompositeCollection ImportedGraphics { get; }

    public ArrayControlVm<double> DashesArrayVm
    {
        get => _dashesArrayVm;
        private set
        {
            _dashesArrayVm = value;
            OnPropertyChanged();
        }
    }

    public KeyValueArrayVm<double, double> MarkerOutlineVm
    {
        get => _markerOutlineVm;
        private set
        {
            _markerOutlineVm = value;
            OnPropertyChanged();
        }
    }

    public Dictionary<string, Enum> AvailableLineStyles { get; } = EnumLocalizator.GetLocalizedNames(typeof(LineStyle));
    public Dictionary<string, Enum> AvailableLineJoins { get; } = EnumLocalizator.GetLocalizedNames(typeof(LineJoin));
    public Dictionary<string, Enum> AvailableLineLegendPositions { get; } = EnumLocalizator.GetLocalizedNames(typeof(LineLegendPosition));
    public Dictionary<string, Enum> AvailableMarkerTypes { get; } = EnumLocalizator.GetLocalizedNames(typeof(MarkerType));

    public LineGroupVm CurrentLineGroup
    {
        get => _currentLineGroup;
        set
        {
            if (_currentLineGroup != null)
                _currentLineGroup.SpeedVmChanged -= UpdateCurrentLine;

            _currentLineGroup = value;

            if (value != null)
	            value.SpeedVmChanged += UpdateCurrentLine;

            UpdateCurrentLine();
            OnPropertyChanged();
        }
    }

    public RenamedLineVm CurrentTotalLine
    {
        get => _currentTotalLine;
        set
        {
            _currentTotalLine = value;
            UpdateCurrentLine();
            OnPropertyChanged();
        }
    }

    public LineVm CurrentImportedGraphic
    {
        get => _currentImportedGraphic;
        set
        {
            _currentImportedGraphic = value;
            UpdateCurrentLine();
            OnPropertyChanged();
        }
    }

    public bool IsSpeedSelected
    {
        get => _isSpeedSelected;

        set
        {
            _isSpeedSelected = value;
            UpdateCurrentLine();
        }
    }

    public LineVm CurrentLine
    {
        get => _currentLine;
        set
        {
            _currentLine = value;
            UpdateDashesArray();
            UpdateMarkerOutline();
            OnPropertyChanged();
        }
    }

    private void Observables_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        if (!ImportedGraphics.ContainsInnerItem(CurrentImportedGraphic) && ImportedGraphics.ContainsAnyInnerItems())
        {
            CollectionContainer firstNotEmptyContainer = ImportedGraphics.Cast<CollectionContainer>()
                .Where(container => container.Collection.GetEnumerator().MoveNext()).First();

            IEnumerator iEnumerator = firstNotEmptyContainer.Collection.GetEnumerator();
            iEnumerator.MoveNext();
            CurrentImportedGraphic = iEnumerator.Current as ImportedGraphicVm;
        }
    }

    private void LineGroups_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        if (!LineGroups.Contains(CurrentLineGroup) && LineGroups.Count > 0)
        {
            CurrentLineGroup = LineGroups.First();
        }
    }

    private void UpdateCurrentLine()
    {
        CurrentLine = _currentLineCollection switch
        {
            LineCollectionType.Components => (_isSpeedSelected ? _currentLineGroup?.SpeedVm : _currentLineGroup?.DensityVm) ?? CurrentLine,
            LineCollectionType.Totals => _currentTotalLine?.LineVm,
            LineCollectionType.ImportedGraphics => _currentImportedGraphic,
            _ => CurrentLine,
        };
    }

    private void UpdateDashesArray()
    {
        DashesArrayVm.PropertyChanged -= ArrayControlVM_PropertyChanged;
        DashesArrayVm = new ArrayControlVm<double>(_currentLine?.Dashes);
        DashesArrayVm.PropertyChanged += ArrayControlVM_PropertyChanged;
    }

    private void ArrayControlVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (_currentLine == null)
        {
            return;
        }

        double[] dashes = DashesArrayVm.Array;
        _currentLine.Dashes = dashes.Length > 0 ? dashes : null;
    }

    private void UpdateMarkerOutline()
    {
        MarkerOutlineVm.PropertyChanged -= MarkerOutlineVM_PropertyChanged;

        (double, double)[] points = (_currentLine?.MarkerOutline ?? Array.Empty<ScreenPoint>())
            .Select(item => (item.X, item.Y))
            .ToArray();

        MarkerOutlineVm = new KeyValueArrayVm<double, double>(points)
        {
            KeyColumnTitle = MarkerOutlineXColumnTitle, ValueColumnTitle = MarkerOutlineYColumnTitle,
        };

        MarkerOutlineVm.PropertyChanged += MarkerOutlineVM_PropertyChanged;
    }

    private void MarkerOutlineVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (_currentLine == null)
        {
            return;
        }

        (double key, double value)[] points = _markerOutlineVm.Array;

        _currentLine.MarkerOutline = points
            .Select(point => new ScreenPoint(point.key, point.value))
            .ToArray();
    }
}
