﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Galaxy_Rotation_4.ViewModel.ControlsNS;

namespace Galaxy_Rotation_4.ViewModel.LineDesignerNS;

public class KeyValueArrayVm<TKey, TValue> : BaseViewModel
{
    public KeyValueArrayVm()
    {
        Values = new ObservableCollection<KeyValueVm<TKey, TValue>>();
    }

    public KeyValueArrayVm((TKey key, TValue value)[] dashes)
    {
        if (dashes != null)
        {
            Values = new ObservableCollection<KeyValueVm<TKey, TValue>>(
                from item in dashes
                select new KeyValueVm<TKey, TValue>(item.key, item.value));

            foreach (KeyValueVm<TKey, TValue> value in Values)
            {
                value.PropertyChanged += Value_PropertyChanged;
            }
        }
        else
        {
            Values = new ObservableCollection<KeyValueVm<TKey, TValue>>();
        }
    }

    public (TKey key, TValue value)[] Array => Values.Select(vm => (vm.Key, vm.Value)).ToArray();

    public ObservableCollection<KeyValueVm<TKey, TValue>> Values { get; }

    private void Value_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        OnChanged();
    }

    private void OnChanged()
    {
        OnPropertyChanged(nameof(Array));
    }

    #region Заголовки столбцов

    private string _keyColumnTitle;

    public string KeyColumnTitle
    {
        get => _keyColumnTitle;
        set
        {
            _keyColumnTitle = value;
            OnPropertyChanged();
        }
    }

    private string _valueColumnTitle;

    public string ValueColumnTitle
    {
        get => _valueColumnTitle;
        set
        {
            _valueColumnTitle = value;
            OnPropertyChanged();
        }
    }

    #endregion

    #region Команда добавления элемента в массив

    private RelayCommand _addCommand;
    public RelayCommand AddCommand => _addCommand ??= new RelayCommand(_ => Add());

    private void Add()
    {
        var newValue = new KeyValueVm<TKey, TValue>();
        newValue.PropertyChanged += Value_PropertyChanged;
        Values.Add(newValue);
        OnChanged();
    }

    #endregion

    #region Команда добавления элемента в массив

    private RelayCommand _removeCommand;

    public RelayCommand RemoveCommand => _removeCommand ??= new RelayCommand(item => Remove(item as KeyValueVm<TKey, TValue>));

    private void Remove(KeyValueVm<TKey, TValue> item)
    {
        item.PropertyChanged -= Value_PropertyChanged;
        Values.Remove(item);
        OnChanged();
    }

    #endregion
}
