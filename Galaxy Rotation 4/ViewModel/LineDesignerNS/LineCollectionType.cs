﻿namespace Galaxy_Rotation_4.ViewModel.LineDesignerNS;

public enum LineCollectionType
{
    Components,
    Totals,
    ImportedGraphics,
}
