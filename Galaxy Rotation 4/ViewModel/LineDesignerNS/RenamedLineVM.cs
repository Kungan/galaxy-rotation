﻿using Galaxy_Rotation_4.ViewModel.Diagrams;

namespace Galaxy_Rotation_4.ViewModel.LineDesignerNS;

public class RenamedLineVm
{
    public RenamedLineVm(LineVm lineVm, string type)
    {
        LineVm = lineVm;
        Type = type;
    }

    public LineVm LineVm { get; }
    public string Type { get; }
}
