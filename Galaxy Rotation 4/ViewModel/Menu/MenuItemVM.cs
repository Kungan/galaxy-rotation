﻿using System.Collections.Generic;
using System.Windows.Input;

namespace Galaxy_Rotation_4.ViewModel.Menu;

public class MenuItemVm : BaseViewModel
{
    public MenuItemVm()
    {
    }

    public MenuItemVm(ICommand command)
    {
        Command = command;
    }

    #region Properties

    public string Header { get; set; }
    public bool IsCheckable { get; set; }
    public List<MenuItemVm> Items { get; } = new();
    public ICommand Command { get; }

    #region IsChecked

    private bool _isChecked;

    public virtual bool IsChecked
    {
        get => _isChecked;
        set
        {
            if (_isChecked != value)
            {
                _isChecked = value;
                OnPropertyChanged();
            }
        }
    }

    #endregion

    #endregion
}
