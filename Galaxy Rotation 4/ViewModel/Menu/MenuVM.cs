﻿using System.Collections.Generic;

namespace Galaxy_Rotation_4.ViewModel.Menu;

public class MenuVm
{
    public MenuVm(IEnumerable<DockWindowVm> dockWindows)
    {
        var items = new List<MenuItemVm>();

        foreach (DockWindowVm dockWindow in dockWindows)
        {
            items.Add(GetMenuItemViewModel(dockWindow));
        }

        Items = items;
    }

    public IEnumerable<MenuItemVm> Items { get; }

    private MenuItemVm GetMenuItemViewModel(DockWindowVm dockWindowViewModel)
    {
        var menuItemViewModel = new MenuItemVm { IsCheckable = true, Header = dockWindowViewModel.Title, IsChecked = !dockWindowViewModel.IsClosed };

        dockWindowViewModel.PropertyChanged += (o, e) =>
        {
            if (e.PropertyName == nameof(DockWindowVm.IsClosed))
            {
                menuItemViewModel.IsChecked = !dockWindowViewModel.IsClosed;
            }
        };

        menuItemViewModel.PropertyChanged += (o, e) =>
        {
            if (e.PropertyName == nameof(MenuItemVm.IsChecked))
            {
                dockWindowViewModel.IsClosed = !menuItemViewModel.IsChecked;
            }
        };

        return menuItemViewModel;
    }
}
