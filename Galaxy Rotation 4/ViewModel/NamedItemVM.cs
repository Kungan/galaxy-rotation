﻿namespace Galaxy_Rotation_4.ViewModel;

// Класс предназначен для использования в коллекциях именованных объектов.
// К примеру - в ObservableCollection, к которому привязывается Combobox.
public class NamedItemVm<T> : BaseViewModel
{
    private T _item;
    private string _name;

    public NamedItemVm(string name, T item)
    {
        _name = name;
        _item = item;
    }

    public string Name
    {
        get => _name;
        set
        {
            _name = value;
            OnPropertyChanged();
        }
    }

    public T Item
    {
        get => _item;
        set
        {
            _item = value;
            OnPropertyChanged();
        }
    }
}
