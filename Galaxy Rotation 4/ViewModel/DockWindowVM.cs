﻿using System.Windows.Input;

namespace Galaxy_Rotation_4.ViewModel;

public abstract class DockWindowVm : BaseViewModel
{
    public DockWindowVm()
    {
        CanClose = true;
        IsClosed = false;
    }

    public void Close()
    {
        IsClosed = true;
    }

    #region Properties

    #region CloseCommand

    private ICommand _closeCommand;

    public ICommand CloseCommand
    {
        get
        {
            if (_closeCommand == null)
                _closeCommand = new RelayCommand(call => Close());

            return _closeCommand;
        }
    }

    #endregion

    #region IsClosed

    private bool _isClosed;

    public virtual bool IsClosed
    {
        get => _isClosed;
        set
        {
            if (_isClosed != value)
            {
                _isClosed = value;
                OnPropertyChanged();
            }
        }
    }

    #endregion

    #region IsActive

    private bool _isActive;

    public bool IsActive
    {
        get => _isActive;
        set
        {
            if (_isActive != value)
            {
                _isActive = value;
                OnPropertyChanged(nameof(IsClosed));
            }
        }
    }

    #endregion

    #region CanClose

    private bool _canClose;

    public bool CanClose
    {
        get => _canClose;
        set
        {
            if (_canClose != value)
            {
                _canClose = value;
                OnPropertyChanged();
            }
        }
    }

    #endregion

    #region Title

    private string _title;

    public string Title
    {
        get => _title;
        set
        {
            if (_title != value)
            {
                _title = value;
                OnPropertyChanged();
            }
        }
    }

    #endregion

    #endregion
}
