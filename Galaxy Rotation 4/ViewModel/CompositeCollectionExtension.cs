﻿using System.Linq;
using System.Windows.Data;

namespace Galaxy_Rotation_4.ViewModel.Extensions;

internal static class CompositeCollectionExtension
{
    public static bool ContainsInnerItem(this CompositeCollection compositeCollection, object itemToFind)
    {
        foreach (object container in compositeCollection)
        {
            foreach (object item in (container as CollectionContainer).Collection)
            {
                if (item.Equals(itemToFind))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public static bool ContainsAnyInnerItems(this CompositeCollection compositeCollection)
    {
        return compositeCollection.Cast<CollectionContainer>().Where(container => container.Collection.GetEnumerator().MoveNext()).Any();
    }
}
