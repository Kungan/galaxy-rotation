﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Galaxy_Rotation_4.ViewModel.Converters;

/// <summary>Конвертер типов bool и enum. Используется для упрощения привязки в XAML коде.</summary>
internal class BooleanToEnumConverter : IValueConverter
{
    /// <summary>Сравнивает перечисление value с перечислением parameter.</summary>
    /// <returns></returns>
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value.Equals(true) ? parameter : Binding.DoNothing;

    /// <summary>Если <see langword="true" />, возвращает parameter.</summary>
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => ((Enum)value).Equals((Enum)parameter);
}
