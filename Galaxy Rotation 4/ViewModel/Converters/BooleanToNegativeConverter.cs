﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Galaxy_Rotation_4.ViewModel.Converters;

/// <summary>Конвертер для bool, возвращающий его отрицание.</summary>
internal class BooleanToNegative : IValueConverter
{
    /// <summary>Возвращает отрицание значения.</summary>
    /// <returns></returns>
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => !(bool)value;

    /// <summary>Возвращает отрицание значения.</summary>
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => !(bool)value;
}
