﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Galaxy_Rotation_4.ViewModel.Converters;

internal class EnumToVisibilityConverter : IValueConverter
{
    /// <summary>Если value равно parameter то возвращается то значение Visibility.Visible (видимо), иначе Visibility.Collapsed (свернуто).</summary>
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
        ((Enum)value).Equals((Enum)parameter) ? Visibility.Visible : Visibility.Collapsed;

    /// <summary>Ничего не делает.</summary>
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => Binding.DoNothing;
}
