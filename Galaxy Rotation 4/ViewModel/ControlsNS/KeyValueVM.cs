﻿namespace Galaxy_Rotation_4.ViewModel.ControlsNS;

public class KeyValueVm<TKey, TValue> : BaseViewModel
{
    private TKey _key;
    private TValue _value;

    public KeyValueVm(TKey key = default, TValue value = default)
    {
        _key = key;
        _value = value;
    }

    public TValue Value
    {
        get => _value;
        set
        {
            _value = value;
            OnPropertyChanged();
        }
    }

    public TKey Key
    {
        get => _key;
        set
        {
            _key = value;
            OnPropertyChanged();
        }
    }
}
