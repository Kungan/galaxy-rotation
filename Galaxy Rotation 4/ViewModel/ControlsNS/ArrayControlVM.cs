﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Galaxy_Rotation_4.ViewModel.ControlsNS;

public class ArrayControlVm<T> : BaseViewModel
{
    public ArrayControlVm()
    {
        Values = new ObservableCollection<ValueVm<T>>();
    }

    public ArrayControlVm(IEnumerable<T> dashes)
    {
        if (dashes != null)
        {
            Values = new ObservableCollection<ValueVm<T>>(
                from item in dashes
                select new ValueVm<T>(item));

            foreach (ValueVm<T> value in Values)
            {
                value.PropertyChanged += Value_PropertyChanged;
            }
        }
        else
        {
            Values = new ObservableCollection<ValueVm<T>>();
        }
    }

    public T[] Array => Values.Select(valueVm => valueVm.Value).ToArray();

    public ObservableCollection<ValueVm<T>> Values { get; }

    private void Value_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        OnChanged();
    }

    private void OnChanged()
    {
        OnPropertyChanged(nameof(Array));
    }

    #region Заголовок столбца

    private string _columnTitle;

    public string ColumnTitle
    {
        get => _columnTitle;
        set
        {
            _columnTitle = value;
            OnPropertyChanged();
        }
    }

    #endregion

    #region Команда добавления элемента

    private RelayCommand _addCommand;
    public RelayCommand AddCommand => _addCommand ??= new RelayCommand(_ => Add());

    private void Add()
    {
        var newValue = new ValueVm<T>();
        newValue.PropertyChanged += Value_PropertyChanged;
        Values.Add(newValue);
        OnChanged();
    }

    #endregion

    #region Команда удаления элемента

    private RelayCommand _removeCommand;

    public RelayCommand RemoveCommand => _removeCommand ??= new RelayCommand(item => Remove(item as ValueVm<T>));

    private void Remove(ValueVm<T> item)
    {
        item.PropertyChanged -= Value_PropertyChanged;
        Values.Remove(item);
        OnChanged();
    }

    #endregion
}
