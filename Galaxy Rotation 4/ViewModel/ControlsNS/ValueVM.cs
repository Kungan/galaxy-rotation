﻿namespace Galaxy_Rotation_4.ViewModel.ControlsNS;

public class ValueVm<T> : BaseViewModel
{
    private T _value;

    public ValueVm(T value = default)
    {
        _value = value;
    }

    public T Value
    {
        get => _value;
        set
        {
            _value = value;
            OnPropertyChanged();
        }
    }
}
