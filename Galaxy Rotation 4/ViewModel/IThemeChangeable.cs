﻿using System.Collections.ObjectModel;
using Xceed.Wpf.AvalonDock.Themes;

namespace Galaxy_Rotation_4.ViewModel;

internal interface IThemeChangeable
{
    ObservableCollection<NamedItemVm<Theme>> Themes { get; }
    Theme CurrentTheme { get; set; }
}
