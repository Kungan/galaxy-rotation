﻿using System;
using System.Windows.Input;

namespace Galaxy_Rotation_4;

public class RelayCommand : ICommand
{
    public RelayCommand(Action<object> execute)
        : this(execute, _ => true)
    {
    }

    public RelayCommand(Action<object> action, Predicate<object> canExecute)
    {
        _executeAction = action;
        _canExecuteAction = canExecute;
    }

    #region Properties

    private readonly Action<object> _executeAction;
    private readonly Predicate<object> _canExecuteAction;

    #endregion

    #region Methods

    public bool CanExecute(object parameter) => _canExecuteAction(parameter);

    public event EventHandler CanExecuteChanged
    {
        add => CommandManager.RequerySuggested += value;
        remove => CommandManager.RequerySuggested -= value;
    }

    public void Execute(object parameter)
    {
        _executeAction(parameter);
    }

    #endregion
}
