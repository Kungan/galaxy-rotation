﻿using System.Windows;
using Galaxy_Rotation_4.Model;

namespace Galaxy_Rotation_4.ViewModel.DeviationsNS;

public class StandardDeviationVm : BaseViewModel
{
    private double _absoluteStd;
    private string _name;
    private double _relativeStd;

    public StandardDeviationVm(StandardDeviation standardDeviation)
    {
        StandardDeviation = standardDeviation;
        standardDeviation.StdChanged += UpdateStd;
        standardDeviation.Curve.NameChanged += UpdateName;
        UpdateName();
        UpdateStd();
        CopyAbsoluteDeviationCommand = new RelayCommand(_ => CopyAbsoluteDeviation());
        CopyRelativeDeviationCommand = new RelayCommand(_ => CopyRelativeDeviation());
    }

    public StandardDeviation StandardDeviation { get; }

    public double AbsoluteStd
    {
        get => _absoluteStd;
        set
        {
            if (value != _absoluteStd)
            {
                _absoluteStd = value;
                OnPropertyChanged();
            }
        }
    }

    public RelayCommand CopyAbsoluteDeviationCommand { get; }
    public RelayCommand CopyRelativeDeviationCommand { get; }

    public string Name
    {
        get => _name;
        set
        {
            if (value != _name)
            {
                _name = value;
                OnPropertyChanged();
            }
        }
    }

    public double RelativeStd
    {
        get => _relativeStd;
        set
        {
            if (value != _relativeStd)
            {
                _relativeStd = value;
                OnPropertyChanged();
            }
        }
    }

    private void CopyAbsoluteDeviation()
    {
        Clipboard.SetText(AbsoluteStd.ToString("F6"));
    }

    private void CopyRelativeDeviation()
    {
        Clipboard.SetText(RelativeStd.ToString("F6"));
    }

    private void UpdateName()
    {
        Name = StandardDeviation.Curve.Name;
    }

    private void UpdateStd()
    {
        AbsoluteStd = StandardDeviation.AbsoluteStd;
        RelativeStd = StandardDeviation.RelativeStd * 100;
    }
}
