﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Galaxy_Rotation_4.Model;

namespace Galaxy_Rotation_4.ViewModel.DeviationsNS;

public class StandardDeviationsGroupVm : DockWindowVm
{
    #region Свойства

    public ObservableCollection<StandardDeviationVm> Deviations { get; } = new();

    #endregion свойства

    #region Конструкторы

    public StandardDeviationsGroupVm()
    {
    }

    public StandardDeviationsGroupVm(IEnumerable<Deviations> deviationsGroup)
    {
        foreach (Deviations deviations in deviationsGroup)
        {
            deviations.StandardDeviationAdded += Deviations_StandardDeviationAdded;
            deviations.StandardDeviationRemoved += Deviations_StandardDeviationRemoved;
            AddDeviations(deviations);
        }
    }

    #endregion конструкторы

    #region Методы

    private void AddDeviation(StandardDeviation standardDeviation)
    {
        Deviations.Add(new StandardDeviationVm(standardDeviation));
    }

    private void AddDeviations(Deviations deviations)
    {
        foreach (StandardDeviation deviation in deviations.StandardDeviations)
        {
            AddDeviation(deviation);
        }
    }

    private void RemoveDeviation(StandardDeviation standardDeviation)
    {
        StandardDeviationVm vm = Deviations.Where(deviation => deviation.StandardDeviation == standardDeviation).First();
        Deviations.Remove(vm);
    }

    #endregion методы

    #region Обработчики

    private void Deviations_StandardDeviationRemoved(object sender, StandardDeviationEventArgs e)
    {
        StandardDeviation standardDeviation = e.StandardDeviation;
        RemoveDeviation(standardDeviation);
    }

    private void Deviations_StandardDeviationAdded(object sender, StandardDeviationEventArgs e)
    {
        StandardDeviation standardDeviation = e.StandardDeviation;
        AddDeviation(standardDeviation);
    }

    #endregion обработчики
}
